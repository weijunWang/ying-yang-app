import { NgModule } from '@angular/core';

import { WordPlacePipe } from './word-place/word-place';

@NgModule({
	declarations: [
		WordPlacePipe,
	],
	imports: [],
	exports: [
		WordPlacePipe,
	]
})

export class PipesModule {}
