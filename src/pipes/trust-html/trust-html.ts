import { DomSanitizer } from '@angular/platform-browser';
import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TrustHtmlPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'trustHtml',
})
export class TrustHtmlPipe implements PipeTransform {
  constructor(private domSanitizer: DomSanitizer){}
    transform(html: string, args: any[]): any {
        return this.domSanitizer.bypassSecurityTrustResourceUrl(html);
    }
}
