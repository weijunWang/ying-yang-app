import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhysicalDataPage } from '../physical-data/physical-data';
import { HttpServerProvider } from '../../providers/http-server/http-server';

@IonicPage()
@Component({
  selector: 'page-gestation',
  templateUrl: 'gestation.html',
})
export class GestationPage {
  childbirth: string;
  last_menstrual: string;
  menstruation: string;
  isShow: Boolean = false;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams, 
    public http: HttpServerProvider) {
  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad GestationPage');
  }
  changeShow() {
    this.isShow = true;
  }
  complateInp(){
    console.log("完成");
    let data;
    if(this.isShow){
      data = {
        last_menstrual: this.last_menstrual,
        menstruation: this.menstruation
      }
    }else{
      data = {
        childbirth: this.childbirth
      }
    }
    let that = this;
    this.http.patch('/api/userinfo', data, function (res) {
      console.log(res);
      if (res) {
        that.navCtrl.push(PhysicalDataPage, {id: 1});
      }
    }, function (err) {
      console.log(err);
    })
  }
  
}
