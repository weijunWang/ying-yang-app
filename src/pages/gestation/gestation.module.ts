import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GestationPage } from './gestation';

@NgModule({
  declarations: [
    GestationPage,
  ],
  imports: [
    IonicPageModule.forChild(GestationPage),
  ],
})
export class GestationPageModule {}
