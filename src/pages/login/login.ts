import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegisterInfoPage } from '../register-info/register-info';
import { ForgetPwdPage } from '../forget-pwd/forget-pwd';
import { RegisterPage } from '../register/register';
import { HttpServerProvider } from '../../providers/http-server/http-server';
import { StorageServeProvider } from '../../providers/storage-serve/storage-serve';
import { TabsPage } from '../tabs/tabs';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  isHide: boolean = false;
  username: string;
  password: string;
  userinfo: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider) {
  }

  ngOnInit() {
    console.log('ionViewDidLoad LoginPage');
    let that = this;
    let token = this.storage.getStorage('token');
    console.log('token', this.storage.getStorage('token'))
    this.http.get('/api/userinfo',{}, function (res) {
      that.userinfo = res;
      if (token) {
        if (that.userinfo.life_stage != '' || that.userinfo.height || that.userinfo.weight) {
          that.navCtrl.push(TabsPage)
        } else {
          that.navCtrl.push(RegisterPage)
        }
      }
    })


  }
  toRegister() {
    console.log("toRegister");
    this.navCtrl.push(RegisterInfoPage);
  }
  toForgetPwd() {
    console.log("toForgetPwd");
    this.navCtrl.push(ForgetPwdPage);
  }
  login() {
    let that = this;
    let data = {
      username: this.username,
      password: this.password
    }
    console.log(data);
    this.http.post('/api/login/', data, function (res) {
      console.log(res);
      console.log(that.userinfo);
      if (that.userinfo.life_stage != '' || that.userinfo.height || that.userinfo.weight) {
        that.navCtrl.push(TabsPage)
      } else {
        that.navCtrl.push(RegisterPage)
      }
    }, function (err) {
      console.log(err);
    })
  }
  focusInput() {
    this.isHide = true;
  }
  blurInput() {
    this.isHide = false;
  }
}
