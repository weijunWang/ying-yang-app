import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';
import { HttpServerProvider } from '../../providers/http-server/http-server';
import { ForgetPwdPage } from '../forget-pwd/forget-pwd';

@IonicPage()
@Component({
  selector: 'page-register-info',
  templateUrl: 'register-info.html',
})
export class RegisterInfoPage {
  vcode: '';
  codeNum: any = "";
  isHide: boolean = false;
  codeString: string = "重新获取"
  showPwd: boolean = false;
  username: string;
  password: string;
  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public http: HttpServerProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RegisterInfoPage');
  }
  getCode() {
    let that = this;
    this.http.post('/api/vcode/', {
      phone: '15721252192'
    }, function (res) {
      console.log(res);
      that.vcode = res.vcode;
    }, function (err) {
      console.log(err);
    })
    console.log("获取验证码");
    this.codeString = "重新获取";
    this.codeNum = 60;
    let timer = setInterval(() => {
      if (this.codeNum <= 0) {
        this.codeNum = 0;
        clearInterval(timer);
      } else {
        this.codeNum--;
      }
      console.log(this.codeNum);
    }, 1000);
  }
  toLogin() {
    console.log("toLogin");
    this.navCtrl.pop()
  }
  toForgetPwd() {
    console.log("toForgetPwd");
    this.navCtrl.push(ForgetPwdPage);
  }
  checkCode(event) {
    console.log(event);
    if (event.length > 0) {
      this.showPwd = true;
    }
  }
  showAlert() {
    let that = this;
    console.log(this.vcode);
    let data = {
      user: {
        username: this.username,
        password: this.password,
      },
      phone: this.username,
      header_img: ''
    }
    console.log(data);
    this.http.post('/api/register/', data, function (res) {
      console.log(res);
      if (res) {
        let alert = that.alertCtrl.create({
          title: '温馨提示',
          subTitle: `在您的使用过程中，我们将会为您及时更新APP信息，为让您获得完整体验，需要开启以下权限`,
          message: `允许信息通知推送`,
          buttons: ['我知道了']
        });
        alert.present();
        setTimeout(function(){
          that.navCtrl.pop();
        }, 1000);
      }
    }, function (err) {
      console.log(err);
    })
  }
  focusInput() {
    this.isHide = true;
  }
  blurInput() {
    this.isHide = false;
  }
}
