import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../providers/http-server/http-server';

@IonicPage()
@Component({
  selector: 'page-has-child',
  templateUrl: 'has-child.html',
})
export class HasChildPage {

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: HttpServerProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HasChildPage');
  }
  confirm(){
    let that = this;
    let data = {
      
    }
    console.log(data);
  }
}
