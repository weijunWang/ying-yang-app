import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HasChildPage } from './has-child';

@NgModule({
  declarations: [
    HasChildPage,
  ],
  imports: [
    IonicPageModule.forChild(HasChildPage),
  ],
})
export class HasChildPageModule {}
