import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ForgetPwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-forget-pwd',
  templateUrl: 'forget-pwd.html',
})
export class ForgetPwdPage {
  isHide: boolean = false;
  codeNum :any = '';
  showPwd :number = 1;
  codeString :string = "获取验证码";
  password :string = '';
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ForgetPwdPage');
  }
  getCode(){
    console.log("获取验证码");
    this.codeString = "重新获取";
    this.codeNum = 60;
    let timer = setInterval(() => {
      if(this.codeNum <= 0){
        this.codeNum = 0;
        clearInterval(timer);
      }else{
        this.codeNum--;
      }
      console.log(this.codeNum);
    },1000);
  }
  toLogin(){
    console.log("toLogin");
    this.navCtrl.pop()
  }
  nextStep(){
    console.log("下一步");
    if(this.showPwd == 1){
      this.showPwd = 2
    }else if(this.showPwd == 2){
      this.showPwd = 3
    }
  }
  focusInput(){
    this.isHide = true;
  }
  blurInput(){
    this.isHide = false;
  }
}
