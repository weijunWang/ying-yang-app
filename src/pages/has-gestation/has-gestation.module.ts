import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HasGestationPage } from './has-gestation';

@NgModule({
  declarations: [
    HasGestationPage,
  ],
  imports: [
    IonicPageModule.forChild(HasGestationPage),
  ],
})
export class HasGestationPageModule {}
