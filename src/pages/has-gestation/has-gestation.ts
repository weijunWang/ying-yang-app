import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhysicalDataPage } from '../physical-data/physical-data';
import { HttpServerProvider } from '../../providers/http-server/http-server';

@IonicPage()
@Component({
  selector: 'page-has-gestation',
  templateUrl: 'has-gestation.html',
})
export class HasGestationPage {
  average_menstruation: string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: HttpServerProvider,) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HasGestationPage');
    
  }
  complateInp(){
    let that = this;
    this.http.patch('/api/userinfo', {
      average_menstruation: that.average_menstruation,
    }, function (res) {
      console.log(res);
      if (res) {
        that.navCtrl.push(PhysicalDataPage, {id: 2});
      }
    }, function (err) {
      console.log(err);
    })
  }
}
