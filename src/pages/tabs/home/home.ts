import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { CordovaServersProvider } from '../../../providers/cordova-servers/cordova-servers';
import {Camera, CameraOptions} from '@ionic-native/camera';
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';


import { KnowledgesPage } from '../../content/knowledges/knowledges';
import { KnowledgeDetailPage } from '../../content/knowledge-detail/knowledge-detail';
// import {EarlyPregnancyPage} from '../../content/early-pregnancy/early-pregnancy';
import { RecipesPage } from '../../content/recipes/recipes';
import { FaqPage } from '../../content/faq/faq';
import { GadgetPage } from '../../content/gadget/gadget'; 
// import { BirthChildInfoPage } from '../../content/birth-child-info/birth-child-info';
import { ChangeBabyHeightPage } from '../../content/change-baby-height/change-baby-height';
import { QuestionnairePage } from '../../content/questionnaire/questionnaire';
import { QuestionnaireGdmPage } from '../../content/questionnaire-gdm/questionnaire-gdm';
import { QuestionnaireFeeddingTypePage } from '../../content/questionnaire-feedding-type/questionnaire-feedding-type';
 
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  listData: any;
  cardData: any;
  pushModal: boolean = false; 
  yunqiPush: boolean = false;
  renchenModal: boolean = true;
  renchenList: any;
  // 有没有小孩的切换开关
  hasBaby: boolean;

  barId: number = 1;
  barContent: string = '请完成妊娠期糖尿病高危因素筛查';
  btnText: string = '查看';
  chioceBarNum: number = 0;
  cameraUpload: string;
  userinfo: any;
  constructor(
    private imagePicker: ImagePicker,
    private camera: Camera, 
    public navCtrl: NavController, 
    public actionSheetCtrl: ActionSheetController, 
    private cordovaServers: CordovaServersProvider,
    public http: HttpServerProvider,
    public storage: StorageServeProvider) {
      
  }
  ngOnInit() {
    let that = this;
    this.http.get('/api/userinfo', {}, function (res) {
      console.log(res);
      if (res) {
        that.userinfo = res;
        that.storage.setStorage('userinfo', res);
        if(res.has_child){
          that.hasBaby = true;
        }
      }
    })
    this.cameraUpload = '../../../assets/imgs/home-img.png';
    document.getElementsByClassName('scroll-content')[0].setAttribute('marginBottom', '0')
    console.log('cordovaServers',this.cordovaServers.getTest());
    this.renchenList = [
      {
        id: 1,
        flag: false,
        name: '无妊娠期糖尿病'
      },{
        id: 2,
        flag: false,
        name: '已确诊妊娠期糖尿病'
      },{
        id: 3,
        flag: false,
        name: '还未做检查'
      }
    ]
    this.listData = [
      {
        icon: '../../../assets/imgs/home-icon1.png',
        title: '知识库',
        text: '孕育知识科普讲解'
      }, {
        icon: '../../../assets/imgs/home-icon2.png',
        title: '营养食谱',
        text: '孕育知识科普讲解'
      }, {
        icon: '../../../assets/imgs/home-icon3.png',
        title: '小工具',
        text: '孕育知识科普讲解'
      }, {
        icon: '../../../assets/imgs/home-icon4.png',
        title: '问与答',
        text: '孕育知识科普讲解'
      }
    ]
    this.cardData = [
      {
        title: '孕早期知识',
        text: '孕早期有哪些注意事项',
        img: '../../../assets/imgs/home-card-img.png'
      }
    ]
  }
  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      title: '选择上传方式',
      buttons: [
        {
          text: '拍照',
          handler: () => {
            console.log('Destructive clicked');
            // alert(this.cordovaServers.openCamera())
            const options: CameraOptions = {
              quality: 90,                                                   //相片质量 0 -100
              destinationType: this.camera.DestinationType.DATA_URL,        //DATA_URL 是 base64   FILE_URL 是文件路径
              encodingType: this.camera.EncodingType.JPEG,
              mediaType: this.camera.MediaType.PICTURE,
              saveToPhotoAlbum: true,                                       //是否保存到相册
              sourceType: this.camera.PictureSourceType.CAMERA ,         //是打开相机拍照还是打开相册选择  PHOTOLIBRARY : 相册选择, CAMERA : 拍照,
            }
        
            this.camera.getPicture(options).then((imageData) => {
              console.log("got file: " + imageData);
        
              // If it's base64:
              let base64Image = 'data:image/jpeg;base64,' + imageData;
              // this.path = base64Image;
              // alert(base64Image);
              this.cameraUpload = base64Image;
              //If it's file URI
              // this.path = imageData;
        
              // this.upload();
        
            }, (err) => {
              // Handle error
              console.log('err', err);
            });
          }
        },{
          text: '本地上传',
          handler: () => {
            console.log('Archive clicked');
            const options: ImagePickerOptions = {
              maximumImagesCount: 6,
              // width: IMAGE_SIZE,
              // height: IMAGE_SIZE,
              quality: 100
            };
            
            // 获取图片
            this.imagePicker.getPictures(options).then((results) => {
              for (var i = 0; i < results.length; i++) {
                  console.log('Image URI: ' + results[i]);
                  // this.cameraUpload = results[i];
              }
              // alert()
              this.cameraUpload = results[0];
            }, (err) => {
              console.log('获取图片失败');
              alert(err);
            });
            
          }
        },{
          text: '取消',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    actionSheet.present();
  }
  renchenClick(id){
    for(let i = 0;i < this.renchenList.length;i++){
      this.renchenList[i].flag = false;
    }
    this.renchenList[id - 1].flag = true;
    // this.renchenModal = false;
  }
  closeYunqiPush(){
    this.yunqiPush = false;
  }
  closeRenchengModal(){
    this.renchenModal = false;
  }
  toKnowledgePage(i) {
    console.log(i)
    switch (i) {
      case 0:
        this.navCtrl.push(KnowledgesPage);
        break;
      case 1:
        this.navCtrl.push(RecipesPage);
        break;
      case 2:
        this.navCtrl.push(GadgetPage);
        break;
      case 3:
        this.navCtrl.push(FaqPage);
        break;
      default:
        break;
    }
  }
  toYunZao(){
    this.navCtrl.push(KnowledgesPage)
  }
  toKno(){
    this.navCtrl.push(KnowledgeDetailPage);
  }
  toWenjuan(id){
    console.log(id);
    if(id == 1){
      // this.hasBaby = true;
      this.navCtrl.push(QuestionnairePage);
    }
  }
  closeModal(){
    this.pushModal = false;
  }
  toWriteHeight(){
    this.navCtrl.push(ChangeBabyHeightPage);
    // this.navCtrl.push(QuestionnairePage);
  }
  toTest(){
    this.navCtrl.push(QuestionnairePage);
  }
  toRecord(){

  }
  toWenjuan3(){
    this.navCtrl.push(QuestionnaireGdmPage);
  }
  weiyangType(){
    this.navCtrl.push(QuestionnaireFeeddingTypePage);
  }
  chanegBaby(){
    this.hasBaby = true;
  }
}
