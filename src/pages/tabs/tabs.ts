import { Component } from '@angular/core';

import { HomePage } from './home/home';
import { ConcatPage } from './concat/concat';
import { MinePage } from './mine/mine';
import { RecordPage } from './record/record';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = ConcatPage;
  tab3Root = RecordPage;
  tab4Root = MinePage;

  constructor() {
    console.log('cons');
  }
  ngOnInit() {
  }
}
