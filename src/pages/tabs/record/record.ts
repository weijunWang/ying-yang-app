import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { DietPage } from '../../content/diet/diet';
import { DeedsPage } from '../../content/deeds/deeds';
import { MotionPage } from '../../content/motion/motion';
import { WeightCurvePage } from '../../content/weight-curve/weight-curve';

@Component({
  selector: 'page-record',
  templateUrl: 'record.html'
})
export class RecordPage {
  keyword: string = '';
  placeholder: string = '孕早期妈妈都在做什么......';
  listData: any;
  retieList: any;
  constructor(public navCtrl: NavController) {

  }
  ngOnInit() {
    console.log('record 记录');
    this.listData = [
      {
        url: '../../../assets/imgs/record-icon1.png',
        title: '体重',
        text: '记录体重，及时关注身体…'
      }, {
        url: '../../../assets/imgs/record-icon2.png',
        title: '膳食',
        text: '记录体重，及时关注身体…'
      }, {
        url: '../../../assets/imgs/record-icon3.png',
        title: '运动',
        text: '记录体重，及时关注身体…',
      },{
        url: '../../../assets/imgs/record-icon4.png',
        title: '事记',
        text: '记录体重，及时关注身体…',
      }
    ]
  }
  toPage(i){
    console.log(i);
    switch(i){
      case 0:
        this.navCtrl.push(WeightCurvePage);
        break;
      case 1:
        this.navCtrl.push(DietPage);
        break;
      case 2:
        this.navCtrl.push(MotionPage);
        break;
      case 3:
        this.navCtrl.push(DeedsPage);
        break;
      default: 
        break;
    }
  }
}
