import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FeedingPage } from '../../content/feeding/feeding';
import { ExchangePointsPage } from '../../content/exchange-points/exchange-points';
import { NewsPage } from '../../content/news/news';
import { NotificationPage } from '../../content/notification/notification';
import { ContactUsPage } from '../../content/contact-us/contact-us';
import { SettingPage } from '../../content/setting/setting';

@Component({
  selector: 'page-mine',
  templateUrl: 'mine.html'
})
export class MinePage {
  listData: any;
  constructor(public navCtrl: NavController) {

  }
  ngOnInit(){
    console.log('ngOnInit');
    this.listData = [
      {
        id: 1,
        icon: '../../../assets/imgs/mine-icon1.png',
        text: '喂养方式'
      },{
        id: 2,
        icon: '../../../assets/imgs/mine-icon2.png',
        text: '我的积分'
      },{
        id: 3,
        icon: '../../../assets/imgs/mine-icon3.png',
        text: '消息通知'
      },{
        id: 4,
        icon: '../../../assets/imgs/mine-icon4.png',
        text: '我的收藏'
      },{
        id: 5,
        icon: '../../../assets/imgs/mine-icon5.png',
        text: '联系我们'
      },{
        id: 6,
        icon: '../../../assets/imgs/mine-icon6.png',
        text: '设置'
      }
    ]
  }
  toPage(id){
    switch(id){
      case 1:
       this.navCtrl.push(FeedingPage);
       break;
      case 2:
       this.navCtrl.push(ExchangePointsPage);
       break;
      case 3:
       this.navCtrl.push(NotificationPage);
       break;
      case 4:
       this.navCtrl.push(NewsPage);
       break;
      case 5:
       this.navCtrl.push(ContactUsPage);
       break;
      case 6:
       this.navCtrl.push(SettingPage);
       break;

      default: 
        break;
    }
  }
}
