import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { ContactSearchPage } from '../../content/contact-search/contact-search';
import { SuccessStoryPage } from '../../content/success-story/success-story';
import { PostsPage } from '../../content/posts/posts';
import { PostingPage } from '../../content/posting/posting';
import { ActivityDetailPage } from '../../content/activity-detail/activity-detail';

@Component({
  selector: 'page-concat',
  templateUrl: 'concat.html'
})
export class ConcatPage {
  keyword: string = '';
  placeholder: string = '孕早期妈妈都在做什么......';
  listData: any;
  retieList: any;
  cardData: any;
  constructor(
    public navCtrl: NavController,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {

  }
  ngOnInit(){
    this.listData = [
      {
        url: '../../../assets/imgs/demo.png',
        text: '功能'
      },{
        url: '../../../assets/imgs/demo.png',
        text: '功能'
      },{
        url: '../../../assets/imgs/demo.png',
        text: '功能'
      }
    ]
    this.retieList = [
      {
        url: '../../../assets/imgs/demo.png'
      },{
        url: '../../../assets/imgs/demo.png'
      }
    ]
    this.cardData = [
      {
        id: 1,
        title: '孕育交流',
        text: '产检—与宝宝交流的奇妙时光',
        img: '../../../assets/imgs/concat-img1.png'
      },{
        id: 2,
        title: '成功故事分享',
        text: '我的孕育时光',
        img: '../../../assets/imgs/concat-img2.png'
      }
    ]
  }
  toExchange(){
    console.log('兑换');
    this.navCtrl.push(ActivityDetailPage);
  }
  focusInput(){
    this.navCtrl.push(ContactSearchPage);
  }
  toYunZao(id){
    if(id == 1){
      this.navCtrl.push(PostsPage);
    }else{
      this.navCtrl.push(SuccessStoryPage);
    }
  }
  toPointing(){
    this.navCtrl.push(PostingPage);
  }
}
