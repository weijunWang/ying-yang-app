import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GestationPage } from '../gestation/gestation';
// import { HasChildPage } from '../has-child/has-child';
import { HasGestationPage } from '../has-gestation/has-gestation';
import { BirthChildInfoPage } from '../content/birth-child-info/birth-child-info';
import { HttpServerProvider } from '../../providers/http-server/http-server';
import { StorageServeProvider } from '../../providers/storage-serve/storage-serve';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {
  listData: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider) {
  }

  ngOnInit() {
    this.listData = [
      {
        text: '备孕',
        hide: true
      }, {
        text: '孕育中',
        hide: true
      }, {
        text: '有宝宝',
        hide: true
      }
    ]
    console.log('ionViewDidLoad RegisterPage');
  }
  toNextPage(i) {
    let that = this;
    for (let item = 0; item < this.listData.length; item++) {
      this.listData[item].hide = true;
    }
    this.listData[i].hide = false;
    switch (i) {
      case 0:
        this.http.patch('/api/userinfo', {
          life_stage: "first",
          has_child: 'false',
        }, function (res) {
          console.log(res);
          if (res) {
            that.navCtrl.push(HasGestationPage);
          }
        }, function (err) {
          console.log(err);
        })
        break;
      case 1:
        this.http.patch('/api/userinfo', {
          life_stage: "second",
          has_child: 'false',
        }, function (res) {
          console.log(res);
          if (res) {
            that.navCtrl.push(GestationPage);
          }
        }, function (err) {
          console.log(err);
        })
        break;
      case 2:
        this.http.patch('/api/userinfo', {
          life_stage: "third",
          has_child: 'true',
        }, function (res) {
          console.log(res);
          if (res) {
            that.navCtrl.push(BirthChildInfoPage);
          }
        }, function (err) {
          console.log(err);
        })
        break;
    }
  }
}
