import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PhysicalDataPage } from './physical-data';

@NgModule({
  declarations: [
    PhysicalDataPage,
  ],
  imports: [
    IonicPageModule.forChild(PhysicalDataPage),
  ],
})
export class PhysicalDataPageModule {}
