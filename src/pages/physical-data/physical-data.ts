import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServersProvider } from '../../providers/data-servers/data-servers';
// import { TabsPage } from '../tabs/tabs';
import { PrivacyPolicyPage } from '../content/privacy-policy/privacy-policy';
import { HttpServerProvider } from '../../providers/http-server/http-server';

@IonicPage()
@Component({
  selector: 'page-physical-data',
  templateUrl: 'physical-data.html',
})
export class PhysicalDataPage {
  ionTitle: string = '';


  choice: number = 1;
  swipe: number = 0;
  offsetX: number = 0;
  moveX = 0;
  moveBefore = 0;
  unit = 0.65;
  move: number = 0;
  rulerLen: any;
  numText: any = 50.0;
  rulers: any;

  // 身高
  heights: any;
  heightNum: number;
  rulerHeight: number;
  offsetY: number;
  YmoveBefore: number = 0;
  Ymove: number = 0;
  moveY: number = 0;
  Yunit = 0.50;
  constructor(
    private dataServer: DataServersProvider, 
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: HttpServerProvider) {
  }

  ionViewDidLoad() {
    if(this.navParams.get('id') == 2){  
      this.ionTitle = '身高'
    }else{
      this.ionTitle = '孕前数据'
    }
    console.log('ionViewDidLoad PhysicalDataPage');
    this.rulerLen = -32.5;
    this.heightNum = 160.0;
    this.heights = this.dataServer.getChiHeights();
    this.rulers = this.dataServer.getChiWeights();
  }
  heighTouchstart(e) {
    this.offsetY = e.touches[0].clientY;
    this.YmoveBefore = 0;
    // console.log(e);
  }
  heightTouchmove(e) {
    // console.log(e);
    let ruler = document.getElementById('height-ruler');
    this.Ymove = e.touches[0].clientY;
    // console.log(ruler.dataset.offset);
    let offset: any = ruler.dataset.offset;
    offset = parseFloat(offset);
    let tempMove = 0;
    let len: any = 0;

    tempMove = this.Ymove - this.offsetY;
    tempMove /= 10;
    // console.log(tempMove);
    // //计算两次滑动间的距离
    len = offset + (tempMove - this.YmoveBefore);
    len = parseFloat(len);
    console.log(len);
    // //边界判断，最大偏移长度65rem
    if (len - 0.0 < 30 && len > -30) {
      //将结果保存下来，下一次滑动时取出参与计算
      this.moveY = tempMove;
      ruler.dataset.offset = len;
      this.YmoveBefore = this.moveY;
      //设置样式
      this.rulerHeight = len
      // console.log(len);
      // //显示刻度，保留1位小数
      let heightNum = 160.0 - Number((len / this.Yunit).toFixed(1));
      this.heightNum = Number(heightNum.toFixed(1));
    }
  }
  touchstart(e) {
    // console.log(e);
    this.offsetX = e.touches[0].clientX;
    this.moveBefore = 0;
  }
  touchmove(e) {
    // console.log(e)
    let ruler = document.getElementById('ruler');
    this.move = e.touches[0].clientX;

    let offset: any = ruler.dataset.offset;
    offset = parseFloat(offset);
    let tempMove = 0;
    let len: any = 0;
    len = offset + (tempMove - this.moveBefore);
    len = parseFloat(len);

    tempMove = this.move - this.offsetX;
    tempMove /= 10;
    //计算两次滑动间的距离
    len = offset + (tempMove - this.moveBefore);
    len = parseFloat(len);
    //边界判断，最大偏移长度65rem
    if (len - 0.0 < 0 && len > -70) {
      //将结果保存下来，下一次滑动时取出参与计算
      this.moveX = tempMove;
      ruler.dataset.offset = len;
      this.moveBefore = this.moveX;
      //设置样式
      this.rulerLen = len
      // //显示刻度，保留1位小数
      this.numText = -((len / this.unit).toFixed(1));
    }
  }
  nextStep() {
    this.choice = 2;
    if(this.navParams.get('id') == 2){  
      this.ionTitle = '体重'
    }
  }
  confirm() {
    let data = {
      height: this.heightNum.toString(),
      weight: this.numText.toString()
    }
    let that = this;
    console.log(data);
    
    this.http.patch('/api/userinfo', data, function (res) {
      console.log(res);
      if (res) {
        that.navCtrl.push(PrivacyPolicyPage, {
          params: 0
        });
      }
    }, function (err) {
      console.log(err);
    })
    // this.navCtrl.parent.select(2);
  }
}
