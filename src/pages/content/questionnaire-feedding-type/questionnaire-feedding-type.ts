import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-questionnaire-feedding-type',
  templateUrl: 'questionnaire-feedding-type.html',
})
export class QuestionnaireFeeddingTypePage {
    renchenList: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {
    this.renchenList = [
        {
          id: 1,
          flag: false,
          name: '母乳喂养'
        },{
          id: 2,
          flag: false,
          name: '奶粉喂养'
        },{
          id: 3,
          flag: false,
          name: '混合喂养'
        }
      ]
  }
  renchenClick(id){
    for(let i = 0;i < this.renchenList.length;i++){
      this.renchenList[i].flag = false;
    }
    this.renchenList[id - 1].flag = true;
    // this.renchenModal = false;
  }
  confirm(){
    this.navCtrl.pop();
  }
}