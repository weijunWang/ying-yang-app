import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireFeeddingTypePage } from './questionnaire-feedding-type';

@NgModule({
  declarations: [
    QuestionnaireFeeddingTypePage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireFeeddingTypePage),
  ],
})
export class QuestionnaireFeeddingTypePageModule {}