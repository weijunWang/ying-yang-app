import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-questionnaire',
  templateUrl: 'questionnaire.html',
})
export class QuestionnairePage {
  listData: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {
    this.listData = [
      {
        img: '../../../assets/imgs/no-choose.png',
        text: '年龄≥35岁',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: '孕前BMI≥24',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: '前一胎巨大儿（胎儿出生体重≥4000g）',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: 'GDM史',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: '孕前糖耐量受损',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: '1级亲属（父母，同父母兄弟姐妹）中患2型糖尿病',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: '多囊卵巢综合征史',
        flag: false
      },{
        img: '../../../assets/imgs/no-choose.png',
        text: '以上都无',
        flag: false
      }
    ]
  }
  choose(i){
    console.log(i)
    if(this.listData[i].flag == false){
      this.listData[i].img = '../../../assets/imgs/choose.png';
      this.listData[i].flag = true
    }else{
      this.listData[i].img = '../../../assets/imgs/no-choose.png';
      this.listData[i].flag = false
    }
  }
  confirm(){
    this.navCtrl.pop();
  }
}