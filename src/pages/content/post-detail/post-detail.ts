import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-post-detail',
  templateUrl: 'post-detail.html',
})
export class PostDetailPage {
  imgList: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
      this.imgList = [
        {
          img: '../../../assets/imgs/post-img.png'
        },{
          img: '../../../assets/imgs/post-img.png'
        },{
          img: '../../../assets/imgs/post-img.png'
        }
      ]
    }
}