import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SuccessStoryDetailPage } from './success-story-detail';

@NgModule({
  declarations: [
    SuccessStoryDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(SuccessStoryDetailPage),
  ],
})
export class SuccessStoryDetailPageModule {}