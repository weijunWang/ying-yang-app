import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-success-story-detail',
  templateUrl: 'success-story-detail.html',
})
export class SuccessStoryDetailPage {
  allData: object[];
  pageId: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit(){
    this.pageId = this.navParams.data.id;
    console.log('pageId',this.pageId);
    this.allData = [
        {
          id: 1,
          text: "孕早期禁食有哪些？"
        },{
          id: 2,
          text: "孕期运动"
        },{
          id: 3,
          text: "如何进行宝宝胎教，做哪些事情比较好"
        },{
          id: 4,
          text: "妊娠期"
        }
      ]
  }
}