import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { BirthsSeizedItemPage } from '../births-seized-item/births-seized-item';
import { SetRemindPage } from '../set-remind/set-remind';

@IonicPage()
@Component({
  selector: 'page-births-seized-time',
  templateUrl: 'births-seized-time.html',
})
export class BirthsSeizedTimePage {
    listData: any;
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit(){
        let that = this;
        this.http.get('/api/production_schedule', {}, function (res) {
            console.log(res);
            if (res.data) {
                that.listData = res.data;
            }
        })
        this.listData = [
            {
                id: 1,
                status: '8-12周',
                title: '初诊建卡',
                content: '建立“孕妇健康手册”档案，每次孕检建立“孕妇健康手册”档案，每次孕检'
            },{
                id: 2,
                status: '8-12周',
                title: '初诊建卡',
                content: '建立“孕妇健康手册”档案，每次孕检建立“孕妇健康手册”档案，每次孕检'
            },{
                id: 3,
                status: '8-12周',
                title: '初诊建卡',
                content: '建立“孕妇健康手册”档案，每次孕检建立“孕妇健康手册”档案，每次孕检'
            },{
                id: 4,
                status: '8-12周',
                title: '初诊建卡',
                content: '建立“孕妇健康手册”档案，每次孕检建立“孕妇健康手册”档案，每次孕检'
            },{
                id: 5,
                status: '8-12周',
                title: '初诊建卡',
                content: '建立“孕妇健康手册”档案，每次孕检建立“孕妇健康手册”档案，每次孕检'
            }
        ]
    }
    toBirthDetail(id){
        this.navCtrl.push(BirthsSeizedItemPage, {id: id ? id : 1});
    }
    setRemind(){
        this.navCtrl.push(SetRemindPage);
    }
}