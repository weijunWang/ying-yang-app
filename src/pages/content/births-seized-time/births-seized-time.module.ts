import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BirthsSeizedTimePage } from './births-seized-time';

@NgModule({
  declarations: [
    BirthsSeizedTimePage,
  ],
  imports: [
    IonicPageModule.forChild(BirthsSeizedTimePage),
  ],
})
export class BirthsSeizedTimePageModule {}