import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditHawPage } from './edit-haw';

@NgModule({
  declarations: [
    EditHawPage,
  ],
  imports: [
    IonicPageModule.forChild(EditHawPage),
  ],
})
export class EditHawPageModule {}