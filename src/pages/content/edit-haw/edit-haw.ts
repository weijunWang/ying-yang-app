import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-edit-haw',
  templateUrl: 'edit-haw.html',
})
export class EditHawPage {
    forList: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
        this.forList = [
            {
                title: '家里',
                flag: false
            },{
                title: '餐馆',
                flag: true
            },{
                title: '路边摊',
                flag: false
            }
        ]
    }
    save(){
        console.log('保存');
    }
}