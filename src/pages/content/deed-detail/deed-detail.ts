import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
// import { PostDetailPage } from '../post-detail/post-detail';

@IonicPage()
@Component({
  selector: 'page-deed-detail',
  templateUrl: 'deed-detail.html',
})
export class DeedDetailPage {
    imgList: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
      this.imgList = [
        {
          img: '../../../assets/imgs/post-img.png'
        }
      ]
    }
    toPointing(){
    //   this.navCtrl.push(PostDetailPage);
    }
}