import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeedDetailPage } from './deed-detail';

@NgModule({
  declarations: [
    DeedDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(DeedDetailPage),
  ],
})
export class DeedDetailPageModule {}