import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActionSheetController } from 'ionic-angular';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { PhysicalDataPage } from '../../physical-data/physical-data';
import { HttpServerProvider } from '../../../providers/http-server/http-server';

@IonicPage()
@Component({
    selector: 'page-birth-child-info',
    templateUrl: 'birth-child-info.html',
})
export class BirthChildInfoPage {
    isBoy: boolean = false;
    isGirl: boolean = false;
    simpleColumns: any;
    babyHeight: any;
    babyWeight: any;
    type: any;
    birthType: any;
    babyHeights: any;
    babyWeights: any;
    chidlDate: any;
    types: any;
    childName: string;
    birthTypes: any;
    constructor(
        private dataServer: DataServersProvider,
        public navCtrl: NavController,
        public navParams: NavParams,
        public actionSheetCtrl: ActionSheetController,
        public http: HttpServerProvider) { }
    ngOnInit() {
        this.babyHeights = this.dataServer.getHeightsData();
        this.babyWeights = this.dataServer.getWeightsData();
        this.types = this.dataServer.getFeedType();
        this.birthTypes = this.dataServer.getBirthTypes();
    }
    checkBoy() {
        this.isGirl = false;
        this.isBoy = true;
    }
    checkGirl() {
        this.isGirl = true;
        this.isBoy = false;
    }
    confirm() {
        let that = this;
        let data = {
            name: this.childName,
            sex: this.isBoy ? 'boy' : 'girl',
            birth: this.chidlDate,
            height: this.dataServer.deleteSpace(this.babyHeight),
            weight: this.dataServer.deleteSpace(this.babyWeight),
            feeding_patterns: this.type,
            delivery_mode: this.birthType,
        }
        console.log(data);
        this.http.patch('/api/child_info', data, function (res) {
            console.log(res);
            if (res) {
                that.navCtrl.push(PhysicalDataPage, { id: 2 });
            }
        }, function (err) {
            console.log(err);
        })
    }
}