import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BirthChildInfoPage } from './birth-child-info';

@NgModule({
  declarations: [
    BirthChildInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(BirthChildInfoPage),
  ],
})
export class BirthChildInfoModule {}