import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordDeedPage } from './record-deed';

@NgModule({
  declarations: [
    RecordDeedPage,
  ],
  imports: [
    IonicPageModule.forChild(RecordDeedPage),
  ],
})
export class RecordDeedPageModule {}