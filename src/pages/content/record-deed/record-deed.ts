import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-record-deed',
  templateUrl: 'record-deed.html',
})
export class RecordDeedPage {
    fileModel: any;
    fileList: any;
    allData: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
      this.fileList = [];
      this.allData = [
        {
          text: '孕期运动'
        },{
          text: '孕期运动'
        }
      ]
    }
    changepic() {
        // let reads= new FileReader();
        // let f=document.getElementById('file').files[0];
        // reads.readAsDataURL(f);
        // reads.onload=function (e) {
        //     document.getElementById('show').src=this.result;
        // };
      }
    inpChange(){
      // console.log(this.fileModel);
      // console.log(document.getElementById('file').files[0]);
      let that = this;
      let reads= new FileReader();
      let f = (<HTMLInputElement>document.getElementById('file')).files[0];
      reads.readAsDataURL(f);
      reads.onload=function (e) {
          that.fileList.push({
            img: this.result
          });
      };
    }
    
}