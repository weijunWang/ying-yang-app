import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { RecordDeedPage } from '../record-deed/record-deed';
import { DeedDetailPage } from '../deed-detail/deed-detail';

@IonicPage()
@Component({
    selector: 'page-deeds',
    templateUrl: 'deeds.html',
})
export class DeedsPage {
    img_data: any = [{
        id: 1,
        canEdit: true,
        title: '孕迹时光',
        status: '孕迹时光',
        dateTime: '2018年10月20日',
        bgImg: '../../../assets/imgs/shiji.png',
        accumulatePoints: '10',
    }];
    img_data1: any = [{
        id: 1,
        canEdit: true,
        title: '孕迹时光',
        status: '孕迹时光',
        dateTime: '2018年10月20日',
        bgImg: '../../../assets/imgs/shiji.png',
        accumulatePoints: '10',
    }];
    img_data2: any = [{
        id: 1,
        canEdit: true,
        title: '孕迹时光',
        status: '孕迹时光',
        dateTime: '2018年10月20日',
        bgImg: '../../../assets/imgs/shiji.png',
        accumulatePoints: '10',
    }];
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit() {
        let that = this;
        this.http.get('/api/deeds/', {}, function (res) {
            console.log(res);
            if (res) {
                that.img_data = res.data;
                that.img_data1 = [];
                that.img_data2 = [];
                for (let i = 0; i < that.img_data.length; i++) {
                    if (i % 2 == 0) {
                        that.img_data1.push(that.img_data[i])
                    } else {
                        that.img_data2.push(that.img_data[i])
                    }
                }
                // console.log();
                // let box_img = document.getElementsByClassName('box_img');
                // console.log(box_img);
            }
        })
        // this.getNode();
        // this.img_data1 = [];
        // this.img_data2 = [];
        // this.img_data = [
        //     {   
        //         id: 1,
        //         canEdit: true,
        //         title: '孕迹时光',
        //         status: '孕迹时光',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/shiji.png',
        //         accumulatePoints: '10',
        //     },{
        //         id: 2,
        //         canEdit: false,
        //         title: '第一次“打招呼”',
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/shiji.png',
        //         accumulatePoints: '10',
        //     },{
        //         id: 3,
        //         canEdit: true,
        //         title: '给宝宝讲故事',
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/shiji.png',
        //         accumulatePoints: '10',
        //     },{
        //         id: 4,
        //         canEdit: false,
        //         title: '给宝宝讲故事',
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/shiji.png',
        //         accumulatePoints: '10',
        //     },{
        //         id: 5,
        //         canEdit: false,
        //         title: '给宝宝讲故事',
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     },{
        //         id: 6,
        //         canEdit: true,
        //         title: '给宝宝讲故事',
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }
        // ]
        // for(let i = 0;i < this.img_data.length;i++){
        //     if(i % 2 == 0){
        //         this.img_data1.push(this.img_data[i])
        //     }else{
        //         this.img_data2.push(this.img_data[i])
        //     }
        // }
        // let box_img = document.getElementsByClassName('box_img');
        // console.log(box_img);
    }
    toPage(id) {
        if (id == 1) {
            this.navCtrl.push(RecordDeedPage);
        } else {
            this.navCtrl.push(DeedDetailPage);
        }
    }
}