import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DeedsPage } from './deeds';

@NgModule({
  declarations: [
    DeedsPage,
  ],
  imports: [
    IonicPageModule.forChild(DeedsPage),
  ],
})
export class DeedsPageModule {}