import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-notification',
  templateUrl: 'notification.html',
})
export class NotificationPage {
    items: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit() {
        this.items = [
            { id: 1, title: 'item1' },
            { id: 2, title: 'item2' },
            { id: 3, title: 'item3' },
            { id: 4, title: 'item4' },
            { id: 5, title: 'item5' },
            { id: 6, title: 'item6' }
        ];
    }
    removeItem(item){
        console.log(item);
    }
}