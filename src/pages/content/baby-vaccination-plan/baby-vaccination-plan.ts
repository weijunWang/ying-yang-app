import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
  selector: 'page-baby-vaccination-plan',
  templateUrl: 'baby-vaccination-plan.html',
})
export class BabyVaccinationPlanPage {
    listData: any;
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit(){
        let that = this;
        this.http.get('/api/vaccination_program/', {}, function (res) {
            console.log(res);
            if (res) {
            }
        })
        this.listData = [
            {
                status: '24小时内',
                title: '卡介苗第一针',
                content: '可预防乙型病毒性肝炎',
                is: true
            },{
                status: '24小时内',
                title: '卡介苗第一针',
                content: '可预防乙型病毒性肝炎',
                is: false
            },{
                status: '24小时内',
                title: '卡介苗第一针',
                content: '可预防乙型病毒性肝炎',
                is: false
            }
        ]
    }
}