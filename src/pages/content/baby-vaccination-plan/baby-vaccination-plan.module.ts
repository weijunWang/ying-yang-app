import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BabyVaccinationPlanPage } from './baby-vaccination-plan';

@NgModule({
  declarations: [
    BabyVaccinationPlanPage,
  ],
  imports: [
    IonicPageModule.forChild(BabyVaccinationPlanPage),
  ],
})
export class BabyVaccinationPlanPageModule {}