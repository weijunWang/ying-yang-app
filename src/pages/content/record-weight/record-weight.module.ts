import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordWeightPage } from './record-weight';

@NgModule({
  declarations: [
    RecordWeightPage,
  ],
  imports: [
    IonicPageModule.forChild(RecordWeightPage),
  ],
})
export class RecordWeightPageModule {}