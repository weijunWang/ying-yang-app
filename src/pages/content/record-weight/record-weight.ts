import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { CalendarComponentOptions } from 'ion2-calendar';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
    selector: 'page-record-weight',
    templateUrl: 'record-weight.html',
})
export class RecordWeightPage {
    dateRange: { from: string; to: string; };
    type: 'string';
    optionsRange: CalendarComponentOptions = {
        from: new Date(2011, 1, 7),
        monthFormat: 'YYYY 年 MM 月 ',
        weekdays: ['日', '一', '二', '三', '四', '五', '六'],
    };

    choice: number = 1;
    swipe: number = 0;
    offsetX: number = 0;
    moveX = 0;
    moveBefore = 0;
    unit = 0.65;
    move: number = 0;
    rulerLen: -32.5;
    numText: any = 50.0;
    rulers: any;
    constructor(private dataServer: DataServersProvider, public navCtrl: NavController, public navParams: NavParams) {
      // this.openCalendar();
    }
    // openCalendar() {
    //   const options: CalendarModalOptions = {
    //     title: 'BASIC',
    //   };
    //   let myCalendar =  this.modalCtrl.create(CalendarModal, {
    //     options: options
    //   });
   
    //   myCalendar.present();
   
    //   myCalendar.onDidDismiss((date: CalendarResult, type: string) => {
    //     console.log(date);
    //   })
    // }
    ngOnInit(){
        this.rulerLen = -32.5;
        this.rulers = this.dataServer.getChiWeights();
    }
    onChange($event){
        console.log($event);
    }
    touchstart(e){
        // console.log(e);
        this.offsetX = e.touches[0].clientX;
        this.moveBefore = 0;
      }
      touchmove(e){
        // console.log(e)
        let ruler = document.getElementById('ruler');
        this.move = e.touches[0].clientX;
    
        let offset: any = ruler.dataset.offset;
        offset = parseFloat(offset);
        let tempMove = 0;
        let len: any = 0;
        len = offset + (tempMove - this.moveBefore);
        len = parseFloat(len);
    
        tempMove = this.move - this.offsetX;
        tempMove /= 10;
        //计算两次滑动间的距离
        len = offset + (tempMove - this.moveBefore);
        len = parseFloat(len);
        //边界判断，最大偏移长度65rem
        if (len - 0.0 < 0 && len > -65) {
            //将结果保存下来，下一次滑动时取出参与计算
            this.moveX = tempMove;
            ruler.dataset.offset = len;
            this.moveBefore = this.moveX;
            //设置样式
            this.rulerLen = len
            // //显示刻度，保留1位小数
            this.numText = -((len / this.unit).toFixed(1));
        }
    }
    saveNum(){
        let data = {
            weight: this.numText
        }
        this.navCtrl.pop();
        console.log(data);
    }
}