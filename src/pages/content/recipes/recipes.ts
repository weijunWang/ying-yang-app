import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RecipesOnedayPage } from '../recipes-oneday/recipes-oneday';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';

@IonicPage()
@Component({
  selector: 'page-recipes',
  templateUrl: 'recipes.html',
})
export class RecipesPage {
  listData: any = [
    {
      "title": "",
      "body": "",
      "detail": "",
      "img": "",
      "id": 1,
      "status": ""
    }
  ];
  text: string;
  listText: any;
  userinfo: any;
  stageStatus: any;
  isChoiced: boolean = false;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider) {
  }
  ngOnInit() {
    let that = this;
    this.userinfo = this.storage.getStorage('userinfo');
    this.stageStatus = ['孕早期', '孕中期', '孕晚期', 'GDM', '其他', '母乳喂养', '喂食喂养'];
    if(this.userinfo.life_stage == 'first'){
      this.text = this.stageStatus[0];
    }else if(this.userinfo.life_stage == 'second'){
      this.text = this.stageStatus[1];
    }else{
      this.text = this.stageStatus[5];
    }
    this.http.get('/api/recipe_list', {}, function (res) {
      console.log(res);
      if (res) {
        
      }
    })
    this.listText = [
      {
        isChoice: true,
        text: '孕早期'
      }, {
        isChoice: false,
        text: '孕中期'
      }, {
        isChoice: false,
        text: '孕晚期'
      }, {
        isChoice: false,
        text: 'GDM食谱'
      }
    ]
    this.listData = [
      {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img2.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }
    ]
  }
  modelAlert() {
    this.isChoiced = true;
  }
  toPage(i) {
    this.navCtrl.push(RecipesOnedayPage);
  }
  clickItem(index) {
    for (let i = 0; i < this.listText.length; i++) {
      this.listText[i].isChoice = false;
    }
    this.listText[index].isChoice = true;
  }
  clickClose() {
    this.isChoiced = false;
  }
}