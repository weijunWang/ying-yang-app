import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { KnowledgeDetailPage } from '../knowledge-detail/knowledge-detail'


@IonicPage()
@Component({
  selector: 'page-knowledge-base',
  templateUrl: 'knowledge-base.html',
})
export class KnowledgeBasePage {
  allData: any = [];
  keyword = "";
  storgeData: any;
  userinfo: any;
  searching: boolean = false;;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider) {
  }

  ngOnInit() {
    console.log('knowledgeArticleTitle', this.storage.getStorage('knowledgeArticleTitle'));
    this.storgeData = this.storage.getStorage('knowledgeArticleTitle');
    let that = this;
    this.userinfo = this.storage.getStorage('userinfo');
    this.http.get('/api/get_articles', {}, function (res) {
      console.log(res);
      if (res) {
        
      }
    })
    console.log('ionViewDidLoad KnowledgeBasePage');
  }
  toPage(id) {
    this.navCtrl.push(KnowledgeDetailPage, {id: id});
  }
  inputFocus() {
    if (this.keyword != '') {
      this.searching = true;
    }
  }
  search(){
    let that = this;
    let storageArr = this.storage.getStorage('knowledgeArticleTitle') ? this.storage.getStorage('knowledgeArticleTitle') : [];
    storageArr.push({text: this.keyword})
    this.storage.setStorage('knowledgeArticleTitle', storageArr);
    this.searching = true;
      let data = {
        "keyword": this.keyword
      }
      this.http.post('/api/search_articles/', data, function (res) {
        console.log(res);
        if (res) {
          that.allData = res;
        }
      }, function(err){
        console.log(err);
      }) 
  }
  clearAll() {
    this.storage.removeStorage('knowledgeArticleTitle')
    console.log('knowledgeArticleTitle', this.storage.getStorage('knowledgeArticleTitle'));
    this.storgeData = [];
  }
}