import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { FaqDetailPage } from '../faq-detail/faq-detail';

@IonicPage()
@Component({
  selector: 'page-faq',
  templateUrl: 'faq.html',
})
export class FaqPage {
  listData: any = [];
  keyword: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {
  }
  ngOnInit() {
    let that = this;
    this.http.get('/api/faq/fstatus', {}, function (res) {
      console.log(res);
      if (res) {
        that.listData = res.faq;
      }
    })
    // this.listData = [
    //   {
    //     id: 1,
    //     title: '孕早期有哪些注意事项呢？',
    //     body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
    //   }, {
    //     id: 2,
    //     title: '孕早期有哪些注意事项呢？',
    //     body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
    //   }, {
    //     id: 3,
    //     title: '孕早期有哪些注意事项呢？',
    //     body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
    //   }
    // ]
  }
  toPage(id) {
    this.navCtrl.push(FaqDetailPage);
  }
  focusInput() {
    console.log(this.keyword);
  }
  searchFaq(){
    let that = this;
    let data = {
      "stage": "fstatus",
      "search_title": this.keyword
    }
    console.log(data);
    this.http.post('/api/search_faq/', data, function (res) {
      console.log(res);
      if(res.faq){
        that.listData = res.faq;
      }
    }, function (err) {
      console.log(err);
    })
  }
}