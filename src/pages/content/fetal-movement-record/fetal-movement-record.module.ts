import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FetalMovementRecordPage } from './fetal-movement-record';

@NgModule({
  declarations: [
    FetalMovementRecordPage,
  ],
  imports: [
    IonicPageModule.forChild(FetalMovementRecordPage),
  ],
})
export class FetalMovementRecordModule {}