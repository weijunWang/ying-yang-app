import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-fetal-movement-record',
  templateUrl: 'fetal-movement-record.html',
})
export class FetalMovementRecordPage {
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
}