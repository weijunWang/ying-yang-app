import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordDietPage } from './record-diet';

@NgModule({
  declarations: [
    RecordDietPage,
  ],
  imports: [
    IonicPageModule.forChild(RecordDietPage),
  ],
})
export class RecipesOnedayModule {}