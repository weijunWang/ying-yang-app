import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AddFoodPage } from '../add-food/add-food'

@IonicPage()
@Component({
  selector: 'page-record-diet',
  templateUrl: 'record-diet.html',
})
export class RecordDietPage {
    forList: any;
    isShow: boolean;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
        console.log(this.navParams.get('isShow'));
        this.isShow = this.navParams.get('isShow');
        this.forList = [
            {
                title: '家里',
                flag: false
            },{
                title: '餐馆',
                flag: true
            },{
                title: '路边摊',
                flag: false
            }
        ]
    }
    toAddFood(){
        console.log(this.navCtrl.push(AddFoodPage));
    }
}