import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { KnowledgeDetailPage } from '../knowledge-detail/knowledge-detail';

@IonicPage()
@Component({
  selector: 'page-early-pregnancy',
  templateUrl: 'early-pregnancy.html',
})
export class EarlyPregnancyPage {
    allData = null;
    keyword = "";
    listData: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    
    ngOnInit() {
      this.allData = [
        {
          id: 1,
          text: "孕早期禁食有哪些？"
        },{
          id: 2,
          text: "孕期运动"
        },{
          id: 3,
          text: "如何进行宝宝胎教，做哪些事情比较好"
        },{
          id: 4,
          text: "妊娠期"
        }
      ]
      this.listData = [
        {
          img: '../../../assets/imgs/article-img.png',
          title: '孕早期有哪些注意事项呢？'
        },{
          img: '../../../assets/imgs/article-img2.png',
          title: '孕早期有哪些注意事项呢？'
        },{
          img: '../../../assets/imgs/article-img3.png',
          title: '孕早期有哪些注意事项呢？'
        },{
          img: '../../../assets/imgs/article-img.png',
          title: '孕早期有哪些注意事项呢？'
        }
      ]
      console.log('EarlyPregnancyPage');
    }
    toDetailPage(item){
      console.log(item);
      this.navCtrl.push(KnowledgeDetailPage);
    }
}