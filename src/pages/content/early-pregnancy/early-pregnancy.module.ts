import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EarlyPregnancyPage } from './early-pregnancy';

@NgModule({
  declarations: [
    EarlyPregnancyPage,
  ],
  imports: [
    IonicPageModule.forChild(EarlyPregnancyPage),
  ],
})
export class KnowledgeBaseModule {}
