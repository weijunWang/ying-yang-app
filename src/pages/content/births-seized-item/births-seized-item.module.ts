import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BirthsSeizedItemPage } from './births-seized-item';

@NgModule({
  declarations: [
    BirthsSeizedItemPage,
  ],
  imports: [
    IonicPageModule.forChild(BirthsSeizedItemPage),
  ],
})
export class BirthsSeizedItemPageModule {}