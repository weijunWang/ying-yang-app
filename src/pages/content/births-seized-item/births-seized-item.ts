import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
  selector: 'page-births-seized-item',
  templateUrl: 'births-seized-item.html',
})
export class BirthsSeizedItemPage {
    allData: any;
    keyword = "";
    firstData: any;
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    
    ngOnInit() {
        let num = this.navParams.data.id;
        let that = this;
        // this.http.get('/api/production_schedule', {}, function (res) {
        //     console.log(res);
        //     if (res) {
        //         that.allData = res;
        //     }
        // })
        this.allData = [
            {
            id: 1,
            text: "孕早期禁食有哪些？"
            },{
            id: 2,
            text: "孕期运动"
            },{
            id: 3,
            text: "如何进行宝宝胎教，做哪些事情比较好"
            },{
            id: 4,
            text: "妊娠期"
            }
        ]
        this.firstData = [
            {
                text: '测血压'
            },{
                text: '测血压'
            },{
                text: '测血压'
            },{
                text: '测血压'
            }
        ]
      console.log('ionViewDidLoad KnowledgeDetailPage');
    }
}