import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrenatalEducationPage } from './prenatal-education';

@NgModule({
  declarations: [
    PrenatalEducationPage,
  ],
  imports: [
    IonicPageModule.forChild(PrenatalEducationPage),
  ],
})
export class PrenatalEducationModule {}