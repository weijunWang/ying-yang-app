import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
    selector: 'page-prenatal-education',
    templateUrl: 'prenatal-education.html',
})
export class PrenatalEducationPage {
    listData: any;
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit() {
        let that = this;
        this.http.get('/api/get_musics/', {}, function (res) {
            console.log(res);
            if (res) {
                //   res.sort(that.dataserver.sortObjValue('stage'))
                that.listData = res;
            }
        })
        this.listData = [
            {
                bgImg: '../../../assets/imgs/prenatal-education-img1.png',
                isPlay: false,
                title: '大魔法与小蝴蝶',
            }, {
                bgImg: '../../../assets/imgs/prenatal-education-img2.png',
                isPlay: false,
                title: '彩云和唐老鸭',
            }, {
                bgImg: '../../../assets/imgs/prenatal-education-img3.png',
                isPlay: false,
                title: '彩云和唐老鸭',
            }, {
                bgImg: '../../../assets/imgs/prenatal-education-img4.png',
                isPlay: false,
                title: '大魔法与小蝴蝶',
            }
        ]
    }
    play(i) {
        if (this.listData[i].isPlay) {
            this.listData[i].isPlay = false;
        } else {
            for (let i = 0; i < this.listData.length; i++) {
                this.listData[i].isPlay = false;
            }
            this.listData[i].isPlay = true;
        }

    }
    collect(i) {
        console.log('收藏');
    }
    download(i) {
        console.log('下载')
    }
}