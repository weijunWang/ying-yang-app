import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { SuccessStoryDetailPage } from '../success-story-detail/success-story-detail'

@IonicPage()
@Component({
  selector: 'page-success-story',
  templateUrl: 'success-story.html',
})
export class SuccessStoryPage {
  listData: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {
  }
  ngOnInit() {
    let that = this;
    this.http.get('/api/success_stories/', {}, function (res) {
      console.log(res);
      if (res) {
        //   res.sort(that.dataserver.sortObjValue('stage'))
        that.listData = res.data;
      }
    })
    this.listData = [
      {
        id: 1,
        img: '../../../assets/imgs/story-img.png',
        title: '孕妈们如何孕期长胎不长肉的科学秘籍',
        resource: '复旦孕产官方小助手',
        from: '来自“天真的梅子酱”'
      }, {
        id: 2,
        img: '../../../assets/imgs/story-img.png',
        title: '孕妈们如何孕期长胎不长肉的科学秘籍',
        resource: '复旦孕产官方小助手',
        from: '来自“天真的梅子酱”'
      }, {
        id: 3,
        img: '../../../assets/imgs/story-img.png',
        title: '孕妈们如何孕期长胎不长肉的科学秘籍',
        resource: '复旦孕产官方小助手',
        from: '来自“天真的梅子酱”'
      }
    ]
  }
  toDetail(id) {
    this.navCtrl.push(SuccessStoryDetailPage, {id: id ? id : 1});
  }
}