import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PrivacyPolicyPage } from '../privacy-policy/privacy-policy';

@IonicPage()
@Component({
    selector: 'page-setting',
    templateUrl: 'setting.html',
})
export class SettingPage {
    isBegin: boolean = false;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    choiceBegin(){
      this.isBegin = true;
    }
    choiceClose(){
      this.isBegin = false;
    }
    toPage(){
      this.navCtrl.push(PrivacyPolicyPage, {
        params: 1
      });
    }
}