import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
  selector: 'page-posting',
  templateUrl: 'posting.html',
})
export class PostingPage {
  fileModel: any;
  fileList: any;
  allData: any;
  newLabelText: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {
  }
  ngOnInit() {
    let that = this;
    // this.http.get('/api/get_activities/', {}, function (res) {
    //   console.log(res);
    //   if (res) {
    //     that.allData = res;
    //   }
    // })
    this.fileList = [];
    this.allData = [
      {
        text: '孕期运动'
      }, {
        text: '孕期运动'
      }
    ]
  }
  changepic() {
    // let reads= new FileReader();
    // let f=document.getElementById('file').files[0];
    // reads.readAsDataURL(f);
    // reads.onload=function (e) {
    //     document.getElementById('show').src=this.result;
    // };
  }
  inpChange() {
    // console.log(this.fileModel);
    // console.log(document.getElementById('file').files[0]);
    let that = this;
    let reads = new FileReader();
    let f = (<HTMLInputElement>document.getElementById('file')).files[0];
    reads.readAsDataURL(f);
    reads.onload = function (e) {
      that.fileList.push({
        img: this.result
      });
    };
  }
  newLabel() {
    let data = {
      newLabelText: this.allData.newLabelText
    }
    console.log('data', data);
    ///label
  }
  record() {
    let data = {
      "content": "",
      "img_lists": [
        {
          "img": {}
        }
      ],
      "label": []
    }

    console.log('data', data);
    //forum
  }
}