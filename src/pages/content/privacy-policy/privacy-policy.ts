import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TabsPage } from '../../tabs/tabs';

@IonicPage()
@Component({
    selector: 'page-privacy-policy',
    templateUrl: 'privacy-policy.html',
})
export class PrivacyPolicyPage {
    listData: any;
    show: boolean;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
        if(this.navParams.get('params') == 1){
            this.show = false;
        }else{
            this.show = true;
        }
        this.listData = [
            {
                text: '一、我们收集了哪些信息'
            },{
                text: '二、我们如何使用收集的信息'
            },{
                text: '三、我们如何使用Cookie和同类技术'
            },{
                text: '四、我们如何共享、转让、公开披露您的信息'
            },{
                text: '五、我们如何保护您的信息'
            },{
                text: '六、您如何管理您的信息'
            },{
                text: '七、我们如何处理未成年人的信息'
            },{
                text: '八、您的信息如何在全球范围转移'
            },{
                text: '九、本隐私权政策如何更新'
            },{
                text: '十、如何联系我们'
            }
        ]
    }
    confirm() {
        this.navCtrl.push(TabsPage);
        // this.navCtrl.parent.select(0);
      }
}