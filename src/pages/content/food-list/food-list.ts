import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SearchFoodPage } from '../search-food/search-food';

@IonicPage()
@Component({
  selector: 'page-food-list',
  templateUrl: 'food-list.html',
})
export class FoodListPage {
    listData: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
        console.log('id', this.navParams.get('id'));
        this.listData = [
            {
                id: 1,
                icon: '../../../assets/imgs/ig-icon1.png',
                text: '糖类'
            },{
                id: 2,
                icon: '../../../assets/imgs/ig-icon2.png',
                text: '谷类及制品'
            },{
                id: 3,
                icon: '../../../assets/imgs/ig-icon3.png',
                text: '蔬菜类'
            },{
                id: 4,
                icon: '../../../assets/imgs/ig-icon4.png',
                text: '水果类'
            },{
                id: 5,
                icon: '../../../assets/imgs/ig-icon5.png',
                text: '乳制品'
            },{
                id: 6,
                icon: '../../../assets/imgs/ig-icon6.png',
                text: '豆类'
            },{
                id: 7,
                icon: '../../../assets/imgs/ig-icon7.png',
                text: '方便食品'
            },{
                id: 8,
                icon: '../../../assets/imgs/ig-icon8.png',
                text: '饮料类'
            },{
                id: 9,
                icon: '../../../assets/imgs/ig-icon9.png',
                text: '薯类及制品'
            },{
                id: 10,
                icon: '../../../assets/imgs/ig-icon10.png',
                text: '混合膳食'
            }
        ]
    }
    focusInput(){
        console.log('聚焦')
        this.navCtrl.push(SearchFoodPage);
    }
    toFood(id){
        // this.navCtrl.push();
    }
}