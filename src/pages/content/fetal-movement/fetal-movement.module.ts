import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FetalMovementPage } from './fetal-movement';

@NgModule({
  declarations: [
    FetalMovementPage,
  ],
  imports: [
    IonicPageModule.forChild(FetalMovementPage),
  ],
})
export class FetalMovementPageModule {}