import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

import { FetalMovementRecordPage } from '../fetal-movement-record/fetal-movement-record';

@IonicPage()
@Component({
  selector: 'page-fetal-movement',
  templateUrl: 'fetal-movement.html',
})
export class FetalMovementPage {
  isBegin: boolean = false;
  pushModal: boolean = false;
  beginText: string = '点击记录';
  clickTime: string = '59: 59';
  nowTime: any;
  clicks: number = 0;
  validClicks: number = 0;
  beginTime: string = new Date().getHours() + ": " + new Date().getMinutes();
  needClearTimeOut: boolean = false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public alertCtrl: AlertController) {
  }
  changeBegin() {
    if (!this.isBegin) {
      this.isBegin = true;
      this.needClearTimeOut = false;
      this.beginText = '动一下，点一下';
      this.beginTime = new Date().getHours() + ": " + new Date().getMinutes();
      let end = new Date().getTime() + 3600000;
      this.countTime(end);
    } else {
      this.clicks++;
      if(this.validClicks < 5){
        this.validClicks++;
      }
    }
  }
  checkEnd() {
    // let alert = this.alertCtrl.create({
    //   title: '温馨提示',
    //   subTitle: `您记录的时间不足一小时，可能会影响正确结果，确定要结束吗？`,
    //   buttons: [
    //     {
    //       text: '我知道了',
    //       handler: () => {
    //         console.log('确定按钮被点击');
    //         this.needClearTimeOut = true;
    //         this.isBegin = false;
    //         this.clickTime = '59: 59';
    //         this.beginText = '点击记录';
    //       }
    //     }
    //   ]
    // });
    // alert.present();
    this.pushModal = true;
  }
  closeModal(){
    this.pushModal = false;
  }
  checkConform(){
    this.needClearTimeOut = true;
    this.isBegin = false;
    this.clickTime = '59: 59';
    this.beginText = '点击记录';
    this.pushModal = false;
  }
  countTime(end) {
    //获取当前时间
    let date = new Date();
    let now = date.getTime();
    //设置截止时间

    // console.log('end', end);

    //时间差
    let leftTime = end - now;
    // console.log('leftTime', leftTime);
    //定义变量 d,h,m,s保存倒计时的时间
    let m, s;
    if (leftTime >= 0) {
      m = Math.floor(leftTime / 1000 / 60 % 60);
      s = Math.floor(leftTime / 1000 % 60);
    }
    if (m != 0) {
      this.clickTime = m + ": " + s;
    }
    // console.log(this.clickTime);
    //递归每秒调用countTime方法，显示动态时间效果;
    let that = this;
    if(this.needClearTimeOut){
      return false;
    }
    setTimeout(function () {
      // console.log('setTime', end);
      that.countTime(end)
    }, 1000);
  }
  record(){
    this.navCtrl.push(FetalMovementRecordPage);
  }
}