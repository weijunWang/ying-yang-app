import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { RecordDietPage } from '../record-diet/record-diet';

@IonicPage()
@Component({
    selector: 'page-diet',
    templateUrl: 'diet.html',
})
export class DietPage {
    img_data: any = [
        {
            "date_time": "",
            "is_conplate": "",
            "status": "孕8周",
            "point": ""
        }
    ];
    img_data1: any = [
        {
            "date_time": "",
            "is_conplate": "",
            "status": "孕8周",
            "point": ""
        }
    ];
    img_data2: any = [
        {
            "date_time": "",
            "is_conplate": "",
            "status": "孕8周",
            "point": ""
        }
    ];
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit() {
        let that = this;
        this.http.get('/api/get_diets/', {}, function (res) {
            console.log(res);
            if (res) {
                that.img_data = res.data;
                that.img_data1 = [];
                that.img_data2 = [];
                for (let i = 0; i < that.img_data.length; i++) {
                    if (i % 2 == 0) {
                        that.img_data1.push(that.img_data[i])
                    } else {
                        that.img_data2.push(that.img_data[i])
                    }
                }
                // console.log();
                // let box_img = document.getElementsByClassName('box_img');
                // console.log(box_img);
            }
        })
        // this.getNode();
        // this.img_data1 = [];
        // this.img_data2 = [];
        // this.img_data = [
        //     {
        //         id: 1,
        //         canEdit: true,
        //         status: '每日膳食',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }, {
        //         id: 2,
        //         canEdit: false,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }, {
        //         id: 3,
        //         canEdit: true,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }, {
        //         id: 4,
        //         canEdit: false,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }, {
        //         id: 5,
        //         canEdit: false,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }, {
        //         id: 6,
        //         canEdit: true,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //     }
        // ]
        for (let i = 0; i < this.img_data.length; i++) {
            if (i % 2 == 0) {
                this.img_data1.push(this.img_data[i])
            } else {
                this.img_data2.push(this.img_data[i])
            }
        }
        let box_img = document.getElementsByClassName('box_img');
        console.log(box_img);
    }
    toPage(id) {
        let isShow;
        if (id == 1 || id == 3 || id == 6) {
            isShow = true
        } else {
            isShow = false
        }
        this.navCtrl.push(RecordDietPage, {
            isShow: isShow
        });
    }
}