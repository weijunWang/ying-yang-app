import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';

@IonicPage()
@Component({
  selector: 'page-change-baby-height',
  templateUrl: 'change-baby-height.html',
})
export class ChangeBabyHeightPage {
    forList: any;
    height: string;
    weight: string;
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public http: HttpServerProvider) {
    }
    ngOnInit(){
        // let userinfo = this.storage.getStorage('userinfo');
        this.forList = [
            {
                title: '家里',
                flag: false
            },{
                title: '餐馆',
                flag: true
            },{
                title: '路边摊',
                flag: false
            }
        ]
    }
    save(){
        console.log('保存');
        let that = this;
        let data = {
            height: this.height,
            weight: this.weight
        }
        console.log(data);
        this.http.patch('/api/child_info', data, function (res) {
            console.log(res);
            if (res) {
                that.navCtrl.pop();
            }
        }, function (err) {
            console.log(err);
        })
    }
}