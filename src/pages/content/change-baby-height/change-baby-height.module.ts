import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChangeBabyHeightPage } from './change-baby-height';

@NgModule({
  declarations: [
    ChangeBabyHeightPage,
  ],
  imports: [
    IonicPageModule.forChild(ChangeBabyHeightPage),
  ],
})
export class ChangeBabyHeightModule {}