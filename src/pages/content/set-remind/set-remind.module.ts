import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SetRemindPage } from './set-remind';

@NgModule({
  declarations: [
    SetRemindPage,
  ],
  imports: [
    IonicPageModule.forChild(SetRemindPage),
  ],
})
export class SetRemindPageModule {}