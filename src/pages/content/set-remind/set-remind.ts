import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
  selector: 'page-set-remind',
  templateUrl: 'set-remind.html',
})
export class SetRemindPage {
    forList: any;
    dataDateTimes: any;
    hourMinite: any;
    dateTime: string;
    constructor(
      public navCtrl: NavController, 
      public navParams: NavParams,
      public http: HttpServerProvider,
      public storage: StorageServeProvider,
      public dataserver: DataServersProvider) {
    }
    ngOnInit(){
        this.dataDateTimes = [
            {
              name: 'babyHeight',
              options: [
                { text: '1', value: '1' },
                { text: '2', value: '2' },
                { text: '3', value: '3' },
                { text: '4', value: '4' },
                { text: '5', value: '5' },
                { text: '6', value: '6' },
                { text: '7', value: '7' },
                { text: '8', value: '8' },
                { text: '9', value: '9' },
                { text: '10', value: '10' },
                { text: '11', value: '11' },
                { text: '12', value: '12' },
              ]
            },{
                name: 'babyHeight2',
                options: [
                  { text: ':', value: '.' },
                ]
              },{
                name: 'babyHeight3',
                options: [
                  { text: '00', value: '1' },
                  { text: '01', value: '1' },
                  { text: '02', value: '2' },
                  { text: '03', value: '3' },
                  { text: '04', value: '3' },
                  { text: '05', value: '3' },
                  { text: '06', value: '3' },
                  { text: '07', value: '3' },
                  { text: '08', value: '3' },
                  { text: '09', value: '3' },
                  { text: '10', value: '3' },
                  { text: '11', value: '3' },
                  { text: '12', value: '3' },
                  { text: '13', value: '3' },
                  { text: '14', value: '3' },
                  { text: '15', value: '3' },
                  { text: '16', value: '3' },
                  { text: '17', value: '3' },
                  { text: '18', value: '3' },
                  { text: '19', value: '3' },
                  { text: '20', value: '3' },
                  { text: '21', value: '3' },
                  { text: '22', value: '3' },
                  { text: '23', value: '3' },
                  { text: '24', value: '3' },
                  { text: '25', value: '3' },
                  { text: '26', value: '3' },
                  { text: '27', value: '3' },
                  { text: '28', value: '3' },
                  { text: '29', value: '3' },
                  { text: '30', value: '3' },
                  { text: '31', value: '3' },
                  { text: '32', value: '3' },
                  { text: '33', value: '3' },
                  { text: '34', value: '3' },
                  { text: '35', value: '3' },
                  { text: '36', value: '3' },
                  { text: '37', value: '3' },
                  { text: '38', value: '3' },
                  { text: '39', value: '3' },
                  { text: '40', value: '3' },
                  { text: '41', value: '3' },
                  { text: '42', value: '3' },
                  { text: '43', value: '3' },
                  { text: '44', value: '3' },
                  { text: '45', value: '3' },
                  { text: '46', value: '3' },
                  { text: '47', value: '3' },
                  { text: '48', value: '3' },
                  { text: '49', value: '3' },
                  { text: '50', value: '3' },
                  { text: '51', value: '3' },
                  { text: '52', value: '3' },
                  { text: '53', value: '3' },
                  { text: '54', value: '3' },
                  { text: '55', value: '3' },
                  { text: '56', value: '3' },
                  { text: '57', value: '3' },
                  { text: '58', value: '3' },
                  { text: '59', value: '3' },
                ]
              }
        ];
        this.forList = [
            {
                title: '家里',
                flag: false
            },{
                title: '餐馆',
                flag: true
            },{
                title: '路边摊',
                flag: false
            }
        ]
    }
    save(){
        console.log('保存');
        let that = this;
        let data = {
          "remind_date": this.dateTime,
          "remind_time": this.dataserver.deleteSpace(this.hourMinite),
          "title": "title",
          "content": "content"
        }
        console.log(data);
        this.http.post('/api/set_remind', data, function (res) {
          console.log(res);
        }, function (err) {
          console.log(err);
        })
    }
}