import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecipesOnedayPage } from './recipes-oneday';

@NgModule({
  declarations: [
    RecipesOnedayPage,
  ],
  imports: [
    IonicPageModule.forChild(RecipesOnedayPage),
  ],
})
export class RecipesOnedayModule {}