import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-recipes-oneday',
  templateUrl: 'recipes-oneday.html',
})
export class RecipesOnedayPage {
    allData = null;
    keyword = "";
    firstData : any;
    flowData: any;
    footerData: any;
    lwidth: number = 5;
    lheight: number = 10;
    checkMoreTrue: boolean = false;
    checkMoreText: string = '查看更多'
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    
    ngOnInit() {
      console.log('ionViewDidLoad RecipesOnedayPage');
      this.allData = [
        {
          id: 1,
          text: "全麦面包",
          num: '2',
          danwei: '片'
        },{
          id: 2,
          text: "鸡蛋",
          num: '1',
          danwei: '个'
        },{
          id: 3,
          text: "番茄",
          num: '30',
          danwei: 'g'
        },{
          id: 4,
          text: "生菜",
          num: '30',
          danwei: 'g'
        },{
          id: 5,
          text: "干酪",
          num: '30',
          danwei: 'g'
        }
      ]
      this.firstData = [
        {
          id: 1,
          text: "全麦面包",
          num: '2',
          danwei: '片'
        },{
          id: 2,
          text: "鸡蛋",
          num: '1',
          danwei: '个'
        }
      ]
      this.flowData = [
        {
          text: '1',
          lheight: '4'
        },{
          text: '1',
          lheight: '5'
        },{
          text: '1',
          lheight: '5'
        },{
          text: '1',
          lheight: '4'
        },{
          text: '1',
          lheight: '2'
        },{
          text: '1',
          lheight: '4'
        },{
          text: '1',
          lheight: '3'
        },{
          text: '1',
          lheight: '5'
        }
      ]
      this.footerData = [
        {
          text: '能量',
          num: '1857kcal'
        },{
          text: '能量',
          num: '1857kcal'
        },{
          text: '能量',
          num: '1857kcal'
        },{
          text: '能量',
          num: '1857kcal'
        },{
          text: '能量',
          num: '1857kcal'
        },{
          text: '能量',
          num: '1857kcal'
        }
      ]
    }
    checkMore(){
      this.checkMoreTrue = !this.checkMoreTrue;
      if(!this.checkMoreTrue){
        this.checkMoreText = '查看更多'
      }else{
        this.checkMoreText = '收起明细'
      }
    }
}