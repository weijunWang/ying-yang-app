import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightCurveChildPage } from './weight-curve-child';

@NgModule({
  declarations: [
    WeightCurveChildPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightCurveChildPage),
  ],
})
export class WeightCurveChildPageModule {}