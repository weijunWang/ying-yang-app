import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import * as echarts from 'echarts';

import { RecordWeightPage } from '../record-weight/record-weight';
// import { WeightHistoricalPage } from '../weight-historical/weight-historical';
import { WeightaheightHistoricalPage } from '../weightaheight-historical/weightaheight-historical';
// import { WeightCurvePage } from '../weight-curve/weight-curve';

@IonicPage()
@Component({
    selector: 'page-weight-curve-child',
    templateUrl: 'weight-curve-child.html',
})
export class WeightCurveChildPage {
    showHeight: boolean = true;
    constructor(private dataServer: DataServersProvider, public navCtrl: NavController, public navParams: NavParams) {
        console.log(echarts);
    }

    ngOnInit() {/*当进入页面时触发*/
        const ec = echarts as any;
        let myChart2 = ec.init(document.getElementById('chart2'));
        let myChart3 = ec.init(document.getElementById('chart3'));
        console.log("getChildCurveData", this.dataServer.getChildCurveData('boy', 'percent3', 'weight'));
        let option2 = {
            xAxis: [
                {
                    splitLine: {
                        show: false
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: ['出生', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12'] 
                }
            ],
            yAxis: [
                {
                    min: 35,
                    splitLine: {
                        show: false
                    },
                    type: 'value'
                }
            ],
            dataZoom: [{
                type: 'slider',
                show: false
            }, {
                type: "inside"         //详细配置可见echarts官网
            }],
            series: [
                {
                    name: '3%',
                    // stack: '总量',
                    color: '#D6D875',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent3', 'height'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '25%',
                    // stack: '总量',
                    color: '#EE9493',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent25', 'height'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '50%',
                    // stack: '总量',
                    color: '#97EBD2',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent50', 'height'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '75%',
                    // stack: '总量',
                    color: '#9EC7FB',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent75', 'height'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '97',
                    // stack: '总量',
                    color: '#BF3A3A',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent97', 'height'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                }
            ]
        };
        let option3 = {
            xAxis: [
                {
                    splitLine: {
                        show: false
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: ['出生', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12']
                }
            ],
            yAxis: [
                {
                    splitLine: {
                        show: false
                    },
                    type: 'value'
                }
            ],
            dataZoom: [{
                type: 'slider',
                show: false
            }, {
                type: "inside"         //详细配置可见echarts官网
            }],
            series: [
                {
                    name: '3%',
                    // stack: '总量',
                    color: '#D6D875',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent3', 'weight'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '25%',
                    // stack: '总量',
                    color: '#EE9493',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent25', 'weight'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '50%',
                    // stack: '总量',
                    color: '#97EBD2',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent50', 'weight'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '75%',
                    // stack: '总量',
                    color: '#9EC7FB',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent75', 'weight'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                },{
                    name: '97',
                    // stack: '总量',
                    color: '#BF3A3A',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getChildCurveData('boy', 'percent97', 'weight'),
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                }
            ]
        };
        myChart2.setOption(option2);
        myChart3.setOption(option3);
    }
    toCalendar() {
        this.navCtrl.push(RecordWeightPage);
    }
    record() {
        console.log('record');
        this.navCtrl.push(WeightaheightHistoricalPage);
    }
    changeCurver(){
        this.navCtrl.pop();
        // this.navCtrl.push(WeightCurvePage);
    }
    changeWeight(){
        this.showHeight = true;
    }
    changeHeight(){
        this.showHeight = false;
    }
}