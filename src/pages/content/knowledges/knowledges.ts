import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { KnowledgeDetailPage } from '../knowledge-detail/knowledge-detail'
import { KnowledgeBasePage } from '../knowledge-base/knowledge-base'

@IonicPage()
@Component({
  selector: 'page-knowledges',
  templateUrl: 'knowledges.html',
})
export class KnowledgesPage {
  listData: any;
  userinfo: any;
  stageStatus: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {
  }
  ngOnInit() {
    let that = this;
    this.userinfo = this.storage.getStorage('userinfo');
    this.http.get('/api/get_articles', {}, function (res) {
      console.log(res);
      if (res) {
        res.sort(that.dataserver.sortObjValue('stage'))
        that.listData = res;
      }
    })

    this.stageStatus = ['孕早期', '孕中期', '孕晚期', 'GDM', '其他', '母乳喂养', '喂食喂养']
    // this.listData = [
    //   {
    //     id: 1,
    //     title: '孕早期有哪些注意事项呢？',
    //     body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
    //   }, {
    //     id: 2,
    //     title: '孕早期有哪些注意事项呢？',
    //     body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
    //   }, {
    //     id: 3,
    //     title: '孕早期有哪些注意事项呢？',
    //     body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
    //   }
    // ]
  }
  toPage(id) {
    this.navCtrl.push(KnowledgeDetailPage, {
      id: id ? id : 1,
    });
  }
  focusInput() {
    console.log('聚焦')
    this.navCtrl.push(KnowledgeBasePage);
  }
}