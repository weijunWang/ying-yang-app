import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { KnowledgesPage } from './knowledges';

@NgModule({
  declarations: [
    KnowledgesPage,
  ],
  imports: [
    IonicPageModule.forChild(KnowledgesPage),
  ],
})
export class KnowledgesModule {}