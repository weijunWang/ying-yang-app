import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
  selector: 'page-contact-us',
  templateUrl: 'contact-us.html',
})
export class ContactUsPage {
  content: string;
  email: string;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {
  }
  //send_email

  sendEmail() {
    let that = this;
    let data = {
      "email": this.email,
      "content": this.content
    }
    console.log(data);
    this.http.post('/api/send_email/', data, function (res) {
      console.log(res);
    }, function (err) {
      console.log(err);
    })
  }
}