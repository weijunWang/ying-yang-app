import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-questionnaire-gdm',
  templateUrl: 'questionnaire-gdm.html',
})
export class QuestionnaireGdmPage {
    renchenList: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {
    this.renchenList = [
        {
          id: 1,
          flag: false,
          name: '无妊娠期糖尿病'
        },{
          id: 2,
          flag: false,
          name: '已确诊妊娠期糖尿病'
        },{
          id: 3,
          flag: false,
          name: '还未做检查'
        }
      ]
  }
  renchenClick(id){
    for(let i = 0;i < this.renchenList.length;i++){
      this.renchenList[i].flag = false;
    }
    this.renchenList[id - 1].flag = true;
    // this.renchenModal = false;
  }
  confirm(){
    this.navCtrl.pop();
  }
}