import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionnaireGdmPage } from './questionnaire-gdm';

@NgModule({
  declarations: [
    QuestionnaireGdmPage,
  ],
  imports: [
    IonicPageModule.forChild(QuestionnaireGdmPage),
  ],
})
export class QuestionnaireGdmPageModule {}