import { Component, Input, Output, EventEmitter } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SuccessStoryDetailPage } from '../success-story-detail/success-story-detail';
@IonicPage()
@Component({
  selector: 'page-news',
  templateUrl: 'news.html',
})
export class NewsPage {
  @Input("slides") slides: string[] = [];
  @Input("pageNumber") pageNumber: number = 4;
  @Output("slideClick") slideClick = new EventEmitter<number>();
 
  selectedIndex: number = 0;


  musicData: any;
  listData: any;
  listBar: any;
  pet: number  = 0;
  others: any;
  beginSwiper: boolean = false;
  listItems: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }
  ngOnInit() {
    this.musicData = [
      {
        bgImg: '../../../assets/imgs/prenatal-education-img1.png',
        isPlay: false,
        title: '大魔法与小蝴蝶',
      }, {
        bgImg: '../../../assets/imgs/prenatal-education-img2.png',
        isPlay: false,
        title: '彩云和唐老鸭',
      }, {
        bgImg: '../../../assets/imgs/prenatal-education-img3.png',
        isPlay: false,
        title: '彩云和唐老鸭',
      }, {
        bgImg: '../../../assets/imgs/prenatal-education-img4.png',
        isPlay: false,
        title: '大魔法与小蝴蝶',
      }
    ]
    this.listBar = [
      {
        id: 1,
        text: '知识库',
        pet: 'choice1',
        flag: true
      }, {
        id: 2,
        text: '胎教音乐',
        pet: 'choice2',
        flag: false
      }, {
        id: 3,
        text: '问与答',
        pet: 'choice3',
        flag: false
      }, {
        id: 4,
        text: '营养食谱',
        pet: 'choice4',
        flag: false
      }, {
        id: 5,
        text: '其他',
        pet: 'choice5',
        flag: false
      }
    ]
    this.listItems = [
      {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img2.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }, {
        img: '../../../assets/imgs/recipes-img1.png',
        firstText: '一日之餐·周一',
        secondText: '周一的食谱',
        thirdText: '食谱的内容简介',
      }
    ]
    this.listData = [
      {
        id: 1,
        title: '孕早期有哪些注意事项呢？',
        body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
      }, {
        id: 2,
        title: '孕早期有哪些注意事项呢？',
        body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
      }, {
        id: 3,
        title: '孕早期有哪些注意事项呢？',
        body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
      }
    ]

    this.others = [
      {
        id: 1,
        img: '../../../assets/imgs/story-img.png',
        title: '孕妈们如何孕期长胎不长肉的科学秘籍',
        resource: '复旦孕产官方小助手',
        from: '来自“天真的梅子酱”'
      },{
        id: 2,
        img: '../../../assets/imgs/story-img.png',
        title: '孕妈们如何孕期长胎不长肉的科学秘籍',
        resource: '复旦孕产官方小助手',
        from: '来自“天真的梅子酱”'
      },{
        id: 3,
        img: '../../../assets/imgs/story-img.png',
        title: '孕妈们如何孕期长胎不长肉的科学秘籍',
        resource: '复旦孕产官方小助手',
        from: '来自“天真的梅子酱”'
      }
    ]
  }
  toDetail(id){
    this.navCtrl.push(SuccessStoryDetailPage);
  }
  onClick(index) {
    this.selectedIndex = index;
    this.slideClick.emit(index);
    this.pet = index;
  }
  choiceBar(id) {
    for (let i = 0; i < this.listBar.length; i++) {
      this.listBar[i].flag = false;
    }
    this.listBar[id - 1].flag = true;
    this.pet = this.listBar[id - 1].pet;
  }
  play(i) {
    if (this.musicData[i].isPlay) {
      this.musicData[i].isPlay = false;
    } else {
      for (let i = 0; i < this.musicData.length; i++) {
        this.musicData[i].isPlay = false;
      }
      this.musicData[i].isPlay = true;
    }

  }
  collect(i) {
    console.log('收藏');
  }
  download(i) {
    console.log('下载')
  }
}