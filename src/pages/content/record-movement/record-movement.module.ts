import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RecordMovementPage } from './record-movement';

@NgModule({
  declarations: [
    RecordMovementPage,
  ],
  imports: [
    IonicPageModule.forChild(RecordMovementPage),
  ],
})
export class RecordMovementPageModule {}