import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { AddMovementPage } from '../add-movement/add-movement';

@IonicPage()
@Component({
    selector: 'page-record-movement',
    templateUrl: 'record-movement.html',
})
export class RecordMovementPage {
    canShow: boolean = false;
    sportsData: any;
    list2: any;
    listData: any;
    pet: string = 'choice1';
    listBar: any;
    beginSwiper: boolean = false;
    choice: number = 1;
    
    unit = 0.65;// 横向刻度尺的计算值

    swipe: number = 0;
    offsetX: number = 0;
    moveX = 0;
    moveBefore = 0;
    move: number = 0;
    
    rulerLen: any; // 刻度尺图片的偏移度,对应html中的data-offset
    numText: any = 30;//最终选择刻度的数字显示
    rulers: any;//刻度值数组
    
    siteOffsetX: number = 0;
    siteMoveX = 0;
    siteMoveBefore = 0;
    siteMove: number = 0;
    siteLen: any;
    siteNumText: any = 1.5;
    siteTime: any;

    sleepOffsetX: number = 0;
    sleepMoveX = 0;
    sleepMoveBefore = 0;
    sleepMove: number = 0;
    sleepLen: any;
    sleepNumText: any = 1.5;
    sleepTime: any;
    constructor(private dataServer: DataServersProvider, public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit() {
        this.rulerLen = -19.5;
        this.siteLen = -19.5;
        this.sleepLen = -19.5;
        this.rulers = this.dataServer.getChiMinutes();
        this.siteTime = this.dataServer.getChiSleep();
        this.list2 = [];
        this.sportsData = [
            {
                show: false,
                icon: '../../../assets/imgs/mothon-icon1.png',
                name: '购物',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon2.png',
                name: '散步、步行',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon3.png',
                name: '游泳',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon4.png',
                name: '瑜伽',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon5.png',
                name: '慢走、快跑',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon6.png',
                name: '网球',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon7.png',
                name: '篮球',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon8.png',
                name: '比赛打网球',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon9.png',
                name: '快骑脚踏车',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon10.png',
                name: '快跑',
                time: '30'
            },{
                show: false,
                icon: '../../../assets/imgs/mothon-icon-add.png',
                name: '添加运动'
            }
        ]
    }
    closeItem(index){
        console.log(index);
        this.list2.splice(index, 1);
        if(this.list2.length == 0){
            this.beginSwiper = false;
        }
    }
    choiceItem(index) {
        if(index == this.sportsData.length - 1){
            this.navCtrl.push(AddMovementPage);
        }else{
            for (let i = 0; i < this.sportsData.length; i++) {
                this.sportsData[i].show = false;
            }
            this.sportsData[index].show = true;
            this.beginSwiper = true;
            this.rulerLen = -19.5;
            this.numText = 30;
            document.getElementById('ruler').setAttribute('data-offset', '-19.5');
        }
    }
    btnConfirm(){
        console.log(this.numText);
        for(let i = 0;i < this.sportsData.length;i++){
            if(this.sportsData[i].show){
                this.sportsData[i].time = this.numText;
                this.list2.push(this.sportsData[i])
            }
        }
        if(this.beginSwiper){
            this.canShow = true;
        }
    }
    saveTime(){
        var data = {
            numText: this.numText,
            siteNumText: this.siteNumText,
            sleepNumText: this.sleepNumText,
        }
        console.log(data);
    }
    postTime(){

    }
    touchstart(e){
        this.offsetX = e.touches[0].clientX;
        this.moveBefore = 0;
      }
    touchmove(e){
        // console.log(e)
        let ruler = document.getElementById('ruler');
        this.move = e.touches[0].clientX;
    
        let offset: any = ruler.dataset.offset;
        offset = parseFloat(offset);
        let tempMove = 0;
        let len: any = 0;
        len = offset + (tempMove - this.moveBefore);
        len = parseFloat(len);
    
        tempMove = this.move - this.offsetX;
        tempMove /= 10;
        //计算两次滑动间的距离
        len = offset + (tempMove - this.moveBefore);
        len = parseFloat(len);
        //边界判断，最大偏移长度65rem
        if (len - 0.0 < 0 && len > -100) {
            //将结果保存下来，下一次滑动时取出参与计算
            this.moveX = tempMove;
            ruler.dataset.offset = len;
            this.moveBefore = this.moveX;
            //设置样式
            this.rulerLen = len
            // //显示刻度，保留1位小数
            this.numText = - ((len / this.unit).toFixed(0));
        }
    }
    siteTouchstart(e){
        // console.log(e);
        // this.beginSwiper = true;
        this.siteOffsetX = e.touches[0].clientX;
        this.siteMoveBefore = 0;
      }
    siteTouchmove(e){
        // console.log(e)
        let ruler = document.getElementById('site-ruler');
        this.siteMove = e.touches[0].clientX;
    
        let offset: any = ruler.dataset.offset;
        offset = parseFloat(offset);
        let tempMove = 0;
        let len: any = 0;
        len = offset + (tempMove - this.siteMoveBefore);
        len = parseFloat(len);
    
        tempMove = this.siteMove - this.siteOffsetX;
        tempMove /= 10;
        //计算两次滑动间的距离
        len = offset + (tempMove - this.siteMoveBefore);
        len = parseFloat(len);
        //边界判断，最大偏移长度65rem
        if (len - 0.0 < 0 && len > -100) {
            //将结果保存下来，下一次滑动时取出参与计算
            this.siteMoveX = tempMove;
            ruler.dataset.offset = len;
            this.siteMoveBefore = this.siteMoveX;
            //设置样式
            this.siteLen = len
            // //显示刻度，保留1位小数
            let siteNumText = ( - (len / this.unit) / 20).toFixed(1)
            if((Number(siteNumText) * 10) % 5 == 0){
                this.siteNumText = siteNumText;
            }
        }
    }
    sleepTouchstart(e){
        this.sleepOffsetX = e.touches[0].clientX;
        this.sleepMoveBefore = 0;
      }
    sleepTouchmove(e){
        // console.log(e)
        let ruler = document.getElementById('sleep-ruler');
        this.sleepMove = e.touches[0].clientX;
    
        let offset: any = ruler.dataset.offset;
        offset = parseFloat(offset);
        let tempMove = 0;
        let len: any = 0;
        len = offset + (tempMove - this.sleepMoveBefore);
        len = parseFloat(len);
    
        tempMove = this.sleepMove - this.sleepOffsetX;
        tempMove /= 10;
        //计算两次滑动间的距离
        len = offset + (tempMove - this.sleepMoveBefore);
        len = parseFloat(len);
        //边界判断，最大偏移长度65rem
        if (len - 0.0 < 0 && len > -100) {
            //将结果保存下来，下一次滑动时取出参与计算
            this.sleepMoveX = tempMove;
            ruler.dataset.offset = len;
            this.sleepMoveBefore = this.sleepMoveX;
            //设置样式
            this.sleepLen = len
            // //显示刻度，保留1位小数
            let sleepNumText = ( - (len / this.unit) / 20).toFixed(1)
            if((Number(sleepNumText) * 10) % 5 == 0){
                this.sleepNumText = sleepNumText;
            }
        }
    }
    
}