import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
    selector: 'page-activity-detail',
    templateUrl: 'activity-detail.html',
})
export class ActivityDetailPage {
    allData = null;
    keyword = "";
    peopleNum: number = 0;
    headerImgList: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }

    ngOnInit() {
        this.peopleNum = 7;
        this.allData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？"
            }, {
                id: 2,
                text: "孕期运动"
            }, {
                id: 3,
                text: "如何进行宝宝胎教，做哪些事情比较好"
            }, {
                id: 4,
                text: "妊娠期"
            }
        ]
        if(this.peopleNum == 1){
            this.headerImgList = [
                {
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }else if(this.peopleNum == 2){
            this.headerImgList = [
                {
                    width: 5,
                    left: 18.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 13.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }else if(this.peopleNum == 3){
            this.headerImgList = [
                {
                    width: 4.6,
                    left: 13.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 18.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }else if(this.peopleNum == 4){
            this.headerImgList = [
                {
                    width: 5,
                    left: 18.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 13.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 20.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 11.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }else if(this.peopleNum == 5){
            this.headerImgList = [
                {
                    width: 4.2,
                    left: 11.625,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 13.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 18.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.2,
                    left: 20.125,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }else if(this.peopleNum == 6){
            this.headerImgList = [
                {
                    width: 4.4,
                    left: 9.125,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 11.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 13.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 18.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 20.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.2,
                    left: 22.625,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }else if(this.peopleNum == 7){
            this.headerImgList = [
                {
                    width: 3.8,
                    left: 9.125,
                    top: 0.6,
                    zIndex: 2,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.2,
                    left: 11.625,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 13.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.6,
                    left: 18.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 4.2,
                    left: 20.125,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                },{
                    width: 3.8,
                    left: 22.125,
                    top: 0.6,
                    zIndex: 2,
                    img: '../../../assets/imgs/demo.png'
                }
            ]
        }
        console.log('ionViewDidLoad KnowledgeDetailPage');
    }
    duihuan(){
        console.log('兑换');
    }
}