import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { EditHawPage } from '../edit-haw/edit-haw';

@IonicPage()
@Component({
    selector: 'page-weightaheight-historical',
    templateUrl: 'weightaheight-historical.html',
})
export class WeightaheightHistoricalPage {
    items: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit() {
        this.items = [
            { id: 1, title: 'item1' },
            { id: 2, title: 'item2' },
            { id: 3, title: 'item3' },
            { id: 4, title: 'item4' },
            { id: 5, title: 'item5' },
            { id: 6, title: 'item6' }
        ];
    }
    editItem(item){
        console.log(item);
        this.navCtrl.push(EditHawPage);
    }
    removeItem(item){
        console.log(item);
    }
}