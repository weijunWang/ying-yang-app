import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightaheightHistoricalPage } from './weightaheight-historical';

@NgModule({
  declarations: [
    WeightaheightHistoricalPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightaheightHistoricalPage),
  ],
})
export class WeightaheightHistoricalPageModule {}