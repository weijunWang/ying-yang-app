import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { RecordMovementPage } from '../record-movement/record-movement';
import { OnedayMovementPage } from '../oneday-movement/oneday-movement';

@IonicPage()
@Component({
  selector: 'page-motion',
  templateUrl: 'motion.html',
})
export class MotionPage {
    img_data: any = [{   
        id: 1,
        canEdit: true,
        status: '每日运动',
        dateTime: '',
        bgImg: '../../../assets/imgs/diet-img.png',
        accumulatePoints: '10',
        icon: ''
    }];
    img_data1: any = [{   
        id: 1,
        canEdit: true,
        status: '每日运动',
        dateTime: '',
        bgImg: '../../../assets/imgs/diet-img.png',
        accumulatePoints: '10',
        icon: ''
    }];
    img_data2: any = [{   
        id: 1,
        canEdit: true,
        status: '每日运动',
        dateTime: '',
        bgImg: '../../../assets/imgs/diet-img.png',
        accumulatePoints: '10',
        icon: ''
    }];
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit(){
        let that = this;
        this.http.get('/api/get_motions', {}, function (res) {
            console.log(res);
            if (res) {
                that.img_data = res.data;
                that.img_data1 = [];
                that.img_data2 = [];
                for (let i = 0; i < that.img_data.length; i++) {
                    if (i % 2 == 0) {
                        that.img_data1.push(that.img_data[i])
                    } else {
                        that.img_data2.push(that.img_data[i])
                    }
                }
                // console.log();
                // let box_img = document.getElementsByClassName('box_img');
                // console.log(box_img);
            }
        })
        // this.getNode();
        // this.img_data1 = [];
        // this.img_data2 = [];
        // this.img_data = [
        //     {   
        //         id: 1,
        //         canEdit: true,
        //         status: '每日运动',
        //         dateTime: '',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //         icon: ''
        //     },{
        //         id: 2,
        //         canEdit: false,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //         icon: '../../../assets/imgs/mothon-icon1.png'
        //     },{
        //         id: 3,
        //         canEdit: true,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //         icon: ''
        //     },{
        //         id: 4,
        //         canEdit: false,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //         icon: '../../../assets/imgs/mothon-icon3.png'
        //     },{
        //         id: 5,
        //         canEdit: false,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //         icon: '../../../assets/imgs/mothon-icon4.png'
        //     },{
        //         id: 6,
        //         canEdit: true,
        //         status: '孕38周+3天',
        //         dateTime: '2018年10月20日',
        //         bgImg: '../../../assets/imgs/diet-img.png',
        //         accumulatePoints: '10',
        //         icon: '../../../assets/imgs/mothon-icon5.png'
        //     }
        // ]
        // for(let i = 0;i < this.img_data.length;i++){
        //     if(i % 2 == 0){
        //         this.img_data1.push(this.img_data[i])
        //     }else{
        //         this.img_data2.push(this.img_data[i])
        //     }
        // }
        // let box_img = document.getElementsByClassName('box_img');
        // console.log(box_img);
    }
    toPage(id){
        if(id == 1){
            this.navCtrl.push(RecordMovementPage);   
        }else{
            this.navCtrl.push(OnedayMovementPage);
        }
    }
}