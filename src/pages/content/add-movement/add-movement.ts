import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
    selector: 'page-add-movement',
    templateUrl: 'add-movement.html',
})
export class AddMovementPage {
    listData: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
    }
    toPage(){
        this.navCtrl.pop();
    }
}