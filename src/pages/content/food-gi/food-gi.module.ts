import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FoodGiPage } from './food-gi';

@NgModule({
  declarations: [
    FoodGiPage,
  ],
  imports: [
    IonicPageModule.forChild(FoodGiPage),
  ],
})
export class FoodGiPageModule {}