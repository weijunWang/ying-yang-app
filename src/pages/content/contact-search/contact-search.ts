import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-contact-search',
  templateUrl: 'contact-search.html',
})
export class ContactSearchPage {
    allData: any;;
    keyword = "";
    storgeData: any;
    searching: boolean = false;;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    
    ngOnInit() {
      this.storgeData = [
          {
          id: 1,
          text: "孕早期禁食有哪些？",
        },{
          id: 2,
          text: "孕期运动",
        },{
          id: 3,
          text: "孕早期",
        },{
          id: 4,
          text: "妊娠期",
        }
      ]
      this.allData = [
        {
          id: 1,
          title: "孕早期禁食有哪些？",
          content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
          type: '知识库',
          stage: '孕早期'
        },{
          id: 2,
          title: "孕期运动",
          content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
          type: '知识库',
          stage: '孕早期'
        },{
          id: 3,
          title: "如何进行宝宝胎教，做哪些事情比较好",
          content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
          type: '知识库',
          stage: '孕早期'
        },{
          id: 4,
          title: "妊娠期",
          content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
          type: '知识库',
          stage: '孕早期'
        }
      ]
      console.log('ionViewDidLoad KnowledgeBasePage');
    }
    toPage(id){
    }
    inputFocus(){
      if(this.keyword != ''){
        this.searching = true;
      }
    }
    clearAll(){
      this.storgeData = [];
    }
}