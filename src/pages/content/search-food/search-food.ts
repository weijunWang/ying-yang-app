import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
    selector: 'page-search-food',
    templateUrl: 'search-food.html',
})
export class SearchFoodPage {
    allData: any;;
    keyword = "";
    storgeData: any;
    searching: boolean = false;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    
    ngOnInit() {
      this.storgeData = [
          {
          id: 1,
          text: "孕早期禁食有哪些？",
        },{
          id: 2,
          text: "孕期运动",
        },{
          id: 3,
          text: "孕早期",
        },{
          id: 4,
          text: "妊娠期",
        }
      ]
      this.allData = [
        {
            id: 1,
            icon: '../../../assets/imgs/food-icon1.png',
            name: '生菜',
            value: '75'
        }
      ]
      console.log('ionViewDidLoad KnowledgeBasePage');
    }
    inputFocus(){
      if(this.keyword != ''){
        this.searching = true;
      }
    }
    clearAll(){
      this.storgeData = [];
    }
}