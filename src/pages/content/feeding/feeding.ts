import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';

@IonicPage()
@Component({
  selector: 'page-feeding',
  templateUrl: 'feeding.html',
})
export class FeedingPage {
  listData: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider,
    public dataserver: DataServersProvider) {
  }

  ngOnInit() {
    this.listData = [
      {
        type: 1,
        text: '母乳喂养',
        hide: true
      }, {
        type: 2,
        text: '辅食喂养',
        hide: true
      }, {
        type: 3,
        text: '混合喂养',
        hide: true
      }
    ]
    //get_feed
    let that = this;
    this.http.get('/api/get_feed/', {}, function (res) {
      console.log(res);
      if (res) {
        for (let item = 0; item < that.listData.length; item++) {
          if(that.listData[item].type == res.feed_type){
            console.log(item);
            that.listData[item].hide = false;
          }
        }
      }
    })
    
    
    console.log('ionViewDidLoad FeedingPage');
  } //change_feed_type
  toNextPage(i) {
    let text;
    for (let item = 0; item < this.listData.length; item++) {
      this.listData[item].hide = true;
      text = this.listData[item].type;
    }
    let data = {
      "feed_type": this.listData[i].type
    }
    this.http.post('/api/feeding/', data, function (res) {
      console.log(res);
      if (res) {

      }
    }, function (err) {
      console.log(err);
    })
    this.listData[i].hide = false;
    let that = this;
    setTimeout(function () {
      that.navCtrl.pop();
    }, 1000)
  }
}
