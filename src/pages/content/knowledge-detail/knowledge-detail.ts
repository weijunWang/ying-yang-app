import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';

@IonicPage()
@Component({
  selector: 'page-knowledge-detail',
  templateUrl: 'knowledge-detail.html',
})
export class KnowledgeDetailPage {
  allData: any = {
    "stage": "",
    "title": "",
    "create_time": "",
    "article_img": "",
    "author": {
      "author_title": "",
      "author_name": "",
      "author_remark": []
    },
    "article_subheading": "",
    "body": "",
    "is_collection": "",
    "collection_time": ""
  };
  messageData: [
    {
      "id": "",
      "article_uid": "",
      "username": "",
      "header_img": "",
      "content": "",
      "likes": "",
      "reply": [
        {
          "reply_id": "",
          "replay_name": "",
          "replay_header_img": "",
          "replay_message": "",
          "likes": ""
        }
      ]
    }
  ];
  userinfo: any;
  keyword = "";
  pageId: any;
  collectionUrl: string = '../../../assets/imgs/shoucang.png';
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public http: HttpServerProvider,
    public storage: StorageServeProvider) {
  }

  ngOnInit() {
    let that = this;
    this.pageId = this.navParams.data.id;
    this.userinfo = this.storage.getStorage('userinfo');
    this.http.get('/api/get_articles', {}, function (res) {
      console.log(res);
      if (res) {
        that.allData = res[0];
        let data = {
          "topic_type": 1,
          "topic_id": that.pageId
        }
        that.http.post('/api/get_message/', data, function (res) {
          console.log('get_message',res);
          if (res) {
            that.messageData = res.messages;
          }
        }, function(err){console.log(err);}) 
      }
    }) 
    console.log('ionViewDidLoad KnowledgeDetailPage', that.allData);
  }
  collection(){
    let that = this;
    let data = {
      "is_collection": false,
      "is_like": false,
      "message": "",
      "topic_type": 1,
      "topic_id": this.pageId
    }
    if(!this.allData.is_collection){
      data.is_collection = true;
      this.allData.collectionUrl = '../../../assets/imgs/yishoucang.png'
    }
    this.http.patch('/api/send_message/', data, function (res) {
      console.log(res);
    }, function (err) {
      console.log(err);
    })
  }
  sendMessage(){
    let that = this;
    let data = {
      "is_collection": false,
      "is_like": false,
      "message": this.keyword,
      "topic_type": 1,
      "topic_id": this.pageId
    }
    this.http.post('/api/send_message/', data, function (res) {
      console.log(res);
      let data2 = {
        "topic_type": 1,
        "topic_id": that.pageId
      }
      that.http.post('/api/get_message/', data2, function (res) {
        console.log('get_message',res);
        if (res) {
          that.messageData = res.messages;
        }
      }, function(err){
        console.log(err);
      }) 
    }, function (err) {
      console.log(err);
    })
  }
  replay(item){
    let that = this;
    console.log(item);
    let data = {
      "message_id": item.id,
      "content": item.content,
      "is_like": false
    }
    this.http.post('/api/send_reply/', data, function (res) {
      console.log(res);
      let data2 = {
        "topic_type": 1,
        "topic_id": that.pageId
      }
      that.http.post('/api/get_message/', data2, function (res) {
        console.log('get_message',res);
        if (res) {
          that.messageData = res.messages;
        }
      }, function(err){
        console.log(err);
      }) 
    }, function (err) {
      console.log(err);
    })
  }
}