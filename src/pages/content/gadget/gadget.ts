import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { HttpServerProvider } from '../../../providers/http-server/http-server';
import { StorageServeProvider } from '../../../providers/storage-serve/storage-serve';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import { FetalMovementPage } from '../fetal-movement/fetal-movement';
import { PrenatalEducationPage } from '../prenatal-education/prenatal-education';
import { WeightCurvePage } from '../weight-curve/weight-curve';
import { BirthsSeizedTimePage } from '../births-seized-time/births-seized-time';
import { BabyVaccinationPlanPage } from '../baby-vaccination-plan/baby-vaccination-plan';
import { FoodGiPage } from '../food-gi/food-gi';

@IonicPage()
@Component({
    selector: 'page-gadget',
    templateUrl: 'gadget.html',
})
export class GadgetPage {
    listData: any;
    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public http: HttpServerProvider,
        public storage: StorageServeProvider,
        public dataserver: DataServersProvider) {
    }
    ngOnInit(){
        let that = this;
        // this.http.get('/api/production_schedule', {}, function (res) {
        //     console.log(res);
        //     if (res) {
        //         that.listData = res;
        //     }
        // })
        this.listData = [
            {
                text: '胎教音乐',
                img: '../../../assets/imgs/gadget-icon1.png'
            },{
                text: '记体重',
                img: '../../../assets/imgs/gadget-icon2.png'
            },{
                text: '产检时间',
                img: '../../../assets/imgs/gadget-icon3.png'
            },{
                text: '数胎动',
                img: '../../../assets/imgs/gadget-icon4.png'
            },{
                text: '食物GI值',
                img: '../../../assets/imgs/gadget-icon5.png'
            },{
                text: '宝宝接种计划',
                img: '../../../assets/imgs/gadget-icon6.png'
            }
        ]
    }
    toPage(i){
        switch(i){
            case 0:
                this.navCtrl.push(PrenatalEducationPage);
                break;
            case 1:
                this.navCtrl.push(WeightCurvePage);
                break;
            case 2:
                this.navCtrl.push(BirthsSeizedTimePage);
                break;
            case 3:
                this.navCtrl.push(FetalMovementPage);
                break;
            case 4:
                this.navCtrl.push(FoodGiPage);
                break;
            case 5:
                this.navCtrl.push(BabyVaccinationPlanPage);
                break;
            default:
                break;
        }
    }
}