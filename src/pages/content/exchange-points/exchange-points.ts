import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-exchange-points',
  templateUrl: 'exchange-points.html',
})
export class ExchangePointsPage {
    listData: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
  
    ngOnInit() {
      this.listData = [
        {
          text: '母乳喂养',
          hide: true
        },{
          text: '辅食喂养',
          hide: true
        },{
          text: '混合喂养',
          hide: true
        }
      ]
      console.log('ionViewDidLoad ExchangePointsPage');
    }
    toNextPage(i){
      for(let item = 0; item < this.listData.length; item++){
        this.listData[item].hide = true;
      }
      this.listData[i].hide = false;
    }
}