import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExchangePointsPage } from './exchange-points';

@NgModule({
  declarations: [
    ExchangePointsPage,
  ],
  imports: [
    IonicPageModule.forChild(ExchangePointsPage),
  ],
})
export class ExchangePointsPageModule {}