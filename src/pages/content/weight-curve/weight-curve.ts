import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { DataServersProvider } from '../../../providers/data-servers/data-servers';
import * as echarts from 'echarts';

import { RecordWeightPage } from '../record-weight/record-weight';
// import { WeightHistoricalPage } from '../weight-historical/weight-historical';
import { WeightaheightHistoricalPage } from '../weightaheight-historical/weightaheight-historical';
import { WeightCurveChildPage } from '../weight-curve-child/weight-curve-child';

@IonicPage()
@Component({
    selector: 'page-weight-curve',
    templateUrl: 'weight-curve.html',
})
export class WeightCurvePage {
    constructor(private dataServer: DataServersProvider, public navCtrl: NavController, public navParams: NavParams) {
        console.log(echarts);
    }

    ngOnInit() {/*当进入页面时触发*/
        const ec = echarts as any;
        console.log(this.dataServer.getWeigthsLowerLimit(50, 50)[0].data);
        console.log(this.dataServer.getWeightMiddle(50, 50)[0].data);
        console.log(this.dataServer.getWeigthsUpperLimit(50, 50)[0].data);
        console.log(this.dataServer.getWeightDifference(50,50)[0].data);
        let myChart = ec.init(document.getElementById('chart'));
        let option = {
            // title: {
            //     text: '孕期体重曲线'
            // },
            // tooltip: {
            //     trigger: 'axis'
            // },
            // legend: {
            //     data:['平均值','上限','下限', '']
            // },
            // toolbox: {
            //     feature: {
            //         saveAsImage: {}
            //     }
            // },
            xAxis: [
                {
                    splitLine: {
                        show: false
                    },
                    type: 'category',
                    boundaryGap: false,
                    data: ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20', '21', '22', '23', '24', '25', '26', '27', '28', '29', '30', '31', '32', '33', '34', '35', '36', '37', '38', '39', '40']
                }
            ],
            yAxis: [
                {
                    min: 35,
                    splitLine: {
                        show: false
                    },
                    type: 'value'
                }
            ],
            dataZoom: [{
                type: 'slider',
                show: false
            }, {
                type: "inside"         //详细配置可见echarts官网
            }],
            series: [
                {
                    name: '平均值',
                    // stack: '总量',
                    color: '#F7F04B',
                    symbol:'none',//拐点样式
                    data: this.dataServer.getWeightMiddle(50, 50)[0].data,
                    type: 'line',
                    smooth: true,
                }, {
                    smooth: false,
                    color: '#64A0EB',
                    name: '下限',
                    stack: 'limit',
                    type: 'line',
                    symbol:'none',//拐点样式
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                    areaStyle: {
                        color: '#fff'
                    },
                    data: this.dataServer.getWeigthsLowerLimit(50, 50)[0].data,
                },{
                    smooth: false,
                    color: '#FE7171',
                    name: '上限',
                    stack: 'limit',
                    type: 'line',
                    symbol:'none',//拐点样式
                    areaStyle: {
                        color: '#F5AA9B'
                    },
                    itemStyle: {
                        normal: {
                            lineStyle: {
                                width: 1,
                                type: 'dotted'  //'dotted'虚线 'solid'实线
                            }
                        }
                    },
                    data: this.dataServer.getWeightDifference(50, 50)[0].data
                },  
            ]
        };
        myChart.setOption(option);
    }
    toCalendar() {
        this.navCtrl.push(RecordWeightPage);
    }
    record() {
        console.log('record');
        this.navCtrl.push(WeightaheightHistoricalPage);
    }
    changeCurver(){
        this.navCtrl.push(WeightCurveChildPage);
    }
}