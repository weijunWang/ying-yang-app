import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightCurvePage } from './weight-curve';

@NgModule({
  declarations: [
    WeightCurvePage,
  ],
  imports: [
    IonicPageModule.forChild(WeightCurvePage),
  ],
})
export class WeightCurvePageModule {}