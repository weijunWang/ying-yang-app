import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnedayMovementPage } from './oneday-movement';

@NgModule({
  declarations: [
    OnedayMovementPage,
  ],
  imports: [
    IonicPageModule.forChild(OnedayMovementPage),
  ],
})
export class OnedayMovementPageModule {}