import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';



@IonicPage()
@Component({
    selector: 'page-oneday-movement',
    templateUrl: 'oneday-movement.html',
})
export class OnedayMovementPage {
    listData: any;
    constructor(public navCtrl: NavController, public navParams: NavParams) {
    }
    ngOnInit(){
        this.listData = [
            {   
                id: 1,
                show: false,
                img: '../../../assets/imgs/add-food-icon1.png',
                text: '调羹'
            }, {
                id: 2,
                show: false,
                img: '../../../assets/imgs/add-food-icon2.png',
                text: '碗'
            }, {
                id: 3,
                show: false,
                img: '../../../assets/imgs/add-food-icon3.png',
                text: '杯'
            }, {
                id: 4,
                show: false,
                img: '../../../assets/imgs/add-food-icon4.png',
                text: '盘'
            }
        ]
    }
    choiceItem(id){
        for(let i = 0;i < this.listData.length;i++){
            this.listData[i].show = false;
        }
        this.listData[id - 1].show = true;
    }
}