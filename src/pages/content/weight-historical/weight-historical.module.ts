import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WeightHistoricalPage } from './weight-historical';

@NgModule({
  declarations: [
    WeightHistoricalPage,
  ],
  imports: [
    IonicPageModule.forChild(WeightHistoricalPage),
  ],
})
export class WeightHistoricalPageModule {}