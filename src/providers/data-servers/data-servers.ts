import { Injectable } from '@angular/core';

/*
  Generated class for the DataServersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class DataServersProvider {
  heights: Array<{ name: string, options: Array<{ text: string, value: string }> }> = [
    {
      name: 'height1',
      options: [
        { text: '1', value: '1' },
        { text: '2', value: '2' },
        { text: '3', value: '3' },
        { text: '4', value: '4' },
        { text: '5', value: '5' },
        { text: '6', value: '6' },
        { text: '7', value: '7' },
        { text: '8', value: '8' },
        { text: '9', value: '9' },
        { text: '10', value: '10' },
        { text: '11', value: '11' },
        { text: '12', value: '12' },
        { text: '13', value: '13' },
        { text: '14', value: '14' },
        { text: '15', value: '15' },
        { text: '16', value: '16' },
        { text: '17', value: '17' },
        { text: '18', value: '18' },
        { text: '19', value: '19' },
        { text: '20', value: '20' },
        { text: '21', value: '21' },
        { text: '22', value: '22' },
        { text: '23', value: '23' },
        { text: '24', value: '24' },
        { text: '25', value: '25' },
        { text: '26', value: '26' },
        { text: '27', value: '27' },
        { text: '28', value: '28' },
        { text: '29', value: '29' },
        { text: '30', value: '30' },
        { text: '31', value: '31' },
        { text: '32', value: '32' },
        { text: '33', value: '33' },
        { text: '34', value: '34' },
        { text: '35', value: '35' },
        { text: '36', value: '36' },
        { text: '37', value: '37' },
        { text: '38', value: '38' },
        { text: '39', value: '39' },
        { text: '40', value: '40' },
        { text: '41', value: '41' },
        { text: '42', value: '42' },
        { text: '43', value: '43' },
        { text: '44', value: '44' },
        { text: '45', value: '45' },
        { text: '46', value: '46' },
        { text: '47', value: '47' },
        { text: '48', value: '48' },
        { text: '49', value: '49' },
        { text: '50', value: '50' },
        { text: '51', value: '51' },
        { text: '52', value: '52' },
        { text: '53', value: '53' },
        { text: '54', value: '54' },
        { text: '55', value: '55' },
        { text: '56', value: '56' },
        { text: '57', value: '57' },
        { text: '58', value: '58' },
        { text: '59', value: '59' },
        { text: '60', value: '60' },
        { text: '61', value: '61' },
        { text: '62', value: '62' },
        { text: '63', value: '63' },
        { text: '64', value: '64' },
        { text: '65', value: '65' },
        { text: '66', value: '66' },
        { text: '67', value: '67' },
        { text: '68', value: '68' },
        { text: '69', value: '69' },
        { text: '70', value: '70' },
        { text: '71', value: '71' },
        { text: '72', value: '72' },
        { text: '73', value: '73' },
        { text: '74', value: '74' },
        { text: '75', value: '75' },
        { text: '76', value: '76' },
        { text: '77', value: '77' },
        { text: '78', value: '78' },
        { text: '79', value: '79' },
        { text: '80', value: '80' },
        { text: '81', value: '81' },
        { text: '82', value: '82' },
        { text: '83', value: '83' },
        { text: '84', value: '84' },
        { text: '85', value: '85' },
        { text: '86', value: '86' },
        { text: '87', value: '87' },
        { text: '88', value: '88' },
        { text: '89', value: '89' },
        { text: '90', value: '90' },
        { text: '91', value: '91' },
        { text: '92', value: '92' },
        { text: '93', value: '93' },
        { text: '94', value: '94' },
        { text: '95', value: '95' },
        { text: '96', value: '96' },
        { text: '97', value: '97' },
        { text: '98', value: '98' },
        { text: '99', value: '99' },
        { text: '100', value: '100' },
        { text: '101', value: '101' },
        { text: '102', value: '102' },
        { text: '103', value: '103' },
        { text: '104', value: '104' },
        { text: '105', value: '105' },
        { text: '106', value: '106' },
        { text: '107', value: '107' },
        { text: '108', value: '108' },
        { text: '109', value: '109' },
        { text: '110', value: '110' },
        { text: '111', value: '111' },
        { text: '112', value: '112' },
        { text: '113', value: '113' },
        { text: '114', value: '114' },
        { text: '115', value: '115' },
        { text: '116', value: '116' },
        { text: '117', value: '117' },
        { text: '118', value: '118' },
        { text: '119', value: '119' },
        { text: '120', value: '120' },
        { text: '121', value: '121' },
        { text: '122', value: '122' },
        { text: '123', value: '123' },
        { text: '124', value: '124' },
        { text: '125', value: '125' },
        { text: '126', value: '126' },
        { text: '127', value: '127' },
        { text: '128', value: '128' },
        { text: '129', value: '129' },
        { text: '130', value: '130' },
        { text: '131', value: '131' },
        { text: '132', value: '132' },
        { text: '133', value: '133' },
        { text: '134', value: '134' },
        { text: '135', value: '135' },
        { text: '136', value: '136' },
        { text: '137', value: '137' },
        { text: '138', value: '138' },
        { text: '139', value: '139' },
        { text: '140', value: '140' },
        { text: '141', value: '141' },
        { text: '142', value: '142' },
        { text: '143', value: '143' },
        { text: '144', value: '144' },
        { text: '145', value: '145' },
        { text: '146', value: '146' },
        { text: '147', value: '147' },
        { text: '148', value: '148' },
        { text: '149', value: '149' },
        { text: '150', value: '150' },
        { text: '130', value: '130' },
        { text: '131', value: '131' },
        { text: '132', value: '132' },
        { text: '133', value: '133' },
        { text: '134', value: '134' },
        { text: '135', value: '135' },
        { text: '136', value: '136' },
        { text: '137', value: '137' },
        { text: '138', value: '138' },
        { text: '139', value: '139' },
        { text: '140', value: '140' },
        { text: '141', value: '141' },
        { text: '142', value: '142' },
        { text: '143', value: '143' },
        { text: '144', value: '144' },
        { text: '145', value: '145' },
        { text: '146', value: '146' },
        { text: '147', value: '147' },
        { text: '148', value: '148' },
        { text: '149', value: '149' },
        { text: '150', value: '150' },
        { text: '151', value: '151' },
        { text: '152', value: '152' },
        { text: '153', value: '153' },
        { text: '154', value: '154' },
        { text: '155', value: '155' },
        { text: '156', value: '156' },
        { text: '157', value: '157' },
        { text: '158', value: '158' },
        { text: '159', value: '159' },
        { text: '160', value: '160' },
        { text: '161', value: '161' },
        { text: '162', value: '162' },
        { text: '163', value: '163' },
        { text: '164', value: '164' },
        { text: '165', value: '165' },
        { text: '166', value: '166' },
        { text: '167', value: '167' },
        { text: '168', value: '168' },
        { text: '169', value: '169' },
        { text: '170', value: '170' },
        { text: '171', value: '171' },
        { text: '172', value: '172' },
        { text: '173', value: '173' },
        { text: '174', value: '174' },
        { text: '175', value: '175' },
        { text: '176', value: '176' },
        { text: '177', value: '177' },
        { text: '178', value: '178' },
        { text: '179', value: '179' },
        { text: '180', value: '180' },
        { text: '181', value: '181' },
        { text: '182', value: '182' },
        { text: '183', value: '183' },
        { text: '184', value: '184' },
        { text: '185', value: '185' },
        { text: '186', value: '186' },
        { text: '187', value: '187' },
        { text: '188', value: '188' },
        { text: '189', value: '189' },
        { text: '190', value: '190' },
      ]
    }, {
      name: 'height3',
      options: [
        { text: '.', value: '.' }
      ]
    }, {
      name: 'height2',
      options: [
        { text: '0', value: '0' },
        { text: '1', value: '1' },
        { text: '2', value: '2' },
        { text: '3', value: '3' },
        { text: '4', value: '4' },
        { text: '5', value: '5' },
        { text: '6', value: '6' },
        { text: '7', value: '7' },
        { text: '8', value: '8' },
        { text: '9', value: '9' }
      ]
    }
  ]
  weights: Array<{ name: string, options: Array<{ text: string, value: string }> }> = [
    {
      name: 'weight1',
      options: [
        { text: '1', value: '1' },
        { text: '2', value: '2' },
        { text: '3', value: '3' },
        { text: '4', value: '4' },
        { text: '5', value: '5' },
        { text: '6', value: '6' },
        { text: '7', value: '7' },
        { text: '8', value: '8' },
        { text: '9', value: '9' },
        { text: '10', value: '10' },
        { text: '11', value: '11' },
        { text: '12', value: '12' },
        { text: '13', value: '13' },
        { text: '14', value: '14' },
        { text: '15', value: '15' },
        { text: '16', value: '16' },
        { text: '17', value: '17' },
        { text: '18', value: '18' },
        { text: '19', value: '19' },
        { text: '20', value: '20' },
        { text: '21', value: '21' },
        { text: '22', value: '22' },
        { text: '23', value: '23' },
        { text: '24', value: '24' },
        { text: '25', value: '25' },
        { text: '26', value: '26' },
        { text: '27', value: '27' },
        { text: '28', value: '28' },
        { text: '29', value: '29' },
        { text: '30', value: '30' },
        { text: '31', value: '31' },
        { text: '32', value: '32' },
        { text: '33', value: '33' },
        { text: '34', value: '34' },
        { text: '35', value: '35' },
        { text: '36', value: '36' },
        { text: '37', value: '37' },
        { text: '38', value: '38' },
        { text: '39', value: '39' },
        { text: '40', value: '40' },
        { text: '41', value: '41' },
        { text: '42', value: '42' },
        { text: '43', value: '43' },
        { text: '44', value: '44' },
        { text: '45', value: '45' },
        { text: '46', value: '46' },
        { text: '47', value: '47' },
        { text: '48', value: '48' },
        { text: '49', value: '49' },
        { text: '50', value: '50' },
        { text: '51', value: '51' },
        { text: '52', value: '52' },
        { text: '53', value: '53' },
        { text: '54', value: '54' },
        { text: '55', value: '55' },
        { text: '56', value: '56' },
        { text: '57', value: '57' },
        { text: '58', value: '58' },
        { text: '59', value: '59' },
        { text: '60', value: '60' },
        { text: '61', value: '61' },
        { text: '62', value: '62' },
        { text: '63', value: '63' },
        { text: '64', value: '64' },
        { text: '65', value: '65' },
        { text: '66', value: '66' },
        { text: '67', value: '67' },
        { text: '68', value: '68' },
        { text: '69', value: '69' },
        { text: '70', value: '70' },
        { text: '71', value: '71' },
        { text: '72', value: '72' },
        { text: '73', value: '73' },
        { text: '74', value: '74' },
        { text: '75', value: '75' },
        { text: '76', value: '76' },
        { text: '77', value: '77' },
        { text: '78', value: '78' },
        { text: '79', value: '79' },
        { text: '80', value: '80' },
        { text: '81', value: '81' },
        { text: '82', value: '82' },
        { text: '83', value: '83' },
        { text: '84', value: '84' },
        { text: '85', value: '85' },
        { text: '86', value: '86' },
        { text: '87', value: '87' },
        { text: '88', value: '88' },
        { text: '89', value: '89' },
        { text: '90', value: '90' },
        { text: '91', value: '91' },
        { text: '92', value: '92' },
        { text: '93', value: '93' },
        { text: '94', value: '94' },
        { text: '95', value: '95' },
        { text: '96', value: '96' },
        { text: '97', value: '97' },
        { text: '98', value: '98' },
        { text: '99', value: '99' },
        { text: '100', value: '100' },
        { text: '101', value: '101' },
        { text: '102', value: '102' },
        { text: '103', value: '103' },
        { text: '104', value: '104' },
        { text: '105', value: '105' },
        { text: '106', value: '106' },
        { text: '107', value: '107' },
        { text: '108', value: '108' },
        { text: '109', value: '109' },
        { text: '110', value: '110' },
        { text: '111', value: '111' },
        { text: '112', value: '112' },
        { text: '113', value: '113' },
        { text: '114', value: '114' },
        { text: '115', value: '115' },
        { text: '116', value: '116' },
        { text: '117', value: '117' },
        { text: '118', value: '118' },
        { text: '119', value: '119' },
        { text: '120', value: '120' }
      ]
    }, {
      name: 'weight3',
      options: [
        { text: '.', value: '.' }
      ]
    }, {
      name: 'weight2',
      options: [
        { text: '0', value: '0' },
        { text: '1', value: '1' },
        { text: '2', value: '2' },
        { text: '3', value: '3' },
        { text: '4', value: '4' },
        { text: '5', value: '5' },
        { text: '6', value: '6' },
        { text: '7', value: '7' },
        { text: '8', value: '8' },
        { text: '9', value: '9' }
      ]
    }
  ]

  feedType: Array<{ name: string, options: Array<{ text: string, value: string }> }> = [
    {
      name: 'type',
      options: [
        { text: '母乳喂养', value: '1' },
        { text: '辅食喂养', value: '2' },
        { text: '混合喂养', value: '3' }
      ]
    }
  ]
  birthTypes: Array<{ name: string, options: Array<{ text: string, value: string }> }> = [
    {
      name: 'birthType',
      options: [
        { text: '顺产', value: '1' },
        { text: '剖宫产', value: '2' }
      ]
    }
  ]
  chiHeights: Array<{ num: Number }> = [
    {
      num: 100
    }, {
      num: 110
    }, {
      num: 120
    }, {
      num: 130
    }, {
      num: 140
    }, {
      num: 150
    }, {
      num: 160
    }, {
      num: 170
    }, {
      num: 180
    }, {
      num: 190
    }, {
      num: 200
    }, {
      num: 210
    }
  ]
  chiWeights: Array<{ num: Number }> = [
    {
      num: 10
    }, {
      num: 20
    }, {
      num: 30
    }, {
      num: 40
    }, {
      num: 50
    }, {
      num: 60
    }, {
      num: 70
    }, {
      num: 80
    }, {
      num: 90
    }, {
      num: 100
    }, {
      num: 110
    }
  ]
  chiMinutes: Array<{ num: Number }> = [
    {
      num: 10
    }, {
      num: 20
    }, {
      num: 30
    }, {
      num: 40
    }, {
      num: 50
    }, {
      num: 60
    }, {
      num: 70
    }, {
      num: 80
    }, {
      num: 90
    }, {
      num: 100
    }, {
      num: 110
    }, {
      num: 120
    }, {
      num: 130
    }, {
      num: 140
    }, {
      num: 150
    }, {
      num: 10
    }, {
      num: 20
    }, {
      num: 30
    }, {
      num: 40
    }, {
      num: 50
    }, {
      num: 60
    }, {
      num: 70
    }, {
      num: 80
    }, {
      num: 90
    }, {
      num: 100
    }, {
      num: 110
    }, {
      num: 120
    }, {
      num: 130
    }, {
      num: 140
    }, {
      num: 150
    }, {
      num: 160
    }, {
      num: 170
    }, {
      num: 180
    }, {
      num: 190
    }, {
      num: 200
    }, {
      num: 210
    }, {
      num: 220
    }, {
      num: 230
    }, {
      num: 240
    }, {
      num: 250
    }, {
      num: 260
    }, {
      num: 270
    }, {
      num: 280
    }, {
      num: 290
    }, {
      num: 300
    }
  ]
  chiSleep: Array<{ num: Number }> = [
    {
      num: 0.5
    }, {
      num: 1
    }, {
      num: 1.5
    }, {
      num: 2
    }, {
      num: 2.5
    }, {
      num: 3
    }, {
      num: 3.5
    }, {
      num: 4
    }, {
      num: 4.5
    }, {
      num: 5
    }, {
      num: 5.5
    }, {
      num: 6
    }, {
      num: 6.5
    }, {
      num: 7
    }, {
      num: 7.5
    }, {
      num: 8
    }, {
      num: 8.5
    }, {
      num: 9
    }, {
      num: 9.5
    }, {
      num: 10
    }, {
      num: 10.5
    }, {
      num: 11
    }, {
      num: 11.5
    }, {
      num: 12
    }, {
      num: 12.5
    }, {
      num: 13
    }, {
      num: 13.5
    }
  ]

  // 孕妇增重上、中、下限
  weigtsLimit: Array<{ type: Number, data: Array<Number> }> = [
    {
      type: 1,
      data: []
    }, {
      type: 2,
      data: []
    }, {
      type: 3,
      data: []
    }, {
      type: 4,
      data: []
    }
  ]
  constructor() {
    console.log('Hello DataServersProvider Provider');
  }
  getWeigthsLowerLimit(initHeight, initWeight) {
    let weigtsLimit = [
      {
        type: 1,
        data: []
      }, {
        type: 2,
        data: []
      }, {
        type: 3,
        data: []
      }, {
        type: 4,
        data: []
      }
    ];
    let initNum = 0;
    let initArr = [];
    let lowerLimit = 0.5
    let weeks = 12;
    let lower = [0.44, 0.35, 0.23, 0.17];
    for (let i = 0; i < 11; i++) {
      initNum = Number((initNum + (lowerLimit / weeks)).toFixed(3));
      initArr.push(initNum + initWeight);
    }
    initNum = 0.50;
    initArr.push(initNum + initWeight);

    weigtsLimit.forEach(data => {
      data.data = initArr;
      for (let i = 0; i < 28; i++) {
        // console.log(lower[Number(data.type) - 1]);
        initNum = initNum + lower[Number(data.type) - 1];
        data.data.push(initNum + initWeight);
      }
      initArr = initArr.slice(0, 12);
      initNum = 0.50;
      // console.log(initArr);
      // console.log(data);
    })

    return weigtsLimit
  }
  getWeigthsUpperLimit(initHeight, initWeight) {
    let weigtsLimit = [
      {
        type: 1,
        data: []
      }, {
        type: 2,
        data: []
      }, {
        type: 3,
        data: []
      }, {
        type: 4,
        data: []
      }
    ];
    let initNum = 0;
    let initArr = [];
    let upperLimit = 2;
    let weeks = 12;
    let upper = [0.58, 1.50, 0.33, 0.27];
    for (let i = 0; i < 11; i++) {
      initNum = Number((initNum + (upperLimit / weeks)).toFixed(3));
      initArr.push(initNum + initWeight);
    }
    initNum = 2;
    initArr.push(initNum + initWeight);

    weigtsLimit.forEach(data => {
      data.data = initArr;
      for (let i = 0; i < 28; i++) {
        // console.log(upper[Number(data.type) - 1]);
        initNum = initNum + upper[Number(data.type) - 1];
        data.data.push(initNum + initWeight);
      }
      initArr = initArr.slice(0, 12);
      initNum = 2;
      // console.log(initArr);
      // console.log(data);
    })
    return weigtsLimit
  }
  getWeightMiddle(initHeight, initWeight) {
    let weigtsLimit = [
      {
        type: 1,
        data: []
      }, {
        type: 2,
        data: []
      }, {
        type: 3,
        data: []
      }, {
        type: 4,
        data: []
      }
    ];
    // console.log(weigtsLimit);
    let weigthsUpperLimit = this.getWeigthsUpperLimit(initHeight, initWeight)
    let weigthsLowerLimit = this.getWeigthsLowerLimit(initHeight, initWeight)
    weigtsLimit.forEach(data => {
      for (let i = 0; i < 40; i++) {
        let type = data.type.toString()
        let weigthsUpperLimitNum = weigthsUpperLimit[Number(type) - 1]['data'][i];
        let weigthsLowerLimitNum = weigthsLowerLimit[Number(type) - 1]['data'][i];

        data.data.push((Number(weigthsUpperLimitNum) + Number(weigthsLowerLimitNum)) / 2);
      }
    })
    return weigtsLimit
  }
  getWeightDifference(initHeight, initWeight) {
    let lower = [0.44, 0.35, 0.23, 0.17];
    let upper = [0.58, 1.50, 0.33, 0.27];
    let initNum = 0;
    let upperLimit = 2;
    let lowerLimit = 0.5
    let weeks = 12;
    let weigtsLimit = [{
      type: 1,
      data: []
    }, {
      type: 2,
      data: []
    }, {
      type: 3,
      data: []
    }, {
      type: 4,
      data: []
    }]
    weigtsLimit.forEach(data => {
      for (let i = 0; i < 40; i++) {
        // let type = data.type.toString();
        if (i < 12) {
          initNum = Number((initNum + (upperLimit - lowerLimit) / weeks));
          data.data.push(initNum);
        } else {
          initNum = initNum + upper[Number(data.type) - 1] - lower[Number(data.type) - 1];
          data.data.push(initNum);
        }
      }
    })
    return weigtsLimit;
  }
  getChildCurveData(sex, percent, type) {
    let data = {
      girl: {
        percent3: {
          height: [46.6, 50.85, 53.4, 57.45, 59.1, 61.15, 62.5, 64.95, 66.6, 67.4, 68.7, 69.5, 70.0],
          weight: [2.57, 3.5, 4.21, 5.01, 5.55, 5.85, 6.34, 6.65, 6.95, 7.11, 7.34, 7.56, 7.70]
        },
        percent25: {
          height: [49.2, 54.85, 57.2, 60.45, 63.0, 65.15, 66.8, 67.95, 69.6, 70.9, 71.7, 73.6, 74.7],
          weight: [2.96, 3.5, 4.82, 5.01, 5.44, 5.85, 6.21, 6.65, 7.95, 8.08, 8.34, 8.56, 8.74]
        },

        percent50: {
          height: [49.7, 53.85, 57.4, 59.45, 63.1, 64.75, 66.8, 67.95, 68.6, 71.0, 63.7, 74.0, 75.0],
          weight: [3.21, 4.5, 5.21, 6.01, 6.83, 7.35, 7.77, 7.95, 8.25, 8.69, 8.74, 9.11, 9.40]
        },
        percent75: {
          height: [50.9, 55.85, 58.9, 61.45, 64.6, 66.15, 68.4, 69.95, 70.9, 72.8, 73.7, 74.8, 76.8],
          weight: [3.49, 4.5, 5.64, 6.01, 7.37, 7.95, 8.37, 8.65, 8.95, 9.16, 9.54, 10.0, 10.32]
        },
        percent97: {
          height: [53.0, 57.6, 61.6, 65.4, 67.4, 69.2, 71.2, 73.9, 74.9, 75.9, 77.8, 79.2, 80.2],
          weight: [4.04, 5.5, 6.51, 7.41, 8.47, 9.95, 9.59, 9.75, 10.25, 10.71, 10.94, 11.0, 11.57]
        }
      },
      boy: {
        percent3: {
          height: [47.1, 50.85, 54.6, 57.45, 60.3, 62.15, 64.0, 65.95, 66.6, 69.7, 69.7, 70.6, 71.5],
          weight: [2.62, 4.13, 4.53, 4.82, 5.09, 5.55, 6.80, 7.11, 7.32, 7.56, 7.67, 7.90, 8.16]
        },
        percent25: {
          height: [49.2, 54.85, 57.2, 60.45, 63.0, 65.15, 66.8, 67.95, 69.6, 70.9, 71.7, 73.6, 74.7],
          weight: [3.06, 4.13, 5.25, 5.82, 6.90, 7.35, 7.80, 8.11, 8.42, 8.66, 8.87, 9.10, 9.33]
        },

        percent50: {
          height: [50.4, 56.85, 58.7, 60.45, 64.6, 67.15, 68.4, 69.95, 71.6, 72.6, 73.7, 74.6, 76.5],
          weight: [3.32, 4.5, 5.68, 6.82, 7.45, 7.95, 8.41, 8.81, 9.02, 9.33, 9.67, 9.90, 10.08]
        },
        percent75: {
          height: [51.6, 56.85, 60.3, 64.45, 66.2, 68.15, 70.0, 71.95, 72.6, 74.4, 75.7, 77.6, 78.4],
          weight: [3.59, 4.93, 6.15, 7.42, 8.04, 8.55, 9.07, 9.31, 9.82, 10.06, 10.37, 10.70, 10.86]
        },
        percent97: {
          height: [53.8, 58.85, 63.0, 67.45, 69.0, 70.15, 73.0, 74.95, 76.6, 77.5, 78.7, 80.6, 81.8],
          weight: [4.12, 6.13, 7.05, 7.82, 9.2, 9.85, 10.37, 10.81, 11.12, 11.49, 11.67, 11.90, 12.37]
        }
      }
    }
    return data[sex][percent][type];
  }
  getHeightsData() {
    return this.heights;
  }
  getWeightsData() {
    return this.weights;
  }
  getFeedType() {
    return this.feedType;
  }
  getBirthTypes() {
    return this.birthTypes;
  }
  getChiHeights() {
    return this.chiHeights;
  }
  getChiWeights() {
    return this.chiWeights;
  }
  getChiMinutes() {
    return this.chiMinutes;
  }
  getChiSleep() {
    return this.chiSleep;
  }
  // 删除空格
  deleteSpace(str) {
    if (str) {
      let strings = '';
      console.log(str);
      let array = str.split('');
      for (let i = 0; i < array.length; i++) {
        if (array[i] != ' ') {
          strings = strings + array[i];
        }
      }
      return strings;
    }
  }
  // 对象根据属性值排序
  sortObjValue(prot){
    return function (a, b){
      return a[prot] - b[prot];
    }
  }
}
