import { Injectable } from '@angular/core';
// import { Camera, CameraOptions } from '@ionic-native/camera';
/*
  Generated class for the CordovaServersProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class CordovaServersProvider {

  constructor() {
    console.log('Hello CordovaServersProvider Provider');

  }
  getTest(){
    return 'camera'
  }
  // openCamera() {
  //   const options: CameraOptions = {
  //     quality: 90,                                                   //相片质量 0 -100
  //     destinationType: this.camera.DestinationType.DATA_URL,        //DATA_URL 是 base64   FILE_URL 是文件路径
  //     encodingType: this.camera.EncodingType.JPEG,
  //     mediaType: this.camera.MediaType.PICTURE,
  //     saveToPhotoAlbum: true,                                       //是否保存到相册
  //     // sourceType: this.camera.PictureSourceType.CAMERA ,         //是打开相机拍照还是打开相册选择  PHOTOLIBRARY : 相册选择, CAMERA : 拍照,
  //   }

  //   this.camera.getPicture(options).then((imageData) => {
  //     console.log("got file: " + imageData);

  //     // If it's base64:
  //     let base64Image = 'data:image/jpeg;base64,' + imageData;
  //     return base64Image;

  //   }, (err) => {
  //     // Handle error
  //   });
  // }
}
