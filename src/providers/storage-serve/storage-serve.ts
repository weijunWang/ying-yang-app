import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the StorageServeProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class StorageServeProvider {

  constructor(public http: HttpClient) {
    console.log('Hello StorageServeProvider Provider');
  }
  
  setStorage(key, value){
    window.localStorage.setItem(key, JSON.stringify(value));
  }

  getStorage(key){
    return JSON.parse(window.localStorage.getItem(key));
  }

  removeStorage(key){
    window.localStorage.removeItem(key);
  }

  clearStorage(){
    window.localStorage.clear();
  }
}
