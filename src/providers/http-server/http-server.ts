import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { LoadingController } from 'ionic-angular';


@Injectable()
export class HttpServerProvider {

  constructor(public http: HttpClient, public loadingCtrl: LoadingController) {
    console.log('Hello HttpServerProvider Provider');
  }
  // get请求
  public get(url: string, params: any, callback): any {

    let loading = this.loadingCtrl.create({
      content: '加载数据中...'//数据加载中显示
    });
    //显示等待样式
    loading.present();

    return this.http.get(url, {
      params: params,
      headers: this.getHeaders(),
    }).subscribe(res => {
      loading.dismiss();//显示多久消失
      callback && callback(res)
    });
  }
  // post请求
  public post(url: string, datas: any, successBack, errBack): any {
    let loading = this.loadingCtrl.create({
      content: '加载数据中...'//数据加载中显示
    });
    //显示等待样式
    loading.present();
    return this.http.post(url, JSON.stringify(datas), {
      headers: this.getHeaders(),
    }).subscribe(res => {
      loading.dismiss();//显示多久消失
      successBack && successBack(res);
    }, err => {
      loading.dismiss();//显示多久消失
      errBack && errBack(err);
    })
  }
  // put请求
  public put(url: string, datas: any, successBack, errBack): any {
    let loading = this.loadingCtrl.create({
      content: '加载数据中...'//数据加载中显示
    });
    //显示等待样式
    loading.present();
    return this.http.post(url, JSON.stringify(datas), {
      headers: this.getHeaders(),
    }).subscribe(res => {
      loading.dismiss();//显示多久消失
      successBack && successBack(res);
    }, err => {
      loading.dismiss();//显示多久消失
      errBack && errBack(err);
    })
  }
  // patch请求
  public patch(url: string, datas: any, successBack, errBack): any {
    let loading = this.loadingCtrl.create({
      content: '加载数据中...'//数据加载中显示
    });
    //显示等待样式
    loading.present();
    return this.http.patch(url, JSON.stringify(datas), {
      headers: this.getHeaders(),
    }).subscribe(res => {
      loading.dismiss();//显示多久消失
      successBack && successBack(res);
    }, err => {
      loading.dismiss();//显示多久消失
      errBack && errBack(err);
    })
  }
  // delete请求
  public delete(url: string, params: any, callback): any {

    let loading = this.loadingCtrl.create({
      content: '加载数据中...'//数据加载中显示
    });
    //显示等待样式
    loading.present();
    return this.http.get(url, {
      params: params,
      headers: this.getHeaders(),
    }).subscribe(res => {
      loading.dismiss();//显示多久消失
      callback && callback(res)
    });
  }

  private getHeaders(){
    const token = this.getToken();
    // console.log('token', token);
    return token ? new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'Token ' + token
    }) : null;
  }
  getToken(){
    if(window.localStorage.token){
      return JSON.parse(window.localStorage.getItem('token'));
    }else{
      return false;
    }
  }
}
