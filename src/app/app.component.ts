import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// import { RegisterPage } from '../pages/register/register';
// import { PhysicalDataPage } from '../pages/physical-data/physical-data';
// import { TabsPage } from '../pages/tabs/tabs';
// import { NewsPage } from '../pages/content/news/news';
import { LoginPage } from '../pages/login/login';
// import { RecordMovementPage } from '../pages/content/record-movement/record-movement';
// import { WeightCurveChildPage } from '../pages/content/weight-curve-child/weight-curve-child';
// import { WeightCurvePage } from '../pages/content/weight-curve/weight-curve';
// import { KnowledgesPage } from '../pages/content/knowledges/knowledges';
// import { PhysicalDataPage } from '../pages/physical-data/physical-data';
// import { FaqPage } from '../pages/content/faq/faq';
// import { RecordPage } from '../pages/tabs/record/record';
// import { RecipesPage } from '../pages/content/recipes/recipes';
// import { FetalMovementPage } from '../pages/content/fetal-movement/fetal-movement'
// import { GadgetPage } from '../pages/content/gadget/gadget';
// import { RecipesPage } from '../pages/content/recipes/recipes';
// import { KnowledgeBasePage } from '../pages/content/knowledge-base/knowledge-base';
// import { RegisterInfoPage } from '../pages/register-info/register-info';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = LoginPage;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
}
