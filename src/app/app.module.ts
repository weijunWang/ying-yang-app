import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { MultiPickerModule } from 'ion-multi-picker';

import { CalendarModule } from "ion2-calendar";
// 图片查看
import { IonicImageViewerModule } from 'ionic-img-viewer';
import { Camera } from '@ionic-native/camera';
import { ImagePicker } from '@ionic-native/image-picker';
// pages
// 首页
import { HomePage } from '../pages/tabs/home/home';
// 社交
import { ConcatPage } from '../pages/tabs/concat/concat';
// 记录
import { RecordPage } from '../pages/tabs/record/record';
// 我的
import { MinePage } from '../pages/tabs/mine/mine';
// tabs切换
import { TabsPage } from '../pages/tabs/tabs';
// 选择人生阶段
import { RegisterPage } from '../pages/register/register';
// 注册
import { RegisterInfoPage } from '../pages/register-info/register-info';
// 忘记密码
import { ForgetPwdPage } from '../pages/forget-pwd/forget-pwd';
// 登录
import { LoginPage } from '../pages/login/login';

import { IndexPage } from '../pages/index/index';
// 有孩子
import { HasChildPage } from '../pages/has-child/has-child';
// 备孕
import { GestationPage } from '../pages/gestation/gestation';
// 知识库首页
import { KnowledgeBasePage } from '../pages/content/knowledge-base/knowledge-base';
import { HasGestationPage } from '../pages/has-gestation/has-gestation';
import { PhysicalDataPage } from '../pages/physical-data/physical-data';
import { EarlyPregnancyPage } from '../pages/content/early-pregnancy/early-pregnancy';
import { KnowledgeDetailPage } from '../pages/content/knowledge-detail/knowledge-detail';
import { RecipesPage } from '../pages/content/recipes/recipes';
import { FaqPage } from '../pages/content/faq/faq';
import { GadgetPage } from '../pages/content/gadget/gadget';
import { FetalMovementPage } from '../pages/content/fetal-movement/fetal-movement';
import { PrenatalEducationPage } from '../pages/content/prenatal-education/prenatal-education';
import { FetalMovementRecordPage } from '../pages/content/fetal-movement-record/fetal-movement-record';
import { RecipesOnedayPage } from '../pages/content/recipes-oneday/recipes-oneday';
// 问卷
import { QuestionnairePage } from '../pages/content/questionnaire/questionnaire';
import { QuestionnaireGdmPage } from '../pages/content/questionnaire-gdm/questionnaire-gdm';
import { QuestionnaireFeeddingTypePage } from '../pages/content/questionnaire-feedding-type/questionnaire-feedding-type';
//膳食列表页
import { DietPage } from '../pages/content/diet/diet'
//  记膳食
import { RecordDietPage } from '../pages/content/record-diet/record-diet';
// 问与答详情
import { FaqDetailPage } from '../pages/content/faq-detail/faq-detail';
// 孩子出生填写孩子信息
import { BirthChildInfoPage } from '../pages/content/birth-child-info/birth-child-info';
// 修改孩子升高和体重
import { ChangeBabyHeightPage } from '../pages/content/change-baby-height/change-baby-height';
// 称体重画曲线页面
import { WeightCurvePage } from '../pages/content/weight-curve/weight-curve';
// 称孩子体重身高的页面
import { WeightCurveChildPage } from '../pages/content/weight-curve-child/weight-curve-child';
// 知识库列表
import { KnowledgesPage } from '../pages/content/knowledges/knowledges';
// 选择喂养方式
import { FeedingPage } from '../pages/content/feeding/feeding';
// 我的积分
import { ExchangePointsPage } from '../pages/content/exchange-points/exchange-points';
// 消息
import { NewsPage } from '../pages/content/news/news';
// 消息通知
import { NotificationPage } from '../pages/content/notification/notification';
// 联系我们
import { ContactUsPage } from '../pages/content/contact-us/contact-us';
// 设置
import { SettingPage } from '../pages/content/setting/setting';
// 社交
import { ContactSearchPage } from '../pages/content/contact-search/contact-search';
// 分享成功故事
import { SuccessStoryPage } from '../pages/content/success-story/success-story';
// 故事详情
import { SuccessStoryDetailPage } from '../pages/content/success-story-detail/success-story-detail';
// 所有帖子
import { PostsPage } from '../pages/content/posts/posts';
// 帖子详情
import { PostDetailPage } from '../pages/content/post-detail/post-detail';
// 发帖子
import { PostingPage } from '../pages/content/posting/posting';
// 活动详情
import { ActivityDetailPage } from '../pages/content/activity-detail/activity-detail';
// 添加食物
import { AddFoodPage } from '../pages/content/add-food/add-food';
// 记事迹
import { RecordDeedPage } from '../pages/content/record-deed/record-deed';
// 事迹
import { DeedsPage } from '../pages/content/deeds/deeds';
// 事迹详情
import { DeedDetailPage } from '../pages/content/deed-detail/deed-detail';
// 运动
import { MotionPage } from '../pages/content/motion/motion';
// 记运动
import { RecordMovementPage } from '../pages/content/record-movement/record-movement';
// 一天的运动
import { OnedayMovementPage } from '../pages/content/oneday-movement/oneday-movement';
// 添加运动
import { AddMovementPage } from '../pages/content/add-movement/add-movement';
//记体重
import { RecordWeightPage } from '../pages/content/record-weight/record-weight';
// 体重记录
import { WeightHistoricalPage } from '../pages/content/weight-historical/weight-historical';
// 体重身高记录
import { WeightaheightHistoricalPage } from '../pages/content/weightaheight-historical/weightaheight-historical';
// 修改身高和体重
import { EditHawPage } from '../pages/content/edit-haw/edit-haw';
// 产检时间
import { BirthsSeizedTimePage } from '../pages/content/births-seized-time/births-seized-time';
// 产检时间详情
import { BirthsSeizedItemPage } from '../pages/content/births-seized-item/births-seized-item'; 
// 设置提醒
import { SetRemindPage } from '../pages/content/set-remind/set-remind';
// 宝宝接种计划
import { BabyVaccinationPlanPage } from '../pages/content/baby-vaccination-plan/baby-vaccination-plan';
// 食物GI值
import { FoodGiPage } from '../pages/content/food-gi/food-gi';
// 搜索食物
import { SearchFoodPage } from '../pages/content/search-food/search-food';
// 食物类别详细分类
import { FoodListPage } from '../pages/content/food-list/food-list';
// 免责声明
import { PrivacyPolicyPage } from '../pages/content/privacy-policy/privacy-policy';


/* pipes */
import { PipesModule } from '../pipes/pipes.module';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

/*providers*/
import { DataServersProvider } from '../providers/data-servers/data-servers';
import { CordovaServersProvider } from '../providers/cordova-servers/cordova-servers';
import { HttpServerProvider } from '../providers/http-server/http-server';
import { StorageServeProvider } from '../providers/storage-serve/storage-serve';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabsPage,
    ConcatPage,
    RecordPage,
    RegisterPage,
    LoginPage,
    IndexPage,
    HasChildPage,
    GestationPage,
    HasGestationPage,
    RegisterInfoPage,
    PhysicalDataPage,
    ForgetPwdPage,
    MinePage,
    KnowledgeBasePage,
    EarlyPregnancyPage,
    KnowledgeDetailPage,
    RecipesPage,
    FaqPage,
    GadgetPage,
    FetalMovementPage,
    PrenatalEducationPage,
    FetalMovementRecordPage,
    RecipesOnedayPage,
    DietPage,
    RecordDietPage,
    FaqDetailPage,
    BirthChildInfoPage,
    ChangeBabyHeightPage,
    WeightCurvePage,
    KnowledgesPage,
    FeedingPage,
    ExchangePointsPage,
    NewsPage,
    ContactSearchPage,
    SuccessStoryPage,
    SuccessStoryDetailPage,
    PostsPage,
    PostingPage,
    PostDetailPage,
    ActivityDetailPage,
    AddFoodPage,
    RecordDeedPage,
    DeedsPage,
    MotionPage,
    RecordMovementPage,
    DeedDetailPage,
    RecordWeightPage,
    WeightHistoricalPage,
    WeightaheightHistoricalPage,
    EditHawPage,
    OnedayMovementPage,
    BirthsSeizedTimePage,
    BirthsSeizedItemPage,
    SetRemindPage,
    BabyVaccinationPlanPage,
    FoodGiPage,
    SearchFoodPage,
    FoodListPage,
    NotificationPage,
    ContactUsPage,
    SettingPage,
    WeightCurveChildPage,
    QuestionnairePage,
    PrivacyPolicyPage,
    AddMovementPage,
    QuestionnaireGdmPage,
    QuestionnaireFeeddingTypePage,
  ],
  imports: [
    IonicImageViewerModule,
    CalendarModule, //日历插件
    BrowserModule,
    HttpClientModule,
    PipesModule,
    IonicModule.forRoot(MyApp, {
      iconMode: 'ios',
      mode: 'ios',
      tabsHideOnSubPages: 'true',        //隐藏全部子页面tabs
      backButtonText: ""  /*修改返回按钮为返回（默认是）*/
    }),
    MultiPickerModule, //自定义滑动选择的插件
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    HomePage,
    TabsPage,
    ConcatPage,
    RecordPage,
    IndexPage,
    RegisterPage,
    HasChildPage,
    HasGestationPage,
    GestationPage,
    RegisterInfoPage,
    PhysicalDataPage,
    ForgetPwdPage,
    MinePage,
    KnowledgeBasePage,
    EarlyPregnancyPage,
    KnowledgeDetailPage,
    RecipesPage,
    FaqPage,
    GadgetPage,
    FetalMovementPage,
    PrenatalEducationPage,
    FetalMovementRecordPage,
    RecipesOnedayPage,
    DietPage,
    RecordDietPage,
    FaqDetailPage,
    BirthChildInfoPage,
    ChangeBabyHeightPage,
    WeightCurvePage,
    KnowledgesPage,
    FeedingPage,
    ExchangePointsPage,
    NewsPage,
    ContactSearchPage,
    SuccessStoryPage,
    SuccessStoryDetailPage,
    PostsPage,
    PostingPage,
    PostDetailPage,
    ActivityDetailPage,
    AddFoodPage,
    RecordDeedPage,
    DeedsPage,
    MotionPage,
    RecordMovementPage,
    DeedDetailPage,
    RecordWeightPage,
    WeightHistoricalPage,
    WeightaheightHistoricalPage,
    EditHawPage,
    OnedayMovementPage,
    BirthsSeizedTimePage,
    BirthsSeizedItemPage,
    SetRemindPage,
    BabyVaccinationPlanPage,
    FoodGiPage,
    SearchFoodPage,
    FoodListPage,
    NotificationPage,
    ContactUsPage,
    SettingPage,
    WeightCurveChildPage,
    QuestionnairePage,
    PrivacyPolicyPage,
    AddMovementPage,
    QuestionnaireGdmPage,
    QuestionnaireFeeddingTypePage,
  ],
  providers: [
    ImagePicker,
    Camera,
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    DataServersProvider,
    CordovaServersProvider,
    HttpServerProvider,
    StorageServeProvider
  ]
})
export class AppModule {}
