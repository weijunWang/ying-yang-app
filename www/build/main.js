webpackJsonp([36],{

/***/ 105:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KnowledgeDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var KnowledgeDetailPage = /** @class */ (function () {
    function KnowledgeDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.allData = null;
        this.keyword = "";
    }
    KnowledgeDetailPage.prototype.ngOnInit = function () {
        this.allData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？"
            }, {
                id: 2,
                text: "孕期运动"
            }, {
                id: 3,
                text: "如何进行宝宝胎教，做哪些事情比较好"
            }, {
                id: 4,
                text: "妊娠期"
            }
        ];
        console.log('ionViewDidLoad KnowledgeDetailPage');
    };
    KnowledgeDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-knowledge-detail',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/knowledge-detail/knowledge-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>知识详情</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="header-img">\n            <img src="../../../assets/imgs/article-bg-img.png" alt="">\n        </div>\n        <div class="article-header">\n            <div class="article-title">\n                <div>孕早期有哪些注意事项呢？</div>\n                <div>\n                    <ion-icon name="star" style=\'color: yellow\'></ion-icon>\n                    <ion-icon ios="md-share" md="md-share" color="secondary"></ion-icon>\n                </div>\n            </div>\n            <div class=\'article-time\'>\n                    2018.03.02\n            </div>\n            <div class="article-auther-header"> \n                <div class="header-img">\n                    <img src="../../../assets/imgs/article-header-img.png" alt="">\n                </div>\n                <div class="auther-detail">\n                    <div>复旦营养博士</div>\n                    <div>徐春来</div>\n                </div>\n            </div>\n            <div class="article-auther-remark">\n                <div>1212</div>\n                <div>1212</div>\n            </div>\n        </div>\n        <div class="article-content">\n            1212121\n        </div>\n        <div class="article-talk">\n            <div class="article-talk-title">留言板</div>\n            <ul>\n                <li>\n                    <div class="header-img">\n                        <img src="../../../assets/imgs/article-header-img.png" alt="">\n                    </div>\n                    <div class="talk-title">\n                        <div>待产孕妈妈1</div>\n                        <div>孕20周5天</div>\n                        <div>孕育桥、孕迹暖暖都在用哦!</div>\n                        <div class="talk-reply">\n                            <div>作者回复</div>\n                            <div>\n                                1212\n                            </div>\n                        </div>\n                    </div>\n                    <div class="talk-content">\n                        <div>15分钟前</div>\n                        <div>\n                            <ion-icon name="heart" color="danger"></ion-icon>\n                            <span>223</span>\n                        </div>\n                    </div>\n                </li>\n            </ul>\n        </div>\n        <div class="article-footer">\n            <input type="text" placeholder="请输入关键字" [(ngModel)]="keyword">\n            <span class="search">发布</span>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/knowledge-detail/knowledge-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], KnowledgeDetailPage);
    return KnowledgeDetailPage;
}());

//# sourceMappingURL=knowledge-detail.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ForgetPwdPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ForgetPwdPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ForgetPwdPage = /** @class */ (function () {
    function ForgetPwdPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.codeNum = '';
        this.showPwd = 1;
        this.codeString = "获取验证码";
        this.password = '';
    }
    ForgetPwdPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ForgetPwdPage');
    };
    ForgetPwdPage.prototype.getCode = function () {
        var _this = this;
        console.log("获取验证码");
        this.codeString = "重新获取";
        this.codeNum = 60;
        var timer = setInterval(function () {
            if (_this.codeNum <= 0) {
                _this.codeNum = 0;
                clearInterval(timer);
            }
            else {
                _this.codeNum--;
            }
            console.log(_this.codeNum);
        }, 1000);
    };
    ForgetPwdPage.prototype.toLogin = function () {
        console.log("toLogin");
        this.navCtrl.pop();
    };
    ForgetPwdPage.prototype.nextStep = function () {
        console.log("下一步");
        if (this.showPwd == 1) {
            this.showPwd = 2;
        }
        else if (this.showPwd == 2) {
            this.showPwd = 3;
        }
    };
    ForgetPwdPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-forget-pwd',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/forget-pwd/forget-pwd.html"*/'<!--\n  Generated template for the ForgetPwdPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>忘记密码</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="flex-page login-page">\n    <div class="login-page-box">\n      <img src=\'../../assets/imgs/logo.png\' />\n      <span>app名字</span>\n    </div>\n    <div *ngIf="showPwd == 1" class="ion-input-box margin-top-40">\n      请输入手机号\n      <input type="text" placeholder="请输入手机号">\n    </div>\n    <div *ngIf="showPwd == 2" class="ion-input-box pos-relative margin-top-40">\n      请输入验证码\n      <input type="text" placeholder="请输入验证码">\n      <span class="pos-code" (click)="getCode()"><span *ngIf=\'codeNum > 0 && codeNum <= 60\'> ({{codeNum}}S) </span>{{codeString}}</span>\n    </div>\n    <div *ngIf="showPwd == 3" class="ion-input-box">\n      请输入密码\n      <input type="text" placeholder="密码" [ngModel]=\'password\' >\n    </div>\n    <div class="ion-input-box">\n      <button ion-button (click)="nextStep()">下一步</button>\n    </div>\n    <div class="page-footer-register">\n      <div (click)="toLogin()">用户登录</div>\n      <div>|</div>\n      <div>忘记密码</div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/forget-pwd/forget-pwd.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ForgetPwdPage);
    return ForgetPwdPage;
}());

//# sourceMappingURL=forget-pwd.js.map

/***/ }),

/***/ 192:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__home_home__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__concat_concat__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__mine_mine__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__record_record__ = __webpack_require__(383);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_1__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_2__concat_concat__["a" /* ConcatPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_4__record_record__["a" /* RecordPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_3__mine_mine__["a" /* MinePage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="首页" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="社交" tabIcon="contacts"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="记录" tabIcon="ios-pricetag"></ion-tab>\n  <ion-tab [root]="tab4Root" tabTitle="我的" tabIcon="information-circle"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 215:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ActivityDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ActivityDetailPage = /** @class */ (function () {
    function ActivityDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.allData = null;
        this.keyword = "";
        this.peopleNum = 0;
    }
    ActivityDetailPage.prototype.ngOnInit = function () {
        this.peopleNum = 7;
        this.allData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？"
            }, {
                id: 2,
                text: "孕期运动"
            }, {
                id: 3,
                text: "如何进行宝宝胎教，做哪些事情比较好"
            }, {
                id: 4,
                text: "妊娠期"
            }
        ];
        if (this.peopleNum == 1) {
            this.headerImgList = [
                {
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        else if (this.peopleNum == 2) {
            this.headerImgList = [
                {
                    width: 5,
                    left: 18.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 13.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        else if (this.peopleNum == 3) {
            this.headerImgList = [
                {
                    width: 4.6,
                    left: 13.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 18.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        else if (this.peopleNum == 4) {
            this.headerImgList = [
                {
                    width: 5,
                    left: 18.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 13.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 20.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 11.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        else if (this.peopleNum == 5) {
            this.headerImgList = [
                {
                    width: 4.2,
                    left: 11.625,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 13.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 18.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.2,
                    left: 20.125,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        else if (this.peopleNum == 6) {
            this.headerImgList = [
                {
                    width: 4.4,
                    left: 9.125,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 11.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 13.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 18.125,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 20.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.2,
                    left: 22.625,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        else if (this.peopleNum == 7) {
            this.headerImgList = [
                {
                    width: 3.8,
                    left: 9.125,
                    top: 0.6,
                    zIndex: 2,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.2,
                    left: 11.625,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 13.625,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 5,
                    left: 15.625,
                    top: 0,
                    zIndex: 5,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.6,
                    left: 18.125,
                    top: 0.15,
                    zIndex: 4,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 4.2,
                    left: 20.125,
                    top: 0.4,
                    zIndex: 3,
                    img: '../../../assets/imgs/demo.png'
                }, {
                    width: 3.8,
                    left: 22.125,
                    top: 0.6,
                    zIndex: 2,
                    img: '../../../assets/imgs/demo.png'
                }
            ];
        }
        console.log('ionViewDidLoad KnowledgeDetailPage');
    };
    ActivityDetailPage.prototype.duihuan = function () {
        console.log('兑换');
    };
    ActivityDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-activity-detail',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/activity-detail/activity-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>活动详情</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="header-img">\n            <img src="../../../assets/imgs/article-bg-img.png" alt="">\n        </div>\n        <div padding>\n            <div class="article-header">\n                <div class="article-title">\n                    <div>孕早期有哪些注意事项呢？</div>\n                    <div>\n                        <ion-icon name="star" style=\'color: yellow\'></ion-icon>\n                        <ion-icon ios="md-share" md="md-share" color="secondary"></ion-icon>\n                    </div>\n                </div>\n                <div class=\'article-time\'>\n                        2018.03.02\n                </div>\n                <div class="article-auther-header"> \n                    <div class="header-img">\n                        <img src="../../../assets/imgs/activety-logo.png" alt="">\n                    </div>\n                    <div class="auther-detail">\n                        <div>复旦孕产官方小助手</div>\n                        <div>积分兑换活动</div>\n                    </div>\n                </div>\n            </div>\n        </div>\n        \n        <div class="article-content" padding>\n            <div class="article-talk-title">咨询互动内容</div>\n            <div class="article-content-text">\n                12131232123\n            </div>\n            <div class="article-talk-title">活动详情</div>\n            <div class="detail-address">\n                <div>\n                    地址：\n                </div>\n                <div>\n                    电话：\n                </div>\n                <div>\n                    联系人：\n                </div>\n            </div>\n        </div>\n        <div class="activity-footer">\n            <div class="footer-first">\n                已兑换用户\n            </div>\n            <ul class="footer-header">\n                <li *ngFor=\'let item of headerImgList\' [ngStyle]="{\'width\': item.width + \'rem\', \'height\': item.width + \'rem\', \'left\': item.left + \'rem\', \'top\': item.top + \'rem\', \'z-index\': item.zIndex}">\n                    <img src="{{item.img}}" alt="">\n                </li>\n            </ul>\n            <div class="footer-people-num">\n                已经有{{peopleNum}}位用户兑换成功\n            </div>\n            <div class="footer-button">\n                <button ion-button (click)=\'duihuan()\'>去兑换</button>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/activity-detail/activity-detail.html"*/,
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]) === "function" && _b || Object])
    ], ActivityDetailPage);
    return ActivityDetailPage;
    var _a, _b;
}());

//# sourceMappingURL=activity-detail.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BirthChildInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BirthChildInfoPage = /** @class */ (function () {
    function BirthChildInfoPage(navCtrl, navParams, actionSheetCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.actionSheetCtrl = actionSheetCtrl;
        this.isBoy = false;
        this.isGirl = false;
        this.babyHeights = [
            {
                name: 'babyHeight',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' },
                    { text: '4', value: '3' },
                    { text: '5', value: '3' },
                    { text: '6', value: '3' },
                    { text: '7', value: '3' },
                    { text: '8', value: '3' },
                    { text: '9', value: '3' },
                    { text: '10', value: '3' }
                ]
            }
        ];
        this.babyWeights = [
            {
                name: 'babyWeight',
                options: [
                    { text: '1', value: '1' },
                    { text: '2', value: '2' },
                    { text: '3', value: '3' },
                    { text: '4', value: '3' },
                    { text: '5', value: '3' },
                    { text: '6', value: '3' },
                    { text: '7', value: '3' },
                    { text: '8', value: '3' },
                    { text: '9', value: '3' },
                    { text: '10', value: '3' }
                ]
            }
        ];
        this.types = [
            {
                name: 'type',
                options: [
                    { text: '母乳喂养', value: '1' },
                    { text: '辅食喂养', value: '2' },
                    { text: '混合喂养', value: '3' }
                ]
            }
        ];
        this.birthTypes = [
            {
                name: 'birthType',
                options: [
                    { text: '顺产', value: '1' },
                    { text: '剖宫产', value: '2' }
                ]
            }
        ];
    }
    BirthChildInfoPage.prototype.checkBoy = function () {
        this.isGirl = false;
        this.isBoy = true;
    };
    BirthChildInfoPage.prototype.checkGirl = function () {
        this.isGirl = true;
        this.isBoy = false;
    };
    BirthChildInfoPage.prototype.confirm = function () {
        var data = {
            babyHeight: this.babyHeight,
            babyWeight: this.babyWeight,
            type: this.type,
            birthType: this.birthType
        };
        console.log(data);
    };
    BirthChildInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-birth-child-info',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/birth-child-info/birth-child-info.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>birth</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <ion-list class=\'font-color3\'>\n            <ion-label class="font-color1">宝宝出生了！</ion-label>\n            <ion-label stacked>宝宝性别</ion-label>\n            <div class="sex-choice-box">\n                <div class="pos-relative" (click)=\'checkBoy()\'>\n                    <img class=\'boy-header\' src="../../../assets/imgs/boy-header.png" alt="">\n                    <img class=\'pos-icon\' [ngStyle]="{\'display\': isBoy ? \'inline-block\' : \'none\'}" src="../../../assets/imgs/change-right.png" alt="">\n                </div>\n                <div class="pos-relative" (click)=\'checkGirl()\'>\n                    <img class=\'boy-header\' src="../../../assets/imgs/girl-header.png" alt="">\n                    <img class=\'pos-icon\' [ngStyle]="{\'display\': isGirl ? \'inline-block\' : \'none\'}" src="../../../assets/imgs/change-right.png" alt="">\n                </div>\n            </div>\n            <ion-item>\n                <ion-label stacked class=\'font-color3\'>宝宝乳名</ion-label>\n                <ion-input class=\'text-input font-color3\' type="text" placeholder="宝宝乳名"></ion-input>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked class=\'font-color3\'>宝宝生日</ion-label>\n                <ion-datetime class=\'text-input ion-datetime font-color3\' displayFormat="YYYY-MM-DD" cancelText="取消" doneText="确定" placeholder="2018-10-01"\n                    [(ngModel)]="myDate"></ion-datetime>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked class=\'font-color3\'>宝宝身长</ion-label>\n                <ion-multi-picker class=\'text-input ion-datetime font-color3\' [(ngModel)]="babyHeight" placeholder="宝宝身长" item-content [multiPickerColumns]="babyHeights" cancelText="取消" doneText="确定"></ion-multi-picker>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked class=\'font-color3\'>宝宝体重</ion-label>\n                <ion-multi-picker class=\'text-input ion-datetime font-color3\' [(ngModel)]="babyWeight" placeholder="宝宝体重" item-content [multiPickerColumns]="babyWeights" cancelText="取消" doneText="确定"></ion-multi-picker>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked class=\'font-color3\'>喂养方式</ion-label>\n                <ion-multi-picker class=\'text-input ion-datetime font-color3\' [(ngModel)]="type" placeholder="喂养方式" item-content [multiPickerColumns]="types" cancelText="取消" doneText="确定"></ion-multi-picker>\n            </ion-item>\n\n            <ion-item>\n                <ion-label stacked class=\'font-color3\'>分娩方式</ion-label>\n                <ion-multi-picker class=\'text-input ion-datetime font-color3\' [(ngModel)]="birthType" placeholder="分娩方式" item-content [multiPickerColumns]="birthTypes" cancelText="取消" doneText="确定"></ion-multi-picker>\n            </ion-item>\n        </ion-list>\n        <div class="confirm-center-btn">\n            <button class="confirm-btn" ion-button round (click)=\'confirm()\'>完成</button>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/birth-child-info/birth-child-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["ActionSheetController"]])
    ], BirthChildInfoPage);
    return BirthChildInfoPage;
}());

//# sourceMappingURL=birth-child-info.js.map

/***/ }),

/***/ 217:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactSearchPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ContactSearchPage = /** @class */ (function () {
    function ContactSearchPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.keyword = "";
        this.searching = false;
    }
    ;
    ;
    ContactSearchPage.prototype.ngOnInit = function () {
        this.storgeData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？",
            }, {
                id: 2,
                text: "孕期运动",
            }, {
                id: 3,
                text: "孕早期",
            }, {
                id: 4,
                text: "妊娠期",
            }
        ];
        this.allData = [
            {
                id: 1,
                title: "孕早期禁食有哪些？",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }, {
                id: 2,
                title: "孕期运动",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }, {
                id: 3,
                title: "如何进行宝宝胎教，做哪些事情比较好",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }, {
                id: 4,
                title: "妊娠期",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }
        ];
        console.log('ionViewDidLoad KnowledgeBasePage');
    };
    ContactSearchPage.prototype.toPage = function (id) {
    };
    ContactSearchPage.prototype.inputFocus = function () {
        if (this.keyword != '') {
            this.searching = true;
        }
    };
    ContactSearchPage.prototype.clearAll = function () {
        this.storgeData = [];
    };
    ContactSearchPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-contact-search',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/contact-search/contact-search.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>知识库</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="top-bar">\n            <input type="text" placeholder="请输入关键字" (focus)=\'inputFocus()\' [(ngModel)]="keyword">\n            <ion-icon class="search" name="search"></ion-icon>\n        </div>\n        <div class=\'search-text-box\' *ngIf=\'!searching\'>\n            <div class="box-title">\n                <span>搜索记录：</span>\n                <span class="for-list" (click)=\'clearAll()\'>清空</span>\n            </div>\n            <div class="flex-box">\n                <div *ngFor="let i of storgeData">\n                    <div class="for-list">\n                        {{i.text}}\n                    </div>\n                </div>\n            </div>\n            <div class="box-title">大家都在搜：</div>\n            <div class="flex-box">\n                <div *ngFor="let i of storgeData">\n                    <div class="for-list">\n                        {{i.text}}\n                    </div>\n                </div>\n            </div>\n        </div>\n        <ul class="flex-ul" *ngIf=\'searching\'>\n            <li *ngFor="let i of allData" (click)=\'toPage(i.id)\'>\n                <div class="article-title">\n                    <span [innerHTML]=\'i.title | wordPlace:keyword\'></span>\n                </div>\n                <div class="article-content">\n                    {{i.content}}\n                </div>\n                <div class="article-remark">\n                    <span>{{i.type}} </span>\n                    <span>{{i.stage}}</span>\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/contact-search/contact-search.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ContactSearchPage);
    return ContactSearchPage;
}());

//# sourceMappingURL=contact-search.js.map

/***/ }),

/***/ 218:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChangeBabyHeightPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ChangeBabyHeightPage = /** @class */ (function () {
    function ChangeBabyHeightPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ChangeBabyHeightPage.prototype.ngOnInit = function () {
        this.forList = [
            {
                title: '家里',
                flag: false
            }, {
                title: '餐馆',
                flag: true
            }, {
                title: '路边摊',
                flag: false
            }
        ];
    };
    ChangeBabyHeightPage.prototype.save = function () {
        console.log('保存');
    };
    ChangeBabyHeightPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-change-baby-height',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/change-baby-height/change-baby-height.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>记膳食</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="content-box">\n            <div class="top-time">\n                <div class="box">\n                    <span>宝宝体重（Kg)</span>\n                </div>\n                <div class="box">\n                    <span>\n                        45\n                        <!-- <ion-datetime displayFormat="YYYY年MM月DD日" cancelText="取消" doneText="确定" placeholder="2018年12月01日"></ion-datetime> -->\n                    </span>\n                    <img src="../../../assets/imgs/right-icon.png" alt="">\n                </div>\n            </div>\n\n            <div class="top-time">\n                <div class="box">\n                    <span>宝宝身高（Cm)</span>\n                </div>\n                <div class="box">\n                    <span>12\n                        <!-- <ion-item>\n                            <ion-datetime displayFormat="YYYY年MM月DD日" cancelText="取消" doneText="确定" placeholder="2018年12月01日"></ion-datetime>\n                        </ion-item> -->\n                    </span>\n                    <img src="../../../assets/imgs/right-icon.png" alt="">\n                </div>\n            </div>\n\n            <div class="btn-box">\n                <button ion-button (click)=\'save()\'>保存</button>\n            </div>\n            <div class="footer-box">\n                <span>宝宝生长曲线</span>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/change-baby-height/change-baby-height.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ChangeBabyHeightPage);
    return ChangeBabyHeightPage;
}());

//# sourceMappingURL=change-baby-height.js.map

/***/ }),

/***/ 219:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EarlyPregnancyPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__knowledge_detail_knowledge_detail__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var EarlyPregnancyPage = /** @class */ (function () {
    function EarlyPregnancyPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.allData = null;
        this.keyword = "";
    }
    EarlyPregnancyPage.prototype.ngOnInit = function () {
        this.allData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？"
            }, {
                id: 2,
                text: "孕期运动"
            }, {
                id: 3,
                text: "如何进行宝宝胎教，做哪些事情比较好"
            }, {
                id: 4,
                text: "妊娠期"
            }
        ];
        this.listData = [
            {
                img: '../../../assets/imgs/article-img.png',
                title: '孕早期有哪些注意事项呢？'
            }, {
                img: '../../../assets/imgs/article-img2.png',
                title: '孕早期有哪些注意事项呢？'
            }, {
                img: '../../../assets/imgs/article-img3.png',
                title: '孕早期有哪些注意事项呢？'
            }, {
                img: '../../../assets/imgs/article-img.png',
                title: '孕早期有哪些注意事项呢？'
            }
        ];
        console.log('EarlyPregnancyPage');
    };
    EarlyPregnancyPage.prototype.toDetailPage = function (item) {
        console.log(item);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__knowledge_detail_knowledge_detail__["a" /* KnowledgeDetailPage */]);
    };
    EarlyPregnancyPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-early-pregnancy',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/early-pregnancy/early-pregnancy.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>孕早期</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <ul class=\'list-article\'>\n            <li *ngFor=\'let item of listData\' (click)=\'toDetailPage(item)\'>\n                <!-- {{item.title}} -->\n                <div>{{item.title}}</div>\n                <div>\n                    <img src="{{item.img}}" alt="">\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/early-pregnancy/early-pregnancy.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], EarlyPregnancyPage);
    return EarlyPregnancyPage;
}());

//# sourceMappingURL=early-pregnancy.js.map

/***/ }),

/***/ 220:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DietPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__record_diet_record_diet__ = __webpack_require__(221);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var DietPage = /** @class */ (function () {
    function DietPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    DietPage.prototype.ngOnInit = function () {
        // this.getNode();
        this.img_data1 = [];
        this.img_data2 = [];
        this.img_data = [
            {
                id: 1,
                canEdit: true,
                title: '每日膳食',
                dateTime: '2018年10月20日',
                bgImg: '../../../assets/imgs/diet-img.png',
                accumulatePoints: '10',
            }, {
                id: 2,
                canEdit: false,
                title: '每日膳食',
                dateTime: '2018年10月20日',
                bgImg: '../../../assets/imgs/diet-img.png',
                accumulatePoints: '10',
            }, {
                id: 3,
                canEdit: true,
                title: '每日膳食',
                dateTime: '2018年10月20日',
                bgImg: '../../../assets/imgs/diet-img.png',
                accumulatePoints: '10',
            }, {
                id: 4,
                canEdit: false,
                title: '每日膳食',
                dateTime: '2018年10月20日',
                bgImg: '../../../assets/imgs/diet-img.png',
                accumulatePoints: '10',
            }, {
                id: 5,
                canEdit: false,
                title: '每日膳食',
                dateTime: '2018年10月20日',
                bgImg: '../../../assets/imgs/diet-img.png',
                accumulatePoints: '10',
            }, {
                id: 6,
                canEdit: true,
                title: '每日膳食',
                dateTime: '2018年10月20日',
                bgImg: '../../../assets/imgs/diet-img.png',
                accumulatePoints: '10',
            }
        ];
        for (var i = 0; i < this.img_data.length; i++) {
            if (i % 2 == 0) {
                this.img_data1.push(this.img_data[i]);
            }
            else {
                this.img_data2.push(this.img_data[i]);
            }
        }
        var box_img = document.getElementsByClassName('box_img');
        console.log(box_img);
    };
    DietPage.prototype.toPage = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__record_diet_record_diet__["a" /* RecordDietPage */]);
    };
    DietPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-diet',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/diet/diet.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>膳食</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class=\'normal-page\'>\n        <div class="masonry">\n            <div class="column">\n                <div class="item" *ngFor=\'let item of img_data1, let i = index\' [ngStyle]="{\'height\': item.canEdit? \'18rem\' : \'20rem\'}" (click)=\'toPage(item.id)\'>\n                    <div *ngIf=\'!item.canEdit\' class="pos-bg">\n                        <img src="{{item.bgImg}}" alt="">\n                    </div>\n                    <div *ngIf=\'!item.canEdit\' class="pos-points">\n                        {{item.accumulatePoints}}积分\n                    </div>\n                    <div>\n                        <img *ngIf=\'item.canEdit && i > 0\' src="../../../assets/imgs/write-icon.png" alt="">\n                        <img *ngIf=\'i==0\' src="../../../assets/imgs/diet-add.png" alt="">\n                    </div>\n                    <div [ngClass]="{\'fffcolor\':!item.canEdit?true:false}">\n                        {{item.title}}\n                    </div>\n                    <div *ngIf=\'i > 0\' [ngClass]="{\'fffcolor\': !item.canEdit?true:false}">\n                        {{item.dateTime}}\n                    </div>\n                </div>\n            </div>\n            <div class="column">\n                <div class="item" *ngFor=\'let item of img_data2, let i = index\' [ngStyle]="{\'height\': item.canEdit? \'18rem\' : \'20rem\'}" (click)=\'toPage(item.id)\'>\n                    <div *ngIf=\'!item.canEdit\' class="pos-bg">\n                        <img src="{{item.bgImg}}" alt="">\n                    </div>\n                    <div *ngIf=\'!item.canEdit\' class="pos-points">\n                        {{item.accumulatePoints}}积分\n                    </div>\n                    <div>\n                        <img *ngIf=\'item.canEdit\' src="../../../assets/imgs/write-icon.png" alt="">\n                    </div>\n                    <div [ngClass]="{\'fffcolor\': !item.canEdit?true:false}">\n                        {{item.title}}\n                    </div>\n                    <div [ngClass]="{\'fffcolor\': !item.canEdit?true:false}">\n                        {{item.dateTime}}\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/diet/diet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], DietPage);
    return DietPage;
}());

//# sourceMappingURL=diet.js.map

/***/ }),

/***/ 221:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecordDietPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecordDietPage = /** @class */ (function () {
    function RecordDietPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RecordDietPage.prototype.ngOnInit = function () {
        this.forList = [
            {
                title: '家里',
                flag: false
            }, {
                title: '餐馆',
                flag: true
            }, {
                title: '路边摊',
                flag: false
            }
        ];
    };
    RecordDietPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-record-diet',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/record-diet/record-diet.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>记膳食</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="content-box">\n            <div class="top-time">\n                <div class="box">\n                    <img src="../../../assets/imgs/rili.png" alt="">\n                    <span>日期</span>\n                </div>\n                <div class="box">\n                    <span><ion-datetime displayFormat="YYYY年MM月DD日" cancelText="取消" doneText="确定" placeholder="2018年12月01日"></ion-datetime></span>\n                    <img src="../../../assets/imgs/right-icon.png" alt="">\n                </div>\n            </div>\n            <div class="center-box">\n                <ul>\n                    <li>\n                        <div class="one-line">\n                            <div class="text-box">\n                                <div>早餐</div>\n                                <div>就餐地点: </div>\n                            </div>\n                            <div class="img-box">\n                                <img src="../../../assets/imgs/camera.png" alt="">\n                            </div>\n                        </div>\n                        <div class="two-line">\n                            <div class="for-list" *ngFor=\'let item of forList\'>\n                                <img *ngIf=\'item.flag\' src="../../../assets/imgs/diet-circle-choice.png" alt="">\n                                <img *ngIf=\'!item.flag\' src="../../../assets/imgs/diet-circle.png" alt="">\n                                <span>{{item.title}}</span>\n                            </div>\n                        </div>\n                        <div class="three-line">\n                            <div>添加食物: </div>\n                            <div>\n                                <img src="../../../assets/imgs/add-icon.png" alt="">\n                            </div>\n                        </div>\n                        <div class="four-line">\n                            <div class="food-box">\n                                <span>馒头·1个</span>\n                                <img src="../../../assets/imgs/close-icon.png" alt="">\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <div class="one-line">\n                            <div class="text-box">\n                                <div>早餐</div>\n                                <div>就餐地点: </div>\n                            </div>\n                            <div class="img-box">\n                                <img src="../../../assets/imgs/camera.png" alt="">\n                            </div>\n                        </div>\n                        <div class="two-line">\n                            <div class="for-list" *ngFor=\'let item of forList\'>\n                                <img *ngIf=\'item.flag\' src="../../../assets/imgs/diet-circle-choice.png" alt="">\n                                <img *ngIf=\'!item.flag\' src="../../../assets/imgs/diet-circle.png" alt="">\n                                <span>{{item.title}}</span>\n                            </div>\n                        </div>\n                        <div class="three-line">\n                            <div>添加食物: </div>\n                            <div>\n                                <img src="../../../assets/imgs/add-icon.png" alt="">\n                            </div>\n                        </div>\n                        <div class="four-line">\n                            <div class="food-box">\n                                <span>馒头·1个</span>\n                                <img src="../../../assets/imgs/close-icon.png" alt="">\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <div class="one-line">\n                            <div class="text-box">\n                                <div>早餐</div>\n                                <div>就餐地点: </div>\n                            </div>\n                            <div class="img-box">\n                                <img src="../../../assets/imgs/camera.png" alt="">\n                            </div>\n                        </div>\n                        <div class="two-line">\n                            <div class="for-list" *ngFor=\'let item of forList\'>\n                                <img *ngIf=\'item.flag\' src="../../../assets/imgs/diet-circle-choice.png" alt="">\n                                <img *ngIf=\'!item.flag\' src="../../../assets/imgs/diet-circle.png" alt="">\n                                <span>{{item.title}}</span>\n                            </div>\n                        </div>\n                        <div class="three-line">\n                            <div>添加食物: </div>\n                            <div>\n                                <img src="../../../assets/imgs/add-icon.png" alt="">\n                            </div>\n                        </div>\n                        <div class="four-line">\n                            <div class="food-box">\n                                <span>馒头·1个</span>\n                                <img src="../../../assets/imgs/close-icon.png" alt="">\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <div class="one-line">\n                            <div class="text-box">\n                                <div>早餐</div>\n                                <div>就餐地点: </div>\n                            </div>\n                            <div class="img-box">\n                                <img src="../../../assets/imgs/camera.png" alt="">\n                            </div>\n                        </div>\n                        <div class="two-line">\n                            <div class="for-list" *ngFor=\'let item of forList\'>\n                                <img *ngIf=\'item.flag\' src="../../../assets/imgs/diet-circle-choice.png" alt="">\n                                <img *ngIf=\'!item.flag\' src="../../../assets/imgs/diet-circle.png" alt="">\n                                <span>{{item.title}}</span>\n                            </div>\n                        </div>\n                        <div class="three-line">\n                            <div>添加食物: </div>\n                            <div>\n                                <img src="../../../assets/imgs/add-icon.png" alt="">\n                            </div>\n                        </div>\n                        <div class="four-line">\n                            <div class="food-box">\n                                <span>馒头·1个</span>\n                                <img src="../../../assets/imgs/close-icon.png" alt="">\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <div class="one-line">\n                            <div class="text-box">\n                                <div>早餐</div>\n                                <div>就餐地点: </div>\n                            </div>\n                            <div class="img-box">\n                                <img src="../../../assets/imgs/camera.png" alt="">\n                            </div>\n                        </div>\n                        <div class="two-line">\n                            <div class="for-list" *ngFor=\'let item of forList\'>\n                                <img *ngIf=\'item.flag\' src="../../../assets/imgs/diet-circle-choice.png" alt="">\n                                <img *ngIf=\'!item.flag\' src="../../../assets/imgs/diet-circle.png" alt="">\n                                <span>{{item.title}}</span>\n                            </div>\n                        </div>\n                        <div class="three-line">\n                            <div>添加食物: </div>\n                            <div>\n                                <img src="../../../assets/imgs/add-icon.png" alt="">\n                            </div>\n                        </div>\n                        <div class="four-line">\n                            <div class="food-box">\n                                <span>馒头·1个</span>\n                                <img src="../../../assets/imgs/close-icon.png" alt="">\n                            </div>\n                        </div>\n                    </li>\n                    <li>\n                        <div class="one-line">\n                            <div class="text-box">\n                                <div>早餐</div>\n                                <div>就餐地点: </div>\n                            </div>\n                            <div class="img-box">\n                                <img src="../../../assets/imgs/camera.png" alt="">\n                            </div>\n                        </div>\n                        <div class="two-line">\n                            <div class="for-list" *ngFor=\'let item of forList\'>\n                                <img *ngIf=\'item.flag\' src="../../../assets/imgs/diet-circle-choice.png" alt="">\n                                <img *ngIf=\'!item.flag\' src="../../../assets/imgs/diet-circle.png" alt="">\n                                <span>{{item.title}}</span>\n                            </div>\n                        </div>\n                        <div class="three-line">\n                            <div>添加食物: </div>\n                            <div>\n                                <img src="../../../assets/imgs/add-icon.png" alt="">\n                            </div>\n                        </div>\n                        <div class="four-line">\n                            <div class="food-box">\n                                <span>馒头·1个</span>\n                                <img src="../../../assets/imgs/close-icon.png" alt="">\n                            </div>\n                        </div>\n                    </li>\n                </ul>\n            </div>\n        </div>\n        <div class="footer-btn">\n            <div class="button-box">\n                <button ion-button>保存</button>\n            </div>\n            <div class="button-box">\n                <button ion-button>完成</button>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/record-diet/record-diet.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], RecordDietPage);
    return RecordDietPage;
}());

//# sourceMappingURL=record-diet.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ExchangePointsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ExchangePointsPage = /** @class */ (function () {
    function ExchangePointsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ExchangePointsPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                text: '母乳喂养',
                hide: true
            }, {
                text: '辅食喂养',
                hide: true
            }, {
                text: '混合喂养',
                hide: true
            }
        ];
        console.log('ionViewDidLoad ExchangePointsPage');
    };
    ExchangePointsPage.prototype.toNextPage = function (i) {
        for (var item = 0; item < this.listData.length; item++) {
            this.listData[item].hide = true;
        }
        this.listData[i].hide = false;
    };
    ExchangePointsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-exchange-points',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/exchange-points/exchange-points.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>我的积分</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="flex-box mine-flex-page">\n        <div class=\'first-item\' padding>\n            <div class=\'left\'>\n                <div class="left-first">\n                    <img src="../../../assets/imgs/mine-icon2.png" alt="">\n                    <span class="font-color1">500积分</span>\n                </div>\n                <div class="left-second">\n                    <span>可兑换线下专家面对面咨询</span>\n                </div>\n            </div>\n            <div class=\'right\'>\n                <button ion-button>兑换</button>\n            </div>\n        </div>\n        <div class="content-box">\n            <div class="guize">兑换规则:</div>\n            <div class="list">\n                <div class="pos-line">\n                    <img src="../../../assets/imgs/faq-line.png" alt="">\n                </div>\n                <div class="top-img">\n                    <div class="circle-icon">\n                        <img src="../../../assets/imgs/circle.png" alt="">\n                        <span>记录孕期点滴，每记录一次获取10积分；</span>\n                    </div>\n                </div>\n\n                <div class="top-img">\n                    <div class="circle-icon">\n                        <img src="../../../assets/imgs/circle.png" alt="">\n                        <span>100积分可兑换一次专家线下面对面咨询，详情可查看社交圈活动；</span>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/exchange-points/exchange-points.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], ExchangePointsPage);
    return ExchangePointsPage;
}());

//# sourceMappingURL=exchange-points.js.map

/***/ }),

/***/ 223:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FaqDetailPage = /** @class */ (function () {
    function FaqDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FaqDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-faq-detail',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/faq-detail/faq-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>问与答详情</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="article-box">\n            <div class="article-title">\n                <div>孕早期有哪些注意事项呢？</div>\n                <div>\n                    <ion-icon name="star" style=\'color: yellow\'></ion-icon>\n                    <ion-icon ios="md-share" md="md-share" color="secondary"></ion-icon>\n                </div>\n            </div>\n        </div>\n        <div class="article-box article-content">\n            <div class="article-answer">\n                孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长\n                发育的需要。孕妇的进食要适当，比平时应增加25%，但孕末期要适当控制进食量，防止营养过剩，\n                因进食过多可导致消化不良。胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，\n                以满足自身的营养和胎儿生长发育的需要。孕妇的进食要适当，比平时应增加25%，但孕末期要适当控制进食量，\n                防止营养过剩，因进食过多可导致消化不良，妊娠糖尿病。\n            </div>\n            <div class="article-title2">\n                <div>引用知识：</div>\n            </div>\n            <div class="article-other">\n                <div>孕早期有哪些注意事项呢？(来自孕产 APP)</div>\n            </div>\n            <div>孕早期在饮食方面有哪些注意事项呢？</div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/faq-detail/faq-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FaqDetailPage);
    return FaqDetailPage;
}());

//# sourceMappingURL=faq-detail.js.map

/***/ }),

/***/ 224:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FaqPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__faq_detail_faq_detail__ = __webpack_require__(223);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FaqPage = /** @class */ (function () {
    function FaqPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FaqPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                id: 1,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }, {
                id: 2,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }, {
                id: 3,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }
        ];
    };
    FaqPage.prototype.toPage = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__faq_detail_faq_detail__["a" /* FaqDetailPage */]);
    };
    FaqPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-faq',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/faq/faq.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>问与答</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="top-bar">\n            <input type="text" placeholder="请输入关键字" [(ngModel)]="keyword">\n            <ion-icon class="search" name="search"></ion-icon>\n        </div>\n        <div class="content-box">\n            <ul>\n                <div class="pos-line">\n                    <img src="../../../assets/imgs/faq-line.png" alt="">\n                </div>\n                <li *ngFor=\'let item of listData\'>\n                    <div class="top-img">\n                        <div class="circle-icon">\n                            <img src="../../../assets/imgs/circle.png" alt="">\n                        </div>\n                        <div>\n                            孕3周3天\n                        </div>\n                    </div>\n                    <div class="content" (click)=\'toPage(item.id)\'>\n                        <div class="content-title">\n                            {{item.title}}\n                        </div>\n                        <div class="content-box">\n                            {{item.body}}\n                        </div>\n                    </div>\n                </li>\n            </ul>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/faq/faq.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FaqPage);
    return FaqPage;
}());

//# sourceMappingURL=faq.js.map

/***/ }),

/***/ 225:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeedingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FeedingPage = /** @class */ (function () {
    function FeedingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FeedingPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                text: '母乳喂养',
                hide: true
            }, {
                text: '辅食喂养',
                hide: true
            }, {
                text: '混合喂养',
                hide: true
            }
        ];
        console.log('ionViewDidLoad FeedingPage');
    };
    FeedingPage.prototype.toNextPage = function (i) {
        for (var item = 0; item < this.listData.length; item++) {
            this.listData[item].hide = true;
        }
        this.listData[i].hide = false;
    };
    FeedingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-feeding',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/feeding/feeding.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>喂养方式</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="flex-box feeding-page">\n        <div class="box-title">\n            请选择喂养方式\n        </div>\n        <div class="feeding-page-box" *ngFor=\'let item of listData, let i = index\' (click)="toNextPage(i)">\n            {{item.text}}\n            <div class="pos-icon-gou" [ngClass]=\'{"hide": item.hide}\'>\n                <img src="../../assets/imgs/icon-gou.png" alt="">\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/feeding/feeding.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FeedingPage);
    return FeedingPage;
}());

//# sourceMappingURL=feeding.js.map

/***/ }),

/***/ 226:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FetalMovementRecordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var FetalMovementRecordPage = /** @class */ (function () {
    function FetalMovementRecordPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    FetalMovementRecordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fetal-movement-record',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/fetal-movement-record/fetal-movement-record.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>胎动记录</ion-title>\n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="header-box">\n            <div>开始时间</div>\n            <div>有效点击</div>\n            <div>点击次数</div>\n        </div>\n        <ul>\n            <li>\n                <div class="list-box first-child">\n                    <div>22：30—23：30</div>\n                    <div class="text-center">次</div>\n                    <div class="text-right">次</div>\n                </div>\n                <div class="list-box">\n                    <div>2018.09.20</div>\n                    <div class="text-center">孕8周</div>\n                    <div class="text-right">\n                        <span class="normal-btn">正常</span>\n                    </div>\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/fetal-movement-record/fetal-movement-record.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], FetalMovementRecordPage);
    return FetalMovementRecordPage;
}());

//# sourceMappingURL=fetal-movement-record.js.map

/***/ }),

/***/ 227:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FetalMovementPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fetal_movement_record_fetal_movement_record__ = __webpack_require__(226);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var FetalMovementPage = /** @class */ (function () {
    function FetalMovementPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.isBegin = false;
        this.beginText = '点击记录';
        this.clickTime = '59: 59';
        this.clicks = 0;
        this.validClicks = 0;
        this.beginTime = new Date().getHours() + ": " + new Date().getMinutes();
        this.needClearTimeOut = false;
    }
    FetalMovementPage.prototype.changeBegin = function () {
        if (!this.isBegin) {
            this.isBegin = true;
            this.needClearTimeOut = false;
            this.beginText = '动一下，点一下';
            this.beginTime = new Date().getHours() + ": " + new Date().getMinutes();
            var end = new Date().getTime() + 3600000;
            this.countTime(end);
        }
        else {
            this.clicks++;
            if (this.validClicks < 5) {
                this.validClicks++;
            }
        }
    };
    FetalMovementPage.prototype.checkEnd = function () {
        var _this = this;
        var alert = this.alertCtrl.create({
            title: '温馨提示',
            subTitle: "\u60A8\u8BB0\u5F55\u7684\u65F6\u95F4\u4E0D\u8DB3\u4E00\u5C0F\u65F6\uFF0C\u53EF\u80FD\u4F1A\u5F71\u54CD\u6B63\u786E\u7ED3\u679C\uFF0C\u786E\u5B9A\u8981\u7ED3\u675F\u5417\uFF1F",
            buttons: [
                {
                    text: '我知道了',
                    handler: function () {
                        console.log('确定按钮被点击');
                        _this.needClearTimeOut = true;
                        _this.isBegin = false;
                        _this.clickTime = '59: 59';
                        _this.beginText = '点击记录';
                    }
                }
            ]
        });
        alert.present();
    };
    FetalMovementPage.prototype.countTime = function (end) {
        //获取当前时间
        var date = new Date();
        var now = date.getTime();
        //设置截止时间
        // console.log('end', end);
        //时间差
        var leftTime = end - now;
        // console.log('leftTime', leftTime);
        //定义变量 d,h,m,s保存倒计时的时间
        var m, s;
        if (leftTime >= 0) {
            m = Math.floor(leftTime / 1000 / 60 % 60);
            s = Math.floor(leftTime / 1000 % 60);
        }
        if (m != 0) {
            this.clickTime = m + ": " + s;
        }
        // console.log(this.clickTime);
        //递归每秒调用countTime方法，显示动态时间效果;
        var that = this;
        if (this.needClearTimeOut) {
            return false;
        }
        setTimeout(function () {
            // console.log('setTime', end);
            that.countTime(end);
        }, 1000);
    };
    FetalMovementPage.prototype.record = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__fetal_movement_record_fetal_movement_record__["a" /* FetalMovementRecordPage */]);
    };
    FetalMovementPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-fetal-movement',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/fetal-movement/fetal-movement.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>数胎动</ion-title>\n        <button ion-button clear icon-right (click)=\'record()\' style="padding: 0; position: absolute; right: 6px; top: 6px;height: 22px">  \n            <img class="back-btn" src="../../../assets/imgs/history-icon.png" />  \n        </button>  \n    </ion-navbar>\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="header-box">\n            <div class="heart-box" (click)=\'changeBegin()\'>   \n                <img src="../../../assets/imgs/heart_img.png" alt="">\n                <div>{{beginText}}</div>\n            </div>\n            <div *ngIf=\'isBegin\'>{{clickTime}}</div>\n            <div>\n                正常胎动不少于3次/小时，30次/12小时\n            </div>\n        </div>\n        <div class="remark-box" *ngIf=\'!isBegin\'>\n            <!-- <img class=\'img\' src="../../../assets/imgs/border-shadow.png" alt=""> -->\n            <div>操作说明</div>\n            <ul>\n                <!-- <img class=\'line\' src="../../../assets/imgs/line.png" alt=""> -->\n                \n                <li>\n                    <img src="../../../assets/imgs/circle.png" alt="">\n                    <div>胎动时，孕妇最好采用左侧卧位的姿势，在安静的环境中，思想保持集中，心情要平静以确保测量数据的准确性。</div>\n                </li>\n                <li>\n                    <img src="../../../assets/imgs/circle.png" alt="">\n                    <div>每天早、中、晚各计数胎动一小时，将三次记录的胎动相加后乘以4，就得到12小时的胎动次数。</div>\n                </li>\n                <li>\n                    <img src="../../../assets/imgs/circle.png" alt="">\n                    <div>不管是单胎还是双胞胎，数胎动是从开始动到一系列动作结束为一次胎动。</div>\n                </li>\n            </ul>\n        </div>\n\n        <div class="table-content" *ngIf=\'isBegin\'>\n            <img src="../../../assets/imgs/border-shadow.png" alt="">\n            <div class=\'item-box\'>\n                <div>开始时间</div>\n                <div>{{beginTime}}</div>\n            </div>\n            <div class=\'item-box\'>\n                <div>有效点击</div>\n                <div>{{validClicks}}次</div>\n            </div>\n            <div class=\'item-box\'>\n                <div>点击次数</div>\n                <div>{{clicks}}次</div>\n            </div>\n        </div>\n        <div class="footer-btn" *ngIf=\'isBegin\'>\n            <button ion-button (click)=\'checkEnd()\'>结束观察</button>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/fetal-movement/fetal-movement.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], FetalMovementPage);
    return FetalMovementPage;
}());

//# sourceMappingURL=fetal-movement.js.map

/***/ }),

/***/ 228:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GadgetPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__fetal_movement_fetal_movement__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__prenatal_education_prenatal_education__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__weight_curve_weight_curve__ = __webpack_require__(230);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var GadgetPage = /** @class */ (function () {
    function GadgetPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    GadgetPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                text: '胎教音乐',
                img: '../../../assets/imgs/gadget-icon1.png'
            }, {
                text: '记体重',
                img: '../../../assets/imgs/gadget-icon2.png'
            }, {
                text: '产检时间',
                img: '../../../assets/imgs/gadget-icon3.png'
            }, {
                text: '数胎动',
                img: '../../../assets/imgs/gadget-icon4.png'
            }, {
                text: '食物GI值',
                img: '../../../assets/imgs/gadget-icon5.png'
            }, {
                text: '宝宝接种计划',
                img: '../../../assets/imgs/gadget-icon5.png'
            }
        ];
    };
    GadgetPage.prototype.toPage = function (i) {
        switch (i) {
            case 0:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__prenatal_education_prenatal_education__["a" /* PrenatalEducationPage */]);
                break;
            case 1:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__weight_curve_weight_curve__["a" /* WeightCurvePage */]);
                break;
            case 2:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__fetal_movement_fetal_movement__["a" /* FetalMovementPage */]);
                break;
            case 3:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__fetal_movement_fetal_movement__["a" /* FetalMovementPage */]);
                break;
            case 4:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__fetal_movement_fetal_movement__["a" /* FetalMovementPage */]);
                break;
            case 5:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__fetal_movement_fetal_movement__["a" /* FetalMovementPage */]);
                break;
            default:
                break;
        }
    };
    GadgetPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-gadget',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/gadget/gadget.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>小工具</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <ul class=\'content-box\'>\n            <li *ngFor=\'let item of listData, let i = index\' (click)=\'toPage(i)\'> \n                <img src="{{item.img}}" alt="">\n                <div>{{item.text}}</div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/gadget/gadget.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], GadgetPage);
    return GadgetPage;
}());

//# sourceMappingURL=gadget.js.map

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PrenatalEducationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PrenatalEducationPage = /** @class */ (function () {
    function PrenatalEducationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PrenatalEducationPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                bgImg: '../../../assets/imgs/prenatal-education-img1.png',
                isPlay: false,
                title: '大魔法与小蝴蝶',
            }, {
                bgImg: '../../../assets/imgs/prenatal-education-img2.png',
                isPlay: false,
                title: '彩云和唐老鸭',
            }, {
                bgImg: '../../../assets/imgs/prenatal-education-img3.png',
                isPlay: false,
                title: '彩云和唐老鸭',
            }, {
                bgImg: '../../../assets/imgs/prenatal-education-img4.png',
                isPlay: false,
                title: '大魔法与小蝴蝶',
            }
        ];
    };
    PrenatalEducationPage.prototype.play = function (i) {
        if (this.listData[i].isPlay) {
            this.listData[i].isPlay = false;
        }
        else {
            for (var i_1 = 0; i_1 < this.listData.length; i_1++) {
                this.listData[i_1].isPlay = false;
            }
            this.listData[i].isPlay = true;
        }
    };
    PrenatalEducationPage.prototype.collect = function (i) {
        console.log('收藏');
    };
    PrenatalEducationPage.prototype.download = function (i) {
        console.log('下载');
    };
    PrenatalEducationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-prenatal-education',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/prenatal-education/prenatal-education.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>胎教音乐</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="music-box" *ngFor=\'let item of listData, let i = index\'>\n            <img class=\'pos-bg\' src="{{item.bgImg}}" alt="">\n            <div class="pos-right" *ngIf=\'item.isPlay\'>\n                <img src="../../../assets/imgs/download.png" (click)=\'download(i)\' alt="">\n                <img src="../../../assets/imgs/shoucang.png" (click)=\'collect(i)\' alt="">\n            </div>\n            <img *ngIf=\'!item.isPlay\' src="../../../assets/imgs/play.png" (click)="play(i)" alt="">\n            <img *ngIf=\'item.isPlay\' src="../../../assets/imgs/pause.png" (click)="play(i)" alt="">\n            <div>{{item.title}}</div>\n            <img *ngIf=\'item.isPlay\' class=\'play-line\' src="../../../assets/imgs/music.png" alt="">\n        </div>\n        <audio src=""></audio>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/prenatal-education/prenatal-education.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PrenatalEducationPage);
    return PrenatalEducationPage;
}());

//# sourceMappingURL=prenatal-education.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WeightCurvePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_echarts__ = __webpack_require__(483);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_echarts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_echarts__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var WeightCurvePage = /** @class */ (function () {
    function WeightCurvePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        console.log(__WEBPACK_IMPORTED_MODULE_2_echarts__);
    }
    WeightCurvePage.prototype.ionViewDidEnter = function () {
        var ec = __WEBPACK_IMPORTED_MODULE_2_echarts__;
        var myChart = ec.init(document.getElementById('chart'));
        var option = {
            xAxis: {
                type: 'category',
                boundaryGap: false,
                data: ['出生', '10.2', '10.3', '10.4', '10.5', '10.6', '10.7']
            },
            yAxis: {
                min: 17,
                max: 170,
                type: 'value'
            },
            dataZoom: [{
                    type: 'slider',
                    show: false
                }, {
                    type: "inside" //详细配置可见echarts官网
                }],
            series: [{
                    data: [20, 50, 80, 110, 140],
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: '#ccc',
                            lineStyle: {
                                width: 2,
                                type: 'solid',
                                color: "#ccc"
                            }
                        },
                        emphasis: {
                            color: '#4fd6d2',
                            lineStyle: {
                                width: 2,
                                type: 'dotted',
                                color: "#4fd6d2" //折线的颜色
                            }
                        }
                    },
                    areaStyle: { normal: {} },
                }, {
                    data: [0, 30, 60, 90, 120],
                    type: 'line',
                    smooth: true,
                    itemStyle: {
                        normal: {
                            color: '#Fff',
                            lineStyle: {
                                width: 2,
                                type: 'solid',
                                color: "#ccc"
                            }
                        },
                        emphasis: {
                            color: '#4fd6d2',
                            lineStyle: {
                                width: 2,
                                type: 'dotted',
                                color: "#4fd6d2" //折线的颜色
                            }
                        }
                    },
                    areaStyle: { normal: {} },
                }, {
                    data: [20, 38, 66, 103, 78],
                    type: 'line',
                    smooth: true
                }]
        };
        myChart.setOption(option);
    };
    WeightCurvePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-weight-curve',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/weight-curve/weight-curve.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>记体重</ion-title>\n        <button ion-button clear icon-right (click)=\'record()\' style="padding: 0; position: absolute; right: 6px; top: 6px;height: 22px">  \n            <img class="back-btn" src="../../../assets/imgs/history-icon.png" />  \n        </button>  \n    </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n        <!-- <div id="chart"></div> -->\n    <div class="normal-page">\n        <div class="header-box">\n            <div>孕3周+5天</div>\n            <img src="../../../assets/imgs/click-record.png" alt="">\n        </div>\n        <div class="curve-box">\n            <div class="title">孕期体重曲线</div>\n            <div class="canvas-box">\n                <div id="chart"></div>\n            </div>\n        </div>\n        <div class="footer-btn">\n            <button ion-button>保存</button>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/weight-curve/weight-curve.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], WeightCurvePage);
    return WeightCurvePage;
}());

//# sourceMappingURL=weight-curve.js.map

/***/ }),

/***/ 231:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KnowledgeBasePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__knowledge_detail_knowledge_detail__ = __webpack_require__(105);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var KnowledgeBasePage = /** @class */ (function () {
    function KnowledgeBasePage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.keyword = "";
        this.searching = false;
    }
    ;
    ;
    KnowledgeBasePage.prototype.ngOnInit = function () {
        this.storgeData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？",
            }, {
                id: 2,
                text: "孕期运动",
            }, {
                id: 3,
                text: "孕早期",
            }, {
                id: 4,
                text: "妊娠期",
            }
        ];
        this.allData = [
            {
                id: 1,
                title: "孕早期禁食有哪些？",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }, {
                id: 2,
                title: "孕期运动",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }, {
                id: 3,
                title: "如何进行宝宝胎教，做哪些事情比较好",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }, {
                id: 4,
                title: "妊娠期",
                content: '孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......孕妇在孕早期一般注意不要疲劳，行动要舒缓，要避免不协调的运动，避免冲撞，颠簸运动......',
                type: '知识库',
                stage: '孕早期'
            }
        ];
        console.log('ionViewDidLoad KnowledgeBasePage');
    };
    KnowledgeBasePage.prototype.toPage = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__knowledge_detail_knowledge_detail__["a" /* KnowledgeDetailPage */]);
    };
    KnowledgeBasePage.prototype.inputFocus = function () {
        if (this.keyword != '') {
            this.searching = true;
        }
    };
    KnowledgeBasePage.prototype.clearAll = function () {
        this.storgeData = [];
    };
    KnowledgeBasePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-knowledge-base',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/knowledge-base/knowledge-base.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>知识库</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="top-bar">\n            <input type="text" placeholder="请输入关键字" (focus)=\'inputFocus()\' [(ngModel)]="keyword"><ion-icon class="search" name="search"></ion-icon>\n        </div>\n        <div class=\'search-text-box\' *ngIf=\'!searching\'>\n            <div class="box-title">\n                <span>搜索记录：</span>\n                <span class="for-list" (click)=\'clearAll()\'>清空</span>\n            </div>\n            <div class="flex-box">\n                <div *ngFor="let i of storgeData">\n                    <div class="for-list">\n                        {{i.text}}\n                    </div>\n                </div>\n            </div>\n            <div class="box-title">大家都在搜：</div>\n            <div class="flex-box">\n                <div *ngFor="let i of storgeData">\n                    <div class="for-list">\n                        {{i.text}}\n                    </div>\n                </div>\n            </div>\n        </div>\n        <ul class="flex-ul" *ngIf=\'searching\'>\n            <li *ngFor="let i of allData" (click)=\'toPage(i.id)\'>\n                <div class="article-title">\n                    <span [innerHTML]=\'i.title | wordPlace:keyword\'></span>\n                </div>\n                <div class="article-content">\n                    {{i.content}}\n                </div>\n                <div class="article-remark">\n                    <span>{{i.type}} </span>\n                    <span>{{i.stage}}</span>\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/knowledge-base/knowledge-base.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], KnowledgeBasePage);
    return KnowledgeBasePage;
}());

//# sourceMappingURL=knowledge-base.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return KnowledgesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__knowledge_detail_knowledge_detail__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__knowledge_base_knowledge_base__ = __webpack_require__(231);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var KnowledgesPage = /** @class */ (function () {
    function KnowledgesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    KnowledgesPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                id: 1,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }, {
                id: 2,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }, {
                id: 3,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }
        ];
    };
    KnowledgesPage.prototype.toPage = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__knowledge_detail_knowledge_detail__["a" /* KnowledgeDetailPage */]);
    };
    KnowledgesPage.prototype.focusInput = function () {
        console.log('聚焦');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__knowledge_base_knowledge_base__["a" /* KnowledgeBasePage */]);
    };
    KnowledgesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-knowledges',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/knowledges/knowledges.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>知识库</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="top-bar">\n            <input type="text" placeholder="请输入关键字" (focus)="focusInput()" [(ngModel)]="keyword">\n            <ion-icon class="search" name="search"></ion-icon>\n        </div>\n        <div class="content-box">\n            <ul>\n                <div class="pos-line">\n                    <img src="../../../assets/imgs/faq-line.png" alt="">\n                </div>\n                <li *ngFor=\'let item of listData\'>\n                    <div class="pos-right-state">\n                        <span>孕早期</span>\n                        <img src="../../../assets/imgs/knowledge-right-bar.png" alt="">\n                    </div>\n                    <div class="top-img">\n                        <div class="circle-icon">\n                            <img src="../../../assets/imgs/circle.png" alt="">\n                        </div>\n                        <div>\n                            欢迎您的到来\n                        </div>\n                    </div>\n                    <div class="content">\n                        <div class="flex-box">\n                            <div>孕8周+3天</div>\n                            <div>2018.10.23</div>\n                        </div>\n                    </div>\n                    <div class="content">\n                        <div class="content-title">\n                            {{item.title}}\n                        </div>\n                        <div class="content-test">\n                            <div>关于孕早期的相关介绍内容</div>\n                            <div>孕早期的介绍孕早期的介绍孕早期的介绍</div>\n                            <div>孕早期的介绍孕早期的介绍</div>\n                        </div>\n                        <div class="pos-circle-right">\n                            <img src="../../../assets/imgs/circle.png" alt="">\n                        </div>\n                    </div>\n                    <div class="content" (click)=\'toPage(item.id)\'>\n                        <div class="content-title">\n                            {{item.title}}\n                        </div>\n                        <div class="content-img">\n                            <img src="../../../assets/imgs/kownledge-img.png" alt="">\n                        </div>\n                    </div>\n                </li>\n            </ul>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/knowledges/knowledges.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], KnowledgesPage);
    return KnowledgesPage;
}());

//# sourceMappingURL=knowledges.js.map

/***/ }),

/***/ 233:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PostDetailPage = /** @class */ (function () {
    function PostDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PostDetailPage.prototype.ngOnInit = function () {
        this.imgList = [
            {
                img: '../../../assets/imgs/post-img.png'
            }, {
                img: '../../../assets/imgs/post-img.png'
            }, {
                img: '../../../assets/imgs/post-img.png'
            }
        ];
    };
    PostDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-post-detail',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/post-detail/post-detail.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>孕育交流</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <ul class=\'list\'>\n            <li>\n                <div class="list-box">\n                    <div class="header-img">\n                        <img src="../../../assets/imgs/article-header-img.png" alt="">\n                    </div>\n                    <div class="username">\n                        <div>待产孕妈妈~~</div>\n                        <div class="font-color3">孕20周5天</div>\n                    </div>\n                    <div class="label">\n                        孕期运动\n                    </div>\n                </div>\n                <div class="content">\n                    <div class="text-box">\n                        不知道孕期妈妈们都在用哪些孕产APP呀，待产包需要准备什么呢？\n                    </div>\n                    <div class="img-box">\n                        <div class="img-div" *ngFor=\'let item of imgList\' [ngStyle]="{\'width\': imgList.legnth == 1 ? \'100%\' : \'33.33%\'}">\n                            <img src="{{item.img}}" alt="">\n                        </div>\n                    </div>\n                </div>\n                <div class="footer">\n                    <div class="icon-box">\n                        <img src="../../../assets/imgs/talk.png" alt="">\n                        <span>234</span>\n                    </div>\n                    <div class="icon-box">\n                        <img src="../../../assets/imgs/talk.png" alt="">\n                        <span>234</span>\n                    </div>\n                </div>\n            </li>\n        </ul>\n        <div class="article-talk">\n            <div class="article-talk-title">全部回复</div>\n            <ul>\n                <li>\n                    <div class="header-img">\n                        <img src="../../../assets/imgs/article-header-img.png" alt="">\n                    </div>\n                    <div class="talk-title">\n                        <div>待产孕妈妈1</div>\n                        <div>孕20周5天</div>\n                        <div>孕育桥、孕迹暖暖都在用哦!</div>\n                        <div class="talk-reply">\n                            <div>作者回复</div>\n                            <div>\n                                1212\n                            </div>\n                        </div>\n                    </div>\n                    <div class="talk-content">\n                        <div>15分钟前</div>\n                        <div>\n                            <ion-icon name="heart" color="danger"></ion-icon>\n                            <span>223</span>\n                        </div>\n                    </div>\n                </li>\n            </ul>\n        </div>\n        <div class="article-footer">\n            <input type="text" placeholder="输入留言" [(ngModel)]="keyword">\n            <span class="search">发布</span>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/post-detail/post-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PostDetailPage);
    return PostDetailPage;
}());

//# sourceMappingURL=post-detail.js.map

/***/ }),

/***/ 234:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostingPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PostingPage = /** @class */ (function () {
    function PostingPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PostingPage.prototype.ngOnInit = function () {
        this.fileList = [];
        this.allData = [
            {
                text: '孕期运动'
            }, {
                text: '孕期运动'
            }
        ];
    };
    PostingPage.prototype.changepic = function () {
        // let reads= new FileReader();
        // let f=document.getElementById('file').files[0];
        // reads.readAsDataURL(f);
        // reads.onload=function (e) {
        //     document.getElementById('show').src=this.result;
        // };
    };
    PostingPage.prototype.inpChange = function () {
        // console.log(this.fileModel);
        // console.log(document.getElementById('file').files[0]);
        var that = this;
        var reads = new FileReader();
        var f = document.getElementById('file').files[0];
        reads.readAsDataURL(f);
        reads.onload = function (e) {
            that.fileList.push({
                img: this.result
            });
        };
    };
    PostingPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-posting',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/posting/posting.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>发布帖子</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="top-textarea">\n            <textarea name="" id="" placeholder="写下来，分享孕期生活的点点滴滴吧！"></textarea>\n        </div>\n        <div class="img-box">\n            <div class="img" *ngFor=\'let item of fileList\'>\n                <img src="{{item.img}}" alt="">\n            </div>\n            <div class="img">\n                <input type="file" id=\'file\' (change)=\'inpChange()\'>\n                <img src="../../../assets/imgs/pointing-point.png" alt="">\n                <span>图片</span>\n            </div>\n        </div>\n        <div class="label-box">\n            <div>\n                添加标签\n            </div>\n            <div class="flex-box">\n                <div *ngFor="let i of allData">\n                    <div class="for-list">\n                        {{i.text}}\n                    </div>\n                </div>\n                <div>\n                    <div class="input-box">\n                        <input type="text" placeholder="+新标签">\n                        <img src="../../../assets/imgs/point-right.png" alt="">\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/posting/posting.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PostingPage);
    return PostingPage;
}());

//# sourceMappingURL=posting.js.map

/***/ }),

/***/ 235:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PostsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__post_detail_post_detail__ = __webpack_require__(233);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PostsPage = /** @class */ (function () {
    function PostsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    PostsPage.prototype.ngOnInit = function () {
        this.imgList = [
            {
                img: '../../../assets/imgs/post-img.png'
            }, {
                img: '../../../assets/imgs/post-img.png'
            }, {
                img: '../../../assets/imgs/post-img.png'
            }
        ];
    };
    PostsPage.prototype.toPointing = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__post_detail_post_detail__["a" /* PostDetailPage */]);
    };
    PostsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-posts',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/posts/posts.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>孕育交流</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <ul>\n            <li>\n                <div class="list-box">\n                    <div class="header-img">\n                        <img src="../../../assets/imgs/article-header-img.png" alt="">\n                    </div>\n                    <div class="username">\n                        <div>待产孕妈妈~~</div>\n                        <div class="font-color3">孕20周5天</div>\n                    </div>\n                    <div class="label">\n                        孕期运动\n                    </div>\n                </div>\n                <div class="content">\n                    <div class="text-box">\n                        不知道孕期妈妈们都在用哪些孕产APP呀，待产包需要准备什么呢？\n                    </div>\n                    <div class="img-box">\n                        <div class="img-div" *ngFor=\'let item of imgList\' [ngStyle]="{\'width\': imgList.legnth == 1 ? \'100%\' : \'33.33%\'}">\n                            <img src="{{item.img}}" alt="">\n                        </div>\n                    </div>\n                </div>\n                <div class="footer">\n                    <div class="icon-box">\n                        <img src="../../../assets/imgs/talk.png" alt="">\n                        <span>234</span>\n                    </div>\n                    <div class="icon-box" (click)=\'toPointing()\'>\n                        <img src="../../../assets/imgs/talk.png" alt="">\n                        <span>234</span>\n                    </div>\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/posts/posts.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PostsPage);
    return PostsPage;
}());

//# sourceMappingURL=posts.js.map

/***/ }),

/***/ 236:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecipesOnedayPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RecipesOnedayPage = /** @class */ (function () {
    function RecipesOnedayPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.allData = null;
        this.keyword = "";
        this.lwidth = 5;
        this.lheight = 10;
        this.checkMoreTrue = false;
        this.checkMoreText = '查看更多';
    }
    RecipesOnedayPage.prototype.ngOnInit = function () {
        console.log('ionViewDidLoad RecipesOnedayPage');
        this.allData = [
            {
                id: 1,
                text: "全麦面包",
                num: '2',
                danwei: '片'
            }, {
                id: 2,
                text: "鸡蛋",
                num: '1',
                danwei: '个'
            }, {
                id: 3,
                text: "番茄",
                num: '30',
                danwei: 'g'
            }, {
                id: 4,
                text: "生菜",
                num: '30',
                danwei: 'g'
            }, {
                id: 5,
                text: "干酪",
                num: '30',
                danwei: 'g'
            }
        ];
        this.firstData = [
            {
                id: 1,
                text: "全麦面包",
                num: '2',
                danwei: '片'
            }, {
                id: 2,
                text: "鸡蛋",
                num: '1',
                danwei: '个'
            }
        ];
        this.flowData = [
            {
                text: '1',
                lheight: '4'
            }, {
                text: '1',
                lheight: '5'
            }, {
                text: '1',
                lheight: '5'
            }, {
                text: '1',
                lheight: '4'
            }, {
                text: '1',
                lheight: '2'
            }, {
                text: '1',
                lheight: '4'
            }, {
                text: '1',
                lheight: '3'
            }, {
                text: '1',
                lheight: '5'
            }
        ];
        this.footerData = [
            {
                text: '能量',
                num: '1857kcal'
            }, {
                text: '能量',
                num: '1857kcal'
            }, {
                text: '能量',
                num: '1857kcal'
            }, {
                text: '能量',
                num: '1857kcal'
            }, {
                text: '能量',
                num: '1857kcal'
            }, {
                text: '能量',
                num: '1857kcal'
            }
        ];
    };
    RecipesOnedayPage.prototype.checkMore = function () {
        this.checkMoreTrue = !this.checkMoreTrue;
        if (!this.checkMoreTrue) {
            this.checkMoreText = '查看更多';
        }
        else {
            this.checkMoreText = '收起明细';
        }
    };
    RecipesOnedayPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-recipes-oneday',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/recipes-oneday/recipes-oneday.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>食谱详情</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="normal-page">\n        <div class="header-img">\n            <img src="../../../assets/imgs/recipes.png" alt="">\n        </div>\n        <div class="recipes-content">\n            <div class="article-header">\n                <div class="article-title">\n                    <div>周一食谱</div>\n                    <div>\n                        <ion-icon name="star" style=\'color: yellow\'></ion-icon>\n                        <ion-icon ios="md-share" md="md-share" color="secondary"></ion-icon>\n                    </div>\n                </div>\n                <div class="article-auther-remark">\n                    21121212\n                </div>\n            </div>\n            <div class="article-header">\n                <div class="article-title">\n                    <div>早餐：</div>\n                </div>\n                <div class="content-img">\n                    <img src="../../../assets/imgs/recipes-img.png" alt="">\n                </div>\n                <div class="content-first">\n                    <div class="first-box">\n                        <div class="circle"></div>\n                        <div>鸡蛋三明治</div>\n                    </div>\n                </div>\n                <div class="flex-box">\n                    <div *ngFor="let i of allData">\n                        <div class="for-list">\n                            {{i.text}} * {{i.num}}{{i.danwei}}\n                        </div>\n                    </div>\n                </div>\n                <div class="box-title">\n                    <div>上午加餐：</div>\n                </div>\n                <div class="flex-box">\n                    <div *ngFor="let i of firstData">\n                        <div class="for-list">\n                            {{i.text}} * {{i.num}}{{i.danwei}}\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class="article-header">\n                <div class="article-title">\n                    <div>午餐：</div>\n                </div>\n                <div class="content-img">\n                    <img src="../../../assets/imgs/recipes-img.png" alt="">\n                </div>\n                <div class="content-first">\n                    <div class="first-box">\n                        <div class="circle"></div>\n                        <div>鸡蛋三明治</div>\n                    </div>\n                </div>\n                <div class="flex-box">\n                    <div *ngFor="let i of allData">\n                        <div class="for-list">\n                            {{i.text}} * {{i.num}}{{i.danwei}}\n                        </div>\n                    </div>\n                </div>\n                <div class="box-title">\n                    <div>午餐加餐：</div>\n                </div>\n                <div class="flex-box">\n                    <div *ngFor="let i of firstData">\n                        <div class="for-list">\n                            {{i.text}} * {{i.num}}{{i.danwei}}\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class="article-header">\n                <div class="article-title">\n                    <div>晚餐：</div>\n                </div>\n                <div class="content-img">\n                    <img src="../../../assets/imgs/recipes-img.png" alt="">\n                </div>\n                <div class="content-first">\n                    <div class="first-box">\n                        <div class="circle"></div>\n                        <div>鸡蛋三明治</div>\n                    </div>\n                </div>\n                <div class="flex-box">\n                    <div *ngFor="let i of allData">\n                        <div class="for-list">\n                            {{i.text}} * {{i.num}}{{i.danwei}}\n                        </div>\n                    </div>\n                </div>\n                <div class="box-title">\n                    <div>晚餐加餐：</div>\n                </div>\n                <div class="flex-box">\n                    <div *ngFor="let i of firstData">\n                        <div class="for-list">\n                            {{i.text}} * {{i.num}}{{i.danwei}}\n                        </div>\n                    </div>\n                </div>\n            </div>\n            <div class="nutrient">\n                <div class="article-title">\n                    <div>营养素：</div>\n                </div>\n                <div class="nutrient-flow" id="container">\n                    <ul>\n                        <li class=\'box\' *ngFor=\'let item of flowData\' [ngStyle]="{\'height\': item.lheight + \'rem\', \'width\': item.lheight + \'rem\'}">\n                            <div class="box_bg">\n                                {{item.text}}\n                            </div>\n                        </li>\n                    </ul>\n                    <div class="footer-content" *ngIf=\'checkMoreTrue\'>\n                        <div class="article-title">\n                            <div>营养素详细明细：</div>\n                        </div>\n                        <div class="footer-content-box">\n                            <div class="footer-list" *ngFor=\'let item of footerData, let i = index\' [ngStyle]="{\'border-right\': (i+1)%3==0 ? \'none\':\'1px solid #D6D6D6\'}">\n                                {{item.text}} {{item.num}}\n                            </div>\n                        </div>\n                    </div>\n                    <div class="footer-other">\n                        <button ion-button (click)=\'checkMore()\'>{{checkMoreText}}</button>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/recipes-oneday/recipes-oneday.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], RecipesOnedayPage);
    return RecipesOnedayPage;
}());

//# sourceMappingURL=recipes-oneday.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecipesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__recipes_oneday_recipes_oneday__ = __webpack_require__(236);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RecipesPage = /** @class */ (function () {
    function RecipesPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.isChoiced = false;
    }
    RecipesPage.prototype.ngOnInit = function () {
        this.text = '孕早期';
        this.listText = [
            {
                isChoice: true,
                text: '孕早期'
            }, {
                isChoice: false,
                text: '孕中期'
            }, {
                isChoice: false,
                text: '孕晚期'
            }, {
                isChoice: false,
                text: 'GDM食谱'
            }
        ];
        this.listData = [
            {
                img: '../../../assets/imgs/recipes-img1.png',
                firstText: '一日之餐·周一',
                secondText: '周一的食谱',
                thirdText: '食谱的内容简介',
            }, {
                img: '../../../assets/imgs/recipes-img2.png',
                firstText: '一日之餐·周一',
                secondText: '周一的食谱',
                thirdText: '食谱的内容简介',
            }, {
                img: '../../../assets/imgs/recipes-img1.png',
                firstText: '一日之餐·周一',
                secondText: '周一的食谱',
                thirdText: '食谱的内容简介',
            }, {
                img: '../../../assets/imgs/recipes-img1.png',
                firstText: '一日之餐·周一',
                secondText: '周一的食谱',
                thirdText: '食谱的内容简介',
            }, {
                img: '../../../assets/imgs/recipes-img1.png',
                firstText: '一日之餐·周一',
                secondText: '周一的食谱',
                thirdText: '食谱的内容简介',
            }
        ];
    };
    RecipesPage.prototype.modelAlert = function () {
        this.isChoiced = true;
    };
    RecipesPage.prototype.toPage = function (i) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__recipes_oneday_recipes_oneday__["a" /* RecipesOnedayPage */]);
    };
    RecipesPage.prototype.clickItem = function (index) {
        for (var i = 0; i < this.listText.length; i++) {
            this.listText[i].isChoice = false;
        }
        this.listText[index].isChoice = true;
    };
    RecipesPage.prototype.clickClose = function () {
        this.isChoiced = false;
    };
    RecipesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-recipes',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/recipes/recipes.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>营养食谱</ion-title>\n        <button ion-button clear (click)=\'modelAlert()\' style="padding: 0; position: absolute; right: 6px; top: 6px;height: 22px">  \n            <img class="back-btn" src="../../../assets/imgs/top-list.png" />  \n        </button> \n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <div class="modal-page" *ngIf=\'isChoiced\'>\n            <div *ngFor=\'let item of listText, let i = index\' (click)=\'clickItem(i)\' [ngClass]="{\'text-bar\': \'true\', \'style1\': item.isChoice}"><span>{{item.text}}</span></div>\n            <div class="pos-footer">\n                <img (click)=\'clickClose()\' src="../../../assets/imgs/close.png" alt="">\n            </div>\n        </div>\n        <ul>\n            <div class="pos-text">{{text}}</div>\n            <li *ngFor=\'let item of listData, let i = index\' (click)=\'toPage(i)\' [ngStyle]=\'{"flex-direction": i%2==0?"row":"row-reverse"}\'>\n                <div class="item-img">\n                    <img src="{{item.img}}" alt="">\n                </div>\n                <div class="item-box">\n                    <div>{{item.firstText}}</div>\n                    <div>{{item.secondText}}</div>\n                    <div>{{item.thirdText}}</div>\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/recipes/recipes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], RecipesPage);
    return RecipesPage;
}());

//# sourceMappingURL=recipes.js.map

/***/ }),

/***/ 238:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuccessStoryDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var SuccessStoryDetailPage = /** @class */ (function () {
    function SuccessStoryDetailPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SuccessStoryDetailPage.prototype.ngOnInit = function () {
        this.allData = [
            {
                id: 1,
                text: "孕早期禁食有哪些？"
            }, {
                id: 2,
                text: "孕期运动"
            }, {
                id: 3,
                text: "如何进行宝宝胎教，做哪些事情比较好"
            }, {
                id: 4,
                text: "妊娠期"
            }
        ];
    };
    SuccessStoryDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-success-story-detail',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/success-story-detail/success-story-detail.html"*/'<ion-header>\n\n        <ion-navbar>\n            <ion-title>故事详情</ion-title>\n        </ion-navbar>\n    \n    </ion-header>\n    \n    \n    <ion-content padding>\n        <div class="normal-page">\n            <div class="header-img">\n                <img src="../../../assets/imgs/article-bg-img.png" alt="">\n            </div>\n            <div class="article-header">\n                <div class="article-title">\n                    <div>孕早期有哪些注意事项呢？</div>\n                    <div>\n                        <ion-icon name="star" style=\'color: yellow\'></ion-icon>\n                        <ion-icon ios="md-share" md="md-share" color="secondary"></ion-icon>\n                    </div>\n                </div>\n                <div class=\'article-time\'>\n                        2018.03.02\n                </div>\n                <div class="article-auther-header"> \n                    <div class="header-img">\n                        <img src="../../../assets/imgs/article-header-img.png" alt="">\n                    </div>\n                    <div class="auther-detail">\n                        <div>复旦营养博士</div>\n                        <div>徐春来</div>\n                    </div>\n                </div>\n                <div class="article-auther-remark">\n                    <div>1212</div>\n                    <div>1212</div>\n                </div>\n            </div>\n            <div class="article-content">\n                1212121\n            </div>\n            <div class="article-talk">\n                <div class="article-talk-title">全部评论</div>\n                <ul>\n                    <li>\n                        <div class="header-img">\n                            <img src="../../../assets/imgs/article-header-img.png" alt="">\n                        </div>\n                        <div class="talk-title">\n                            <div>待产孕妈妈1</div>\n                            <div>孕20周5天</div>\n                            <div>孕育桥、孕迹暖暖都在用哦!</div>\n                            <div class="talk-reply">\n                                <div>作者回复</div>\n                                <div>\n                                    1212\n                                </div>\n                            </div>\n                        </div>\n                        <div class="talk-content">\n                            <div>15分钟前</div>\n                            <div>\n                                <ion-icon name="heart" color="danger"></ion-icon>\n                                <span>223</span>\n                            </div>\n                        </div>\n                    </li>\n                </ul>\n            </div>\n            <div class="article-footer">\n                <input type="text" placeholder="请输入关键字" [(ngModel)]="keyword">\n                <span class="search">发布</span>\n            </div>\n        </div>\n    </ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/success-story-detail/success-story-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SuccessStoryDetailPage);
    return SuccessStoryDetailPage;
}());

//# sourceMappingURL=success-story-detail.js.map

/***/ }),

/***/ 239:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SuccessStoryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__success_story_detail_success_story_detail__ = __webpack_require__(238);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var SuccessStoryPage = /** @class */ (function () {
    function SuccessStoryPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    SuccessStoryPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                id: 1,
                img: '../../../assets/imgs/story-img.png',
                title: '孕妈们如何孕期长胎不长肉的科学秘籍',
                resource: '复旦孕产官方小助手',
                from: '来自“天真的梅子酱”'
            }, {
                id: 2,
                img: '../../../assets/imgs/story-img.png',
                title: '孕妈们如何孕期长胎不长肉的科学秘籍',
                resource: '复旦孕产官方小助手',
                from: '来自“天真的梅子酱”'
            }, {
                id: 3,
                img: '../../../assets/imgs/story-img.png',
                title: '孕妈们如何孕期长胎不长肉的科学秘籍',
                resource: '复旦孕产官方小助手',
                from: '来自“天真的梅子酱”'
            }
        ];
    };
    SuccessStoryPage.prototype.toDetail = function (id) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__success_story_detail_success_story_detail__["a" /* SuccessStoryDetailPage */]);
    };
    SuccessStoryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-success-story',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/success-story/success-story.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>分享成功故事</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n    <div class="normal-page">\n        <ul class=\'success-box\'>\n            <li *ngFor=\'let item of listData\' (click)=\'toDetail(item.id)\'>\n                <div class="left-img">\n                    <img src="{{item.img}}" alt="">\n                </div>\n                <div class="right-text">\n                    <div>\n                        {{item.title}}\n                    </div>\n                    <div>{{item.resource}}</div>\n                    <div class="font-color1">{{item.from}}</div>\n                </div>\n            </li>\n        </ul>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/success-story/success-story.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], SuccessStoryPage);
    return SuccessStoryPage;
}());

//# sourceMappingURL=success-story.js.map

/***/ }),

/***/ 240:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HasChildPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the HasChildPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HasChildPage = /** @class */ (function () {
    function HasChildPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HasChildPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HasChildPage');
    };
    HasChildPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-has-child',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/has-child/has-child.html"*/'<!--\n  Generated template for the HasChildPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>育儿中</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div>\n    <ion-list>\n      <ion-label class="margin-left16">请输入您的宝宝的信息</ion-label>\n      <ion-item>\n        <ion-label stacked>宝宝乳名</ion-label>\n        <ion-input type="text" placeholder="宝宝乳名"></ion-input>\n      </ion-item>\n\n      <ion-item>\n        <ion-label stacked>宝宝生日</ion-label>\n        <ion-datetime displayFormat="YYYY-MM-DD" cancelText="取消" doneText="确定" placeholder="2018-10-01" [(ngModel)]="myDate"></ion-datetime>\n      </ion-item>\n\n      <ion-label stacked class=\'margin-left16\'>宝宝性别</ion-label>\n      <div class="sex-choice-box">\n        <button ion-fab class=\'button disable-hover\'>男宝宝</button>\n        <button ion-fab class=\'button\'>女宝宝</button>\n      </div>\n    </ion-list>\n    <div class="confirm-center-btn">\n      <button class="confirm-btn" ion-button round>完成</button>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/has-child/has-child.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], HasChildPage);
    return HasChildPage;
}());

//# sourceMappingURL=has-child.js.map

/***/ }),

/***/ 241:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GestationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common_http__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the GestationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var GestationPage = /** @class */ (function () {
    function GestationPage(navCtrl, navParams, http) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.http = http;
        this.isShow = false;
    }
    GestationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad GestationPage');
    };
    GestationPage.prototype.changeShow = function () {
        this.isShow = true;
    };
    GestationPage.prototype.complateInp = function () {
        console.log("完成");
        // this.http.get("api/getArticles").subscribe( res => {
        //   console.log(res);
        // }, err => {
        //   console.log("err: ", err)
        // })
        this.http.post("api/addArticle", {
            title: 'angular4测试',
            content: "angular4测试内容",
        }).subscribe(function (res) {
            console.log(res);
        }, function (err) {
            console.log("err: ", err);
        });
    };
    GestationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-gestation',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/gestation/gestation.html"*/'<!--\n  Generated template for the GestationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>孕育中</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div>\n    <ion-list>\n      <ion-label class="margin-left16">请输入您的预产期</ion-label>\n      \n\n      <ion-item>\n        <ion-label stacked>输入产检时医生告知的预产期</ion-label>\n        <ion-datetime displayFormat="YYYY-MM-DD" max=\'2100-01-01\' cancelText="取消" doneText="确定" placeholder="2018-10-01" [(ngModel)]="myDate"></ion-datetime>\n      </ion-item>\n\n      <ion-item *ngIf="isShow"> \n        <ion-label stacked>月经周期（天）</ion-label>\n        <ion-input type="text" placeholder="28"></ion-input>\n      </ion-item>\n      <ion-label stacked></ion-label>\n      <!-- <ion-label stacked class="margin-left16">当前孕{{}}周{{}}天</ion-label> -->\n    </ion-list>\n    \n    <div class="confirm-center-btn">\n      <div class="green-color" (click)="changeShow()" *ngIf="!isShow">\n        不知道，计算孕期\n      </div>\n      <button class="confirm-btn" ion-button round (click)="complateInp()">完成</button>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/gestation/gestation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1__angular_common_http__["a" /* HttpClient */]])
    ], GestationPage);
    return GestationPage;
}());

//# sourceMappingURL=gestation.js.map

/***/ }),

/***/ 242:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HasGestationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__physical_data_physical_data__ = __webpack_require__(243);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the HasGestationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var HasGestationPage = /** @class */ (function () {
    function HasGestationPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    HasGestationPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad HasGestationPage');
    };
    HasGestationPage.prototype.complateInp = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__physical_data_physical_data__["a" /* PhysicalDataPage */]);
    };
    HasGestationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-has-gestation',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/has-gestation/has-gestation.html"*/'<!--\n  Generated template for the HasGestationPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>hasGestation</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div>\n    <ion-list>\n      <ion-label class="margin-left16">设置月经周期</ion-label>\n      <ion-label class="margin-left16" stacked>请填写以下信息，方便科学备孕</ion-label>\n      \n\n      <ion-item>\n        <ion-label stacked>平均周期天数（天）</ion-label>\n        <ion-input type="text" placeholder="平均周期天数（天）"></ion-input>\n      </ion-item>\n\n      <ion-item> \n        <ion-label stacked>平均月经天数（天）</ion-label>\n        <ion-input type="text" placeholder="平均月经天数（天）"></ion-input>\n      </ion-item>\n      <ion-label stacked></ion-label>\n    </ion-list>\n    \n    <div class="confirm-center-btn">\n      <button class="confirm-btn" ion-button round (click)="complateInp()">完成</button>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/has-gestation/has-gestation.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], HasGestationPage);
    return HasGestationPage;
}());

//# sourceMappingURL=has-gestation.js.map

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PhysicalDataPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the PhysicalDataPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var PhysicalDataPage = /** @class */ (function () {
    function PhysicalDataPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.choice = 1;
        this.swipe = 0;
        this.offsetX = 0;
        this.moveX = 0;
        this.moveBefore = 0;
        this.unit = 0.65;
        this.move = 0;
        this.numText = 50.0;
        this.YmoveBefore = 0;
        this.Ymove = 0;
        this.moveY = 0;
        this.Yunit = 0.48941;
    }
    PhysicalDataPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad PhysicalDataPage');
        this.rulerLen = -32;
        this.heightNum = 160;
        this.heights = [
            {
                num: 100
            }, {
                num: 110
            }, {
                num: 120
            }, {
                num: 130
            }, {
                num: 140
            }, {
                num: 150
            }, {
                num: 160
            }, {
                num: 170
            }, {
                num: 180
            }, {
                num: 190
            }, {
                num: 200
            }
        ];
        this.rulers = [{
                num: 10
            }, {
                num: 20
            }, {
                num: 30
            }, {
                num: 40
            }, {
                num: 50
            }, {
                num: 60
            }, {
                num: 70
            }, {
                num: 80
            }, {
                num: 90
            }, {
                num: 100
            }
        ];
    };
    PhysicalDataPage.prototype.heighTouchstart = function (e) {
        this.offsetY = e.touches[0].clientY;
        this.YmoveBefore = 0;
        // console.log(e);
    };
    PhysicalDataPage.prototype.heightTouchmove = function (e) {
        // console.log(e);
        var ruler = document.getElementById('height-ruler');
        this.Ymove = e.touches[0].clientY;
        // console.log(ruler.dataset.offset);
        var offset = ruler.dataset.offset;
        offset = parseFloat(offset);
        var tempMove = 0;
        var len = 0;
        tempMove = this.Ymove - this.offsetY;
        tempMove /= 10;
        // console.log(tempMove);
        // //计算两次滑动间的距离
        len = offset + (tempMove - this.YmoveBefore);
        len = parseFloat(len);
        console.log(len);
        // //边界判断，最大偏移长度65rem
        if (len - 0.0 < 30 && len > -25) {
            //将结果保存下来，下一次滑动时取出参与计算
            this.moveY = tempMove;
            ruler.dataset.offset = len;
            this.YmoveBefore = this.moveY;
            //设置样式
            this.rulerHeight = len;
            // console.log(len);
            // //显示刻度，保留1位小数
            this.heightNum = 160 - Number((len / this.Yunit).toFixed(1));
        }
    };
    PhysicalDataPage.prototype.touchstart = function (e) {
        // console.log(e);
        this.offsetX = e.touches[0].clientX;
        this.moveBefore = 0;
    };
    PhysicalDataPage.prototype.touchmove = function (e) {
        // console.log(e)
        var ruler = document.getElementById('ruler');
        this.move = e.touches[0].clientX;
        var offset = ruler.dataset.offset;
        offset = parseFloat(offset);
        var tempMove = 0;
        var len = 0;
        len = offset + (tempMove - this.moveBefore);
        len = parseFloat(len);
        tempMove = this.move - this.offsetX;
        tempMove /= 10;
        //计算两次滑动间的距离
        len = offset + (tempMove - this.moveBefore);
        len = parseFloat(len);
        //边界判断，最大偏移长度65rem
        if (len - 0.0 < 0 && len > -65) {
            //将结果保存下来，下一次滑动时取出参与计算
            this.moveX = tempMove;
            ruler.dataset.offset = len;
            this.moveBefore = this.moveX;
            //设置样式
            this.rulerLen = len;
            // //显示刻度，保留1位小数
            this.numText = -((len / this.unit).toFixed(1));
        }
    };
    PhysicalDataPage.prototype.nextStep = function () {
        this.choice = 2;
    };
    PhysicalDataPage.prototype.confirm = function () {
        var data = {
            height: this.heightNum,
            weight: this.numText
        };
        console.log(data);
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
    };
    PhysicalDataPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-physical-data',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/physical-data/physical-data.html"*/'<!--\n  Generated template for the PhysicalDataPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>孕前数据</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="content-box">\n    <div *ngIf=\'choice==1\' class="flex-page-row">\n      <div class="first-box">\n        身高\n      </div>\n      <div class="second-box">\n        <div class="girl-box">\n          <img src="../../assets/imgs/girl.png" alt="">\n        </div>\n        <div class="chi-box">\n            <div class="height-num">{{heightNum}}</div>\n            <div class="height-container">\n                <div class="height-triangle"></div>\n                <div id="height-ruler" data-offset="0" (touchstart)="heighTouchstart($event)" [ngStyle]="{\'transform\': \'translateY(\' + rulerHeight + \'rem)\'}">\n                    <ul class="height-ruler-ul"  (touchmove)="heightTouchmove($event)">\n                        <li *ngFor=\'let item of heights\'>\n                            <span>{{item.num}}</span>\n                        </li>\n                    </ul>\n                </div>\n            </div>\n        </div>\n      </div>\n      <div class="third-box">\n          <button ion-button (click)=\'nextStep()\'>下一步</button>\n      </div>\n    </div>\n    <div *ngIf=\'choice==2\' class="flex-page-row">\n      <div class="first-box">\n        体重\n      </div>\n      <div class="second-weight-box">\n        <div class="girl-box">\n          <img src="../../assets/imgs/girl.png" alt="">\n        </div>\n        <div class="chi-box">\n          <span>\n            <span id="num">{{numText}}</span>\n          </span>\n          <div id="ruler-container">\n              <div id="triangle"></div>\n              <div id="ruler" data-offset="-32.5" (touchstart)="touchstart($event)" [ngStyle]="{\'transform\': \'translateX(\' + rulerLen + \'rem)\'}">\n                  <ul id="ruler-ul"  (touchmove)="touchmove($event)">\n                      <li *ngFor=\'let item of rulers\'>\n                          <span>{{item.num}}</span>\n                      </li>\n                  </ul>\n              </div>\n          </div>\n        </div>\n      </div>\n      <div class="third-box">\n          <button ion-button (click)=\'confirm()\'>保存</button>\n      </div>\n    </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/physical-data/physical-data.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], PhysicalDataPage);
    return PhysicalDataPage;
}());

//# sourceMappingURL=physical-data.js.map

/***/ }),

/***/ 244:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterInfoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__forget_pwd_forget_pwd__ = __webpack_require__(133);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




/**
 * Generated class for the RegisterInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterInfoPage = /** @class */ (function () {
    function RegisterInfoPage(navCtrl, navParams, alertCtrl) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertCtrl = alertCtrl;
        this.codeNum = "";
        this.codeString = "重新获取";
        this.showPwd = false;
    }
    RegisterInfoPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad RegisterInfoPage');
    };
    RegisterInfoPage.prototype.getCode = function () {
        var _this = this;
        console.log("获取验证码");
        this.codeString = "重新获取";
        this.codeNum = 60;
        var timer = setInterval(function () {
            if (_this.codeNum <= 0) {
                _this.codeNum = 0;
                clearInterval(timer);
            }
            else {
                _this.codeNum--;
            }
            console.log(_this.codeNum);
        }, 1000);
    };
    RegisterInfoPage.prototype.toLogin = function () {
        console.log("toLogin");
        this.navCtrl.pop();
    };
    RegisterInfoPage.prototype.toForgetPwd = function () {
        console.log("toForgetPwd");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__forget_pwd_forget_pwd__["a" /* ForgetPwdPage */]);
    };
    RegisterInfoPage.prototype.checkCode = function (event) {
        console.log(event);
        if (event.length > 0) {
            this.showPwd = true;
        }
    };
    RegisterInfoPage.prototype.showAlert = function () {
        var alert = this.alertCtrl.create({
            title: '温馨提示',
            subTitle: "\u5728\u60A8\u7684\u4F7F\u7528\u8FC7\u7A0B\u4E2D\uFF0C\u6211\u4EEC\u5C06\u4F1A\u4E3A\u60A8\u53CA\u65F6\u66F4\u65B0APP\u4FE1\u606F\uFF0C\u4E3A\u8BA9\u60A8\u83B7\u5F97\u5B8C\u6574\u4F53\u9A8C\uFF0C\u9700\u8981\u5F00\u542F\u4EE5\u4E0B\u6743\u9650",
            message: "\u5141\u8BB8\u4FE1\u606F\u901A\u77E5\u63A8\u9001",
            buttons: ['我知道了']
        });
        alert.present();
    };
    RegisterInfoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register-info',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/register-info/register-info.html"*/'<!--\n  Generated template for the RegisterInfoPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>注册</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="flex-page login-page">\n    <div class="login-page-box">\n      <img src=\'../../assets/imgs/logo.png\' />\n      <span>app名字</span>\n    </div>\n    <div class="ion-input-box margin-top-40">\n      <input type="text" placeholder="用户名" [ngModel]=\'username\'>\n    </div>\n    <div class="ion-input-box pos-relative margin-top-40">\n      <input type="text" placeholder="请输入验证码" [ngModel]=\'code\' (ngModelChange)=\'checkCode($event)\'>\n      <span class="pos-code" (click)="getCode()"><span *ngIf=\'codeNum > 0 && codeNum <= 60\'> ({{codeNum}}S) </span>{{codeString}}</span>\n    </div>\n    <div class="ion-input-box" *ngIf=\'showPwd\'>\n      <input type="password" placeholder="密码" [ngModel]=\'password\'>\n    </div>\n    <div class="ion-input-box">\n      <button ion-button (click)=\'showAlert()\'>注册</button>\n    </div>\n    <div class="page-footer-register">\n      <div (click)="toLogin()">用户登录</div>\n      <div>|</div>\n      <div (click)="toForgetPwd()">忘记密码</div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/register-info/register-info.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["AlertController"]])
    ], RegisterInfoPage);
    return RegisterInfoPage;
}());

//# sourceMappingURL=register-info.js.map

/***/ }),

/***/ 245:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegisterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__gestation_gestation__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__has_child_has_child__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__has_gestation_has_gestation__ = __webpack_require__(242);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the RegisterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var RegisterPage = /** @class */ (function () {
    function RegisterPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    RegisterPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                text: '备孕',
                hide: true
            }, {
                text: '孕育中',
                hide: true
            }, {
                text: '有宝宝',
                hide: true
            }
        ];
        console.log('ionViewDidLoad RegisterPage');
    };
    RegisterPage.prototype.toNextPage = function (i) {
        for (var item = 0; item < this.listData.length; item++) {
            this.listData[item].hide = true;
        }
        this.listData[i].hide = false;
        switch (i) {
            case 0:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__gestation_gestation__["a" /* GestationPage */]);
                break;
            case 1:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__has_child_has_child__["a" /* HasChildPage */]);
                break;
            case 2:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__has_gestation_has_gestation__["a" /* HasGestationPage */]);
                break;
        }
    };
    RegisterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-register',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/register/register.html"*/'<!--\n  Generated template for the RegisterPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>注册</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="flex-box register-page">\n    <div class="box-title">\n      请选择人生阶段\n    </div>\n    <div class="register-page-box" *ngFor=\'let item of listData, let i = index\' (click)="toNextPage(i)">\n      {{item.text}}\n      <div class="pos-icon-gou" [ngClass]=\'{"hide": item.hide}\'>\n        <img src="../../assets/imgs/icon-gou.png" alt="">\n      </div>\n    </div>\n    <div class="remark-footer">\n      提醒：孕产APP使用时间为备孕到宝宝1岁\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/register/register.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], RegisterPage);
    return RegisterPage;
}());

//# sourceMappingURL=register.js.map

/***/ }),

/***/ 256:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 256;

/***/ }),

/***/ 299:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/content/activity-detail/activity-detail.module": [
		810,
		35
	],
	"../pages/content/birth-child-info/birth-child-info.module": [
		811,
		34
	],
	"../pages/content/change-baby-height/change-baby-height.module": [
		813,
		33
	],
	"../pages/content/contact-search/contact-search.module": [
		812,
		32
	],
	"../pages/content/diet/diet.module": [
		815,
		31
	],
	"../pages/content/early-pregnancy/early-pregnancy.module": [
		814,
		30
	],
	"../pages/content/exchange-points/exchange-points.module": [
		816,
		29
	],
	"../pages/content/faq-detail/faq-detail.module": [
		817,
		28
	],
	"../pages/content/faq/faq.module": [
		818,
		27
	],
	"../pages/content/feeding/feeding.module": [
		819,
		26
	],
	"../pages/content/fetal-movement-record/fetal-movement-record.module": [
		820,
		25
	],
	"../pages/content/fetal-movement/fetal-movement.module": [
		821,
		24
	],
	"../pages/content/gadget/gadget.module": [
		822,
		23
	],
	"../pages/content/knowledge-base/knowledge-base.module": [
		823,
		22
	],
	"../pages/content/knowledge-detail/knowledge-detail.module": [
		824,
		21
	],
	"../pages/content/knowledges/knowledges.module": [
		825,
		20
	],
	"../pages/content/news/news.module": [
		826,
		19
	],
	"../pages/content/post-detail/post-detail.module": [
		827,
		18
	],
	"../pages/content/posting/posting.module": [
		828,
		17
	],
	"../pages/content/posts/posts.module": [
		829,
		16
	],
	"../pages/content/prenatal-education/prenatal-education.module": [
		830,
		15
	],
	"../pages/content/recipes-oneday/recipes-oneday.module": [
		831,
		14
	],
	"../pages/content/recipes/recipes.module": [
		832,
		13
	],
	"../pages/content/record-diet/record-diet.module": [
		833,
		12
	],
	"../pages/content/success-story-detail/success-story-detail.module": [
		834,
		11
	],
	"../pages/content/success-story/success-story.module": [
		835,
		10
	],
	"../pages/content/weight-curve/weight-curve.module": [
		836,
		9
	],
	"../pages/forget-pwd/forget-pwd.module": [
		837,
		8
	],
	"../pages/gestation/gestation.module": [
		839,
		7
	],
	"../pages/has-child/has-child.module": [
		838,
		6
	],
	"../pages/has-gestation/has-gestation.module": [
		841,
		5
	],
	"../pages/index/index.module": [
		840,
		4
	],
	"../pages/login/login.module": [
		842,
		3
	],
	"../pages/physical-data/physical-data.module": [
		843,
		2
	],
	"../pages/register-info/register-info.module": [
		844,
		1
	],
	"../pages/register/register.module": [
		845,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 299;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 380:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__content_knowledges_knowledges__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__content_early_pregnancy_early_pregnancy__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__content_recipes_recipes__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__content_faq_faq__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__content_gadget_gadget__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__content_birth_child_info_birth_child_info__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__content_change_baby_height_change_baby_height__ = __webpack_require__(218);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var HomePage = /** @class */ (function () {
    function HomePage(navCtrl) {
        this.navCtrl = navCtrl;
        this.pushModal = true;
        this.hasBaby = true;
        this.barId = 1;
        this.barContent = '知识库新增孕期GDM模块了！';
        this.btnText = '查看';
    }
    HomePage.prototype.ngOnInit = function () {
        this.listData = [
            {
                icon: '../../../assets/imgs/home-icon1.png',
                title: '知识库',
                text: '孕育知识科普讲解'
            }, {
                icon: '../../../assets/imgs/home-icon2.png',
                title: '营养食谱',
                text: '孕育知识科普讲解'
            }, {
                icon: '../../../assets/imgs/home-icon3.png',
                title: '小工具',
                text: '孕育知识科普讲解'
            }, {
                icon: '../../../assets/imgs/home-icon4.png',
                title: '问与答',
                text: '孕育知识科普讲解'
            }
        ];
        this.cardData = [
            {
                title: '孕早期知识',
                text: '孕早期有哪些注意事项',
                img: '../../../assets/imgs/home-card-img.png'
            }
        ];
    };
    HomePage.prototype.toKnowledgePage = function (i) {
        console.log(i);
        switch (i) {
            case 0:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_knowledges_knowledges__["a" /* KnowledgesPage */]);
                break;
            case 1:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__content_recipes_recipes__["a" /* RecipesPage */]);
                break;
            case 2:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__content_gadget_gadget__["a" /* GadgetPage */]);
                break;
            case 3:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__content_faq_faq__["a" /* FaqPage */]);
                break;
            default:
                break;
        }
    };
    HomePage.prototype.toYunZao = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__content_early_pregnancy_early_pregnancy__["a" /* EarlyPregnancyPage */]);
    };
    HomePage.prototype.toWenjuan = function (id) {
        console.log(id);
        if (id == 1) {
            // this.hasBaby = true;
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__content_birth_child_info_birth_child_info__["a" /* BirthChildInfoPage */]);
        }
    };
    HomePage.prototype.closeModal = function () {
        this.pushModal = false;
    };
    HomePage.prototype.toWriteHeight = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__content_change_baby_height_change_baby_height__["a" /* ChangeBabyHeightPage */]);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Home</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="normal-page">\n    <div class="show-img-box">\n      <img class=\'home-img\' src="../../../assets/imgs/home-img.png" alt="">\n      <div class="pos-camare">\n        <img src="../../../assets/imgs/home-camera.png" alt="">\n      </div>\n    </div>\n    <div class=\'center-content\' *ngIf=\'!hasBaby\'>\n      <div class="center-item">\n        孕8周+2天\n      </div>\n      <div class="pos-center">孕育中</div>\n      <div class="center-item">\n        距离预产期222天\n      </div>\n      <div class="home-tip">\n        <div class="pos-tip-text">\n          <span>{{barContent}}</span>\n          <span class="change-btn" (click)=\'toWenjuan(barId)\'>{{btnText}}</span>\n        </div>\n      </div>\n    </div>\n\n    <div class=\'center-content-hasBaby\' *ngIf=\'hasBaby\'>\n      <div class="pos-center">一个月</div>\n      <div class="pos-write" (click)=\'toWriteHeight()\'>\n        <img src="../../../assets/imgs/home-write-icon.png" alt="">\n      </div>\n      <div class="line">\n        <img src="../../../assets/imgs/red-line.png" alt="">\n      </div>\n      <div class=\'circle-box\'>\n        <div class="font-color4">45</div>\n        <div class="font-color1">cm</div>\n      </div>\n      <div class="center-line"><img src="../../../assets/imgs/red-line.png" alt=""></div>\n      <div class="circle-box">\n        <div class="font-color4">12</div>\n        <div class="font-color1">kg</div>\n      </div>\n      <div  class="line"><img src="../../../assets/imgs/red-line.png" alt=""></div>\n    </div>\n\n    <ul class=\'mine-list\'>\n      <li class=\'mine-list-item\' *ngFor=\'let item of listData, let i = index\' (click)=\'toKnowledgePage(i)\'>\n        <div class="item-icon" [ngClass]="{\'bg1\': i==0, \'bg2\': i==1, \'bg3\': i==2, \'bg4\': i==3}">\n          <img src="{{item.icon}}" alt="">\n        </div>\n        <div class="item-text">\n          {{item.title}}\n        </div>\n      </li>\n    </ul>\n    <div class="home-card" *ngFor=\'let item of cardData\'>\n      <div class="card-title" (click)=\'toYunZao()\'>\n        <div class="card-title-text">{{item.title}}</div>\n        <div>\n          <img src="../../../assets/imgs/home-right.png" alt="">\n        </div>\n      </div>\n      <div class="card-img">\n        <div class="pos-card-img-text"><span>{{item.text}}</span></div>\n        <div class="cimg">\n          <img src="{{item.img}}" alt="">\n        </div>\n      </div>\n    </div>\n    <div class="home-card" *ngFor=\'let item of cardData\'>\n      <div class="card-title">\n        <div class="card-title-text">{{item.title}}</div>\n        <div>\n          <img src="../../../assets/imgs/home-right.png" alt="">\n        </div>\n      </div>\n      <div class="card-img">\n        <div class="pos-card-img-text2" [ngStyle]="{\'text-align\': hasBaby ? \'center\' : \'right\'}">\n          <div>{{item.text}}</div>\n          <div>{{item.text}}</div>\n          <div>\n            <button ion-button>去记录</button>\n          </div>\n        </div>\n        <div class="cimg">\n          <img src="{{item.img}}" alt="">\n        </div>\n      </div>\n    </div>\n\n    <div class="pos-modal" *ngIf=\'pushModal\'>\n      <div class="pos-dialog">\n        <div class="pos-content">\n          <div class="modal-header">\n            <img src="../../../assets/imgs/modal-icon-left.png" alt="">\n            <span class="font-color1">GDM高危测试</span>\n            <img src="../../../assets/imgs/modal-icon-right.png" alt="">\n          </div>\n          <div class="modal-center font-color4">\n            判断是否患有妊娠期糖尿病最佳方法是进行产检项目——糖筛，一般在妊娠24-38周进行。\n          </div>\n          <div class="modal-footer">\n            <button ion-button>去测试</button>\n          </div>\n        </div>\n        <div class="modal-close" (click)=\'closeModal()\'>\n          <img src="../../../assets/imgs/modal-close.png" alt="">\n        </div>\n      </div>\n    </div>\n\n    <!-- <div class="pos-modal" *ngIf=\'yunqiPush\'>\n      <div class="pos-dialog">\n        <div class="pos-content">\n          <div class="modal-header">\n            <img src="../../../assets/imgs/modal-icon-left.png" alt="">\n            <span class="font-color1">孕期推送</span>\n            <img src="../../../assets/imgs/modal-icon-right.png" alt="">\n          </div>\n          <div class="modal-center font-color4">\n              孕妇在有身是期一般注意不要疲劳，行动要舒缓，要避免不协调的动作，避免冲撞，颠簸行为。随着孕龄的增加，孕妇的体重和腹部负担逐渐加剧，膨隆的子宫榨取着四周的脏器和血管。\n          </div>\n          <div class="modal-footer font-color1">\n            <span>我知道了</span>\n          </div>\n        </div>\n      </div>\n    </div> -->\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 381:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConcatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__content_contact_search_contact_search__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__content_success_story_success_story__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__content_posts_posts__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__content_posting_posting__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__content_activity_detail_activity_detail__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ConcatPage = /** @class */ (function () {
    function ConcatPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.keyword = '';
        this.placeholder = '孕早期妈妈都在做什么......';
    }
    ConcatPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                url: '../../../assets/imgs/demo.png',
                text: '功能'
            }, {
                url: '../../../assets/imgs/demo.png',
                text: '功能'
            }, {
                url: '../../../assets/imgs/demo.png',
                text: '功能'
            }
        ];
        this.retieList = [
            {
                url: '../../../assets/imgs/demo.png'
            }, {
                url: '../../../assets/imgs/demo.png'
            }
        ];
        this.cardData = [
            {
                id: 1,
                title: '孕育交流',
                text: '产检—与宝宝交流的奇妙时光',
                img: '../../../assets/imgs/concat-img1.png'
            }, {
                id: 2,
                title: '成功故事分享',
                text: '我的孕育时光',
                img: '../../../assets/imgs/concat-img2.png'
            }
        ];
    };
    ConcatPage.prototype.toExchange = function () {
        console.log('兑换');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__content_activity_detail_activity_detail__["a" /* ActivityDetailPage */]);
    };
    ConcatPage.prototype.focusInput = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_contact_search_contact_search__["a" /* ContactSearchPage */]);
    };
    ConcatPage.prototype.toYunZao = function (id) {
        if (id == 1) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__content_posts_posts__["a" /* PostsPage */]);
        }
        else {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__content_success_story_success_story__["a" /* SuccessStoryPage */]);
        }
    };
    ConcatPage.prototype.toPointing = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__content_posting_posting__["a" /* PostingPage */]);
    };
    ConcatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-concat',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/concat/concat.html"*/'<ion-header>\n    <ion-navbar>\n        <ion-title>社交</ion-title>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n    <div class="normal-page">\n        <div class="pos-point" (click)=\'toPointing()\'>\n            发布\n        </div>\n        <div class="top-box">\n            <div class=\'pos-concat-bg\'>\n                <img  src="../../../assets/imgs/concat-bg.png" alt="">\n            </div>\n            <div class="top-bar">\n                <input type="text" placeholder="请输入关键字" (focus)="focusInput()" [(ngModel)]="keyword">\n                <ion-icon class="search" name="search"></ion-icon>\n            </div>\n            <div class="flex-top-box">\n                <div>记录孕期生活换积分</div>\n                <div>线下妇幼专家面对面咨询</div>\n                <div>\n                    <button ion-button (click)=\'toExchange()\'>去兑换</button>\n                </div>\n            </div>\n        </div>\n        <div class="home-card" *ngFor=\'let item of cardData\'>\n            <div class="card-title" (click)=\'toYunZao(item.id)\'>\n                <div class="card-title-text">{{item.title}}</div>\n                <div>\n                    <img src="../../../assets/imgs/home-right.png" alt="">\n                </div>\n            </div>\n            <div class="card-img">\n                <div class="pos-card-img-text"><span>{{item.text}}</span></div>\n                <div class="cimg">\n                    <img src="{{item.img}}" alt="">\n                </div>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/concat/concat.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], ConcatPage);
    return ConcatPage;
}());

//# sourceMappingURL=concat.js.map

/***/ }),

/***/ 382:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MinePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__content_feeding_feeding__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__content_exchange_points_exchange_points__ = __webpack_require__(222);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var MinePage = /** @class */ (function () {
    function MinePage(navCtrl) {
        this.navCtrl = navCtrl;
    }
    MinePage.prototype.ngOnInit = function () {
        console.log('ngOnInit');
        this.listData = [
            {
                id: 1,
                icon: '../../../assets/imgs/mine-icon1.png',
                text: '人生阶段'
            }, {
                id: 2,
                icon: '../../../assets/imgs/mine-icon2.png',
                text: '喂养方式'
            }, {
                id: 3,
                icon: '../../../assets/imgs/mine-icon3.png',
                text: '足迹记录'
            }, {
                id: 4,
                icon: '../../../assets/imgs/mine-icon4.png',
                text: '消息通知'
            }, {
                id: 5,
                icon: '../../../assets/imgs/mine-icon5.png',
                text: '意见反馈'
            }
        ];
    };
    MinePage.prototype.toPage = function (id) {
        switch (id) {
            case 1:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_feeding_feeding__["a" /* FeedingPage */]);
                break;
            case 2:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__content_exchange_points_exchange_points__["a" /* ExchangePointsPage */]);
                break;
            case 3:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_feeding_feeding__["a" /* FeedingPage */]);
                break;
            case 4:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_feeding_feeding__["a" /* FeedingPage */]);
                break;
            case 5:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_feeding_feeding__["a" /* FeedingPage */]);
                break;
            default:
                break;
        }
    };
    MinePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-mine',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/mine/mine.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>mine</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="flex-box mine-flex-page">\n    <div class="top-box">\n      <div class="top-header">\n        <div class="top-items">BMI值</div>\n        <div class="top-items">\n          <img class=\'bg-img\' src="../../../assets/imgs/demo.png" alt="">\n        </div>\n        <div class="top-items">糖尿病标识</div>\n      </div>\n      <div class="top-center">用户昵称</div>\n      <div class="top-footer">距宝宝出生22天</div>\n      <div class="pos-bg-img">\n        <img src="../../../assets/imgs/mine-bg-img.png" alt="">\n      </div>\n    </div>\n    <ul class=\'content-list\'>\n      <li class=\'first-item\' padding>\n        <img src="../../../assets/imgs/mine-yunfu.png" alt="">\n        <span>孕育阶段</span>\n        <span>孕37周+3天</span>\n        <div class=\'right\'>\n            <img src="../../../assets/imgs/right-icon.png" alt="">\n        </div>\n      </li>\n      <li *ngFor=\'let item of listData\' class=\'content-item\' (click)=\'toPage(item.id)\'>\n        <div class="first-icon">\n          <img src="{{item.icon}}" alt="">\n        </div>\n        <div class="text-content">{{item.text}}</div>\n      </li>\n    </ul>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/mine/mine.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], MinePage);
    return MinePage;
}());

//# sourceMappingURL=mine.js.map

/***/ }),

/***/ 383:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RecordPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__content_diet_diet__ = __webpack_require__(220);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var RecordPage = /** @class */ (function () {
    function RecordPage(navCtrl) {
        this.navCtrl = navCtrl;
        this.keyword = '';
        this.placeholder = '孕早期妈妈都在做什么......';
    }
    RecordPage.prototype.ngOnInit = function () {
        console.log('record 记录');
        this.listData = [
            {
                url: '../../../assets/imgs/record-icon1.png',
                title: '体重',
                text: '记录体重，及时关注身体…'
            }, {
                url: '../../../assets/imgs/record-icon2.png',
                title: '膳食',
                text: '记录体重，及时关注身体…'
            }, {
                url: '../../../assets/imgs/record-icon3.png',
                title: '运动',
                text: '记录体重，及时关注身体…',
            }, {
                url: '../../../assets/imgs/record-icon4.png',
                title: '事记',
                text: '记录体重，及时关注身体…',
            }
        ];
    };
    RecordPage.prototype.toPage = function (i) {
        console.log(i);
        switch (i) {
            case 0:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_diet_diet__["a" /* DietPage */]);
                break;
            case 1:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_diet_diet__["a" /* DietPage */]);
                break;
            case 2:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_diet_diet__["a" /* DietPage */]);
                break;
            case 3:
                this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__content_diet_diet__["a" /* DietPage */]);
                break;
            default:
                break;
        }
    };
    RecordPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-record',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/record/record.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      记录\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="normal-page">\n    <div class="show-img-box">\n      <img src="../../../assets/imgs/record-img.png" alt="">\n    </div>\n    <ul class=\'concat-list\'>\n      <li class=\'concat-list-item\' *ngFor=\'let item of listData, let i = index\' (click)=\'toPage(i)\'>\n        <img src="{{item.url}}" alt="">\n        <div>\n          {{item.title}}\n        </div>\n        <div class="item-text">\n          {{item.text}}\n        </div>\n      </li>\n    </ul>\n    <div class="content"></div>\n  </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/tabs/record/record.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"]])
    ], RecordPage);
    return RecordPage;
}());

//# sourceMappingURL=record.js.map

/***/ }),

/***/ 429:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NewsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var NewsPage = /** @class */ (function () {
    function NewsPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    NewsPage.prototype.ngOnInit = function () {
        this.listData = [
            {
                id: 1,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }, {
                id: 2,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }, {
                id: 3,
                title: '孕早期有哪些注意事项呢？',
                body: '孕期营养胎儿生长所需营养都来自于孕妇。孕妇必须从食物中获取足够的营养，以满足自身的营养和胎儿生长发育的需要......'
            }
        ];
    };
    NewsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-news',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/news/news.html"*/'<ion-header>\n\n    <ion-navbar>\n        <ion-title>消息通知</ion-title>\n    </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n    <div class="flex-box mine-flex-page">\n        <div class=\'first-item\' padding>\n            <div class=\'left\'>\n                <div class="left-first">\n                    <img src="../../../assets/imgs/mine-icon2.png" alt="">\n                    <span class="font-color1">500积分</span>\n                </div>\n                <div class="left-second">\n                    <span>可兑换线下专家面对面咨询</span>\n                </div>\n            </div>\n            <div class=\'right\'>\n                <button ion-button>兑换</button>\n            </div>\n        </div>\n        <div class="content-box">\n            <div class="guize">兑换规则:</div>\n            <div class="list">\n                <div class="pos-line">\n                    <img src="../../../assets/imgs/faq-line.png" alt="">\n                </div>\n                <div class="top-img">\n                    <div class="circle-icon">\n                        <img src="../../../assets/imgs/circle.png" alt="">\n                        <span>记录孕期点滴，每记录一次获取10积分；</span>\n                    </div>\n                </div>\n\n                <div class="top-img">\n                    <div class="circle-icon">\n                        <img src="../../../assets/imgs/circle.png" alt="">\n                        <span>100积分可兑换一次专家线下面对面咨询，详情可查看社交圈活动；</span>\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</ion-content>'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/content/news/news.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], NewsPage);
    return NewsPage;
}());

//# sourceMappingURL=news.js.map

/***/ }),

/***/ 430:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return IndexPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the IndexPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var IndexPage = /** @class */ (function () {
    function IndexPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    IndexPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad IndexPage');
    };
    IndexPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-index',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/index/index.html"*/'<!--\n  Generated template for the IndexPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>indexPage</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="flex-page">\n    \n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/index/index.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], IndexPage);
    return IndexPage;
}());

//# sourceMappingURL=index.js.map

/***/ }),

/***/ 431:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__register_info_register_info__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__forget_pwd_forget_pwd__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__register_register__ = __webpack_require__(245);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var LoginPage = /** @class */ (function () {
    function LoginPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad LoginPage');
    };
    LoginPage.prototype.toRegister = function () {
        console.log("toRegister");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__register_info_register_info__["a" /* RegisterInfoPage */]);
    };
    LoginPage.prototype.toForgetPwd = function () {
        console.log("toForgetPwd");
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__forget_pwd_forget_pwd__["a" /* ForgetPwdPage */]);
    };
    LoginPage.prototype.login = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__register_register__["a" /* RegisterPage */]);
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/login/login.html"*/'<!--\n  Generated template for the LoginPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n\n  <ion-navbar>\n    <ion-title>login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <div class="flex-page login-page">\n      <div class="login-page-box">\n        <img src=\'../../assets/imgs/logo.png\' />\n        <span>app名字</span>\n      </div>\n      <div class="ion-input-box margin-top-40">\n        <input type="text" placeholder="用户名">\n      </div>\n      <div class="ion-input-box"> \n          <input type="password" placeholder="密码">\n      </div>\n      <div class="ion-input-box"> \n        <button ion-button (click)=\'login()\'>登录</button>\n      </div>\n      <div class="page-footer">\n        <div (click)="toRegister()">新用户注册</div>\n        <div>|</div>\n        <div (click)="toForgetPwd()">忘记密码</div>\n      </div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/pages/login/login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavController"], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["NavParams"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 432:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(433);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(453);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 453:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_common_http__ = __webpack_require__(257);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(797);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__ = __webpack_require__(805);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_home_home__ = __webpack_require__(380);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_concat_concat__ = __webpack_require__(381);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_tabs_record_record__ = __webpack_require__(383);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_tabs_mine_mine__ = __webpack_require__(382);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__ = __webpack_require__(192);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_register_register__ = __webpack_require__(245);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_register_info_register_info__ = __webpack_require__(244);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_forget_pwd_forget_pwd__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_login_login__ = __webpack_require__(431);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__pages_index_index__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__pages_has_child_has_child__ = __webpack_require__(240);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_gestation_gestation__ = __webpack_require__(241);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_content_knowledge_base_knowledge_base__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__pages_has_gestation_has_gestation__ = __webpack_require__(242);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__pages_physical_data_physical_data__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__pages_content_early_pregnancy_early_pregnancy__ = __webpack_require__(219);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_content_knowledge_detail_knowledge_detail__ = __webpack_require__(105);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_content_recipes_recipes__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_content_faq_faq__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_content_gadget_gadget__ = __webpack_require__(228);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_content_fetal_movement_fetal_movement__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_content_prenatal_education_prenatal_education__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_content_fetal_movement_record_fetal_movement_record__ = __webpack_require__(226);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_content_recipes_oneday_recipes_oneday__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_content_diet_diet__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_content_record_diet_record_diet__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_content_faq_detail_faq_detail__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_content_birth_child_info_birth_child_info__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_34__pages_content_change_baby_height_change_baby_height__ = __webpack_require__(218);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_35__pages_content_weight_curve_weight_curve__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_36__pages_content_knowledges_knowledges__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_37__pages_content_feeding_feeding__ = __webpack_require__(225);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_38__pages_content_exchange_points_exchange_points__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_39__pages_content_news_news__ = __webpack_require__(429);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_40__pages_content_contact_search_contact_search__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_41__pages_content_success_story_success_story__ = __webpack_require__(239);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_42__pages_content_success_story_detail_success_story_detail__ = __webpack_require__(238);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_43__pages_content_posts_posts__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_44__pages_content_post_detail_post_detail__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_45__pages_content_posting_posting__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_46__pages_content_activity_detail_activity_detail__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_47__pipes_pipes_module__ = __webpack_require__(808);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_48__ionic_native_status_bar__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_49__ionic_native_splash_screen__ = __webpack_require__(426);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






// pages
// 首页

// 社交

// 记录

// 我的

// tabs切换

// 选择人生阶段

// 注册

// 忘记密码

// 登录


// 有孩子

// 备孕

// 知识库首页












//膳食列表页

//  记膳食

// 问与答详情

// 孩子出生填写孩子信息

// 修改孩子升高和体重

// 称体重画曲线页面

// 知识库列表

// 选择喂养方式

// 我的积分

// 消息

// 社交

// 分享成功故事

// 故事详情

// 所有帖子

// 帖子详情

// 发帖子

// 活动详情

/* pipes */



var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_concat_concat__["a" /* ConcatPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_record_record__["a" /* RecordPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_index_index__["a" /* IndexPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_has_child_has_child__["a" /* HasChildPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_gestation_gestation__["a" /* GestationPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_has_gestation_has_gestation__["a" /* HasGestationPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_register_info_register_info__["a" /* RegisterInfoPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_physical_data_physical_data__["a" /* PhysicalDataPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_forget_pwd_forget_pwd__["a" /* ForgetPwdPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_mine_mine__["a" /* MinePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_content_knowledge_base_knowledge_base__["a" /* KnowledgeBasePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_content_early_pregnancy_early_pregnancy__["a" /* EarlyPregnancyPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_content_knowledge_detail_knowledge_detail__["a" /* KnowledgeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_content_recipes_recipes__["a" /* RecipesPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_content_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_content_gadget_gadget__["a" /* GadgetPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_content_fetal_movement_fetal_movement__["a" /* FetalMovementPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_content_prenatal_education_prenatal_education__["a" /* PrenatalEducationPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_content_fetal_movement_record_fetal_movement_record__["a" /* FetalMovementRecordPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_content_recipes_oneday_recipes_oneday__["a" /* RecipesOnedayPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_content_diet_diet__["a" /* DietPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_content_record_diet_record_diet__["a" /* RecordDietPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_content_faq_detail_faq_detail__["a" /* FaqDetailPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_content_birth_child_info_birth_child_info__["a" /* BirthChildInfoPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_content_change_baby_height_change_baby_height__["a" /* ChangeBabyHeightPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_content_weight_curve_weight_curve__["a" /* WeightCurvePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_content_knowledges_knowledges__["a" /* KnowledgesPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_content_feeding_feeding__["a" /* FeedingPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_content_exchange_points_exchange_points__["a" /* ExchangePointsPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_content_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_content_contact_search_contact_search__["a" /* ContactSearchPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_content_success_story_success_story__["a" /* SuccessStoryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_content_success_story_detail_success_story_detail__["a" /* SuccessStoryDetailPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_content_posts_posts__["a" /* PostsPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_content_posting_posting__["a" /* PostingPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_content_post_detail_post_detail__["a" /* PostDetailPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_content_activity_detail_activity_detail__["a" /* ActivityDetailPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_47__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicModule"].forRoot(__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */], {
                    tabsHideOnSubPages: 'true',
                    backButtonText: "返回" /*修改返回按钮为返回（默认是）*/
                }, {
                    links: [
                        { loadChildren: '../pages/content/activity-detail/activity-detail.module#ActivityDetailPageModule', name: 'ActivityDetailPage', segment: 'activity-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/birth-child-info/birth-child-info.module#BirthChildInfoModule', name: 'BirthChildInfoPage', segment: 'birth-child-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/contact-search/contact-search.module#ContactSearchPageModule', name: 'ContactSearchPage', segment: 'contact-search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/change-baby-height/change-baby-height.module#ChangeBabyHeightModule', name: 'ChangeBabyHeightPage', segment: 'change-baby-height', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/early-pregnancy/early-pregnancy.module#KnowledgeBaseModule', name: 'EarlyPregnancyPage', segment: 'early-pregnancy', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/diet/diet.module#DietModule', name: 'DietPage', segment: 'diet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/exchange-points/exchange-points.module#ExchangePointsPageModule', name: 'ExchangePointsPage', segment: 'exchange-points', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/faq-detail/faq-detail.module#FaqDetailModule', name: 'FaqDetailPage', segment: 'faq-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/faq/faq.module#KnowledgeBaseModule', name: 'FaqPage', segment: 'faq', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/feeding/feeding.module#FeedingPageModule', name: 'FeedingPage', segment: 'feeding', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/fetal-movement-record/fetal-movement-record.module#FetalMovementRecordModule', name: 'FetalMovementRecordPage', segment: 'fetal-movement-record', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/fetal-movement/fetal-movement.module#FetalMovementPageModule', name: 'FetalMovementPage', segment: 'fetal-movement', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/gadget/gadget.module#GadgetModule', name: 'GadgetPage', segment: 'gadget', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/knowledge-base/knowledge-base.module#KnowledgeBasePageModule', name: 'KnowledgeBasePage', segment: 'knowledge-base', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/knowledge-detail/knowledge-detail.module#KnowledgeDetailPageModule', name: 'KnowledgeDetailPage', segment: 'knowledge-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/knowledges/knowledges.module#KnowledgesModule', name: 'KnowledgesPage', segment: 'knowledges', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/news/news.module#NewsPageModule', name: 'NewsPage', segment: 'news', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/post-detail/post-detail.module#PostDetailPageModule', name: 'PostDetailPage', segment: 'post-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/posting/posting.module#PostingPageModule', name: 'PostingPage', segment: 'posting', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/posts/posts.module#PostsPageModule', name: 'PostsPage', segment: 'posts', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/prenatal-education/prenatal-education.module#PrenatalEducationModule', name: 'PrenatalEducationPage', segment: 'prenatal-education', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/recipes-oneday/recipes-oneday.module#RecipesOnedayModule', name: 'RecipesOnedayPage', segment: 'recipes-oneday', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/recipes/recipes.module#RecipesModule', name: 'RecipesPage', segment: 'recipes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/record-diet/record-diet.module#RecipesOnedayModule', name: 'RecordDietPage', segment: 'record-diet', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/success-story-detail/success-story-detail.module#SuccessStoryDetailPageModule', name: 'SuccessStoryDetailPage', segment: 'success-story-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/success-story/success-story.module#SuccessStoryPageModule', name: 'SuccessStoryPage', segment: 'success-story', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/weight-curve/weight-curve.module#WeightCurveModule', name: 'WeightCurvePage', segment: 'weight-curve', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/forget-pwd/forget-pwd.module#ForgetPwdPageModule', name: 'ForgetPwdPage', segment: 'forget-pwd', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/has-child/has-child.module#HasChildPageModule', name: 'HasChildPage', segment: 'has-child', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/gestation/gestation.module#GestationPageModule', name: 'GestationPage', segment: 'gestation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/index/index.module#IndexPageModule', name: 'IndexPage', segment: 'index', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/has-gestation/has-gestation.module#HasGestationPageModule', name: 'HasGestationPage', segment: 'has-gestation', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/physical-data/physical-data.module#PhysicalDataPageModule', name: 'PhysicalDataPage', segment: 'physical-data', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register-info/register-info.module#RegisterInfoPageModule', name: 'RegisterInfoPage', segment: 'register-info', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/register/register.module#RegisterPageModule', name: 'RegisterPage', segment: 'register', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_5_ion_multi_picker__["MultiPickerModule"],
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicApp"]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_14__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_tabs_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_concat_concat__["a" /* ConcatPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_tabs_record_record__["a" /* RecordPage */],
                __WEBPACK_IMPORTED_MODULE_15__pages_index_index__["a" /* IndexPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_register_register__["a" /* RegisterPage */],
                __WEBPACK_IMPORTED_MODULE_16__pages_has_child_has_child__["a" /* HasChildPage */],
                __WEBPACK_IMPORTED_MODULE_19__pages_has_gestation_has_gestation__["a" /* HasGestationPage */],
                __WEBPACK_IMPORTED_MODULE_17__pages_gestation_gestation__["a" /* GestationPage */],
                __WEBPACK_IMPORTED_MODULE_12__pages_register_info_register_info__["a" /* RegisterInfoPage */],
                __WEBPACK_IMPORTED_MODULE_20__pages_physical_data_physical_data__["a" /* PhysicalDataPage */],
                __WEBPACK_IMPORTED_MODULE_13__pages_forget_pwd_forget_pwd__["a" /* ForgetPwdPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_tabs_mine_mine__["a" /* MinePage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_content_knowledge_base_knowledge_base__["a" /* KnowledgeBasePage */],
                __WEBPACK_IMPORTED_MODULE_21__pages_content_early_pregnancy_early_pregnancy__["a" /* EarlyPregnancyPage */],
                __WEBPACK_IMPORTED_MODULE_22__pages_content_knowledge_detail_knowledge_detail__["a" /* KnowledgeDetailPage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_content_recipes_recipes__["a" /* RecipesPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_content_faq_faq__["a" /* FaqPage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_content_gadget_gadget__["a" /* GadgetPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_content_fetal_movement_fetal_movement__["a" /* FetalMovementPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_content_prenatal_education_prenatal_education__["a" /* PrenatalEducationPage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_content_fetal_movement_record_fetal_movement_record__["a" /* FetalMovementRecordPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_content_recipes_oneday_recipes_oneday__["a" /* RecipesOnedayPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_content_diet_diet__["a" /* DietPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_content_record_diet_record_diet__["a" /* RecordDietPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_content_faq_detail_faq_detail__["a" /* FaqDetailPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_content_birth_child_info_birth_child_info__["a" /* BirthChildInfoPage */],
                __WEBPACK_IMPORTED_MODULE_34__pages_content_change_baby_height_change_baby_height__["a" /* ChangeBabyHeightPage */],
                __WEBPACK_IMPORTED_MODULE_35__pages_content_weight_curve_weight_curve__["a" /* WeightCurvePage */],
                __WEBPACK_IMPORTED_MODULE_36__pages_content_knowledges_knowledges__["a" /* KnowledgesPage */],
                __WEBPACK_IMPORTED_MODULE_37__pages_content_feeding_feeding__["a" /* FeedingPage */],
                __WEBPACK_IMPORTED_MODULE_38__pages_content_exchange_points_exchange_points__["a" /* ExchangePointsPage */],
                __WEBPACK_IMPORTED_MODULE_39__pages_content_news_news__["a" /* NewsPage */],
                __WEBPACK_IMPORTED_MODULE_40__pages_content_contact_search_contact_search__["a" /* ContactSearchPage */],
                __WEBPACK_IMPORTED_MODULE_41__pages_content_success_story_success_story__["a" /* SuccessStoryPage */],
                __WEBPACK_IMPORTED_MODULE_42__pages_content_success_story_detail_success_story_detail__["a" /* SuccessStoryDetailPage */],
                __WEBPACK_IMPORTED_MODULE_43__pages_content_posts_posts__["a" /* PostsPage */],
                __WEBPACK_IMPORTED_MODULE_45__pages_content_posting_posting__["a" /* PostingPage */],
                __WEBPACK_IMPORTED_MODULE_44__pages_content_post_detail_post_detail__["a" /* PostDetailPage */],
                __WEBPACK_IMPORTED_MODULE_46__pages_content_activity_detail_activity_detail__["a" /* ActivityDetailPage */],
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_48__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_49__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["IonicErrorHandler"] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 797:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(7);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(423);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(426);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(192);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import { RegisterPage } from '../pages/register/register';
// import { PhysicalDataPage } from '../pages/physical-data/physical-data';

// import { LoginPage } from '../pages/login/login';
// import { KnowledgesPage } from '../pages/content/knowledges/knowledges';
// import { PhysicalDataPage } from '../pages/physical-data/physical-data';
// import { FaqPage } from '../pages/content/faq/faq';
// import { RecordPage } from '../pages/tabs/record/record';
// import { RecipesPage } from '../pages/content/recipes/recipes';
// import { FetalMovementPage } from '../pages/content/fetal-movement/fetal-movement'
// import { GadgetPage } from '../pages/content/gadget/gadget';
// import { RecipesPage } from '../pages/content/recipes/recipes';
// import { KnowledgeBasePage } from '../pages/content/knowledge-base/knowledge-base';
// import { RegisterInfoPage } from '../pages/register-info/register-info';
var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen) {
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            statusBar.styleDefault();
            splashScreen.hide();
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({template:/*ion-inline-start:"/Users/mac/Desktop/code/ionic/yingyang/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/mac/Desktop/code/ionic/yingyang/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["Platform"], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 808:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__word_place_word_place__ = __webpack_require__(809);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_1__word_place_word_place__["a" /* WordPlacePipe */],
            ],
            imports: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_1__word_place_word_place__["a" /* WordPlacePipe */],
            ]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 809:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return WordPlacePipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(57);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the WordPlacePipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
var WordPlacePipe = /** @class */ (function () {
    /**
     * Takes a value and makes it lowercase.
     */
    function WordPlacePipe(sanitizer) {
        this.sanitizer = sanitizer;
    }
    WordPlacePipe.prototype.transform = function (input, word) {
        if (!word)
            return input;
        var result = input.replace(word, "<span style='color: #3DDFD2;'>" + word + "</span>");
        return this.sanitizer.bypassSecurityTrustHtml(result);
    };
    WordPlacePipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Pipe"])({
            name: 'wordPlace',
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["c" /* DomSanitizer */]])
    ], WordPlacePipe);
    return WordPlacePipe;
}());

//# sourceMappingURL=word-place.js.map

/***/ })

},[432]);
//# sourceMappingURL=main.js.map